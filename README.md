# Cinta Tulungagung

Use [Inertia.js](https://inertiajs.com/) works with [Laravel](https://laravel.com/) and [React](https://reactjs.org/).

> This is a port of the original [Ping CRM](https://github.com/inertiajs/pingcrm) written in Laravel and Vue.

## Installation

Clone the repo locally:

```sh
git clone https://gitlab.com/dwiheruwaspodo/cinta-tulungagung.git
cd cinta-tulungagung
```

Install PHP dependencies:

```sh
composer install
```

Install NPM dependencies:

```sh
yarn install
```

Build assets:

```sh
yarn run dev
```

Setup configuration:

```sh
cp .env.example .env
```

Generate application key:

```sh
php artisan key:generate
```

Create an database manually. You can also use database MySQL or Postgres, simply update your configuration accordingly in .env file.

```sh
DB_CONNECTION=mysql/postgres
DB_HOST=
DB_PORT=
DB_DATABASE=
DB_USERNAME=
DB_PASSWORD=
```

Publish storage 
```sh
php artisan storage:link
```

Run database migrations:

```sh
php artisan migrate
```

Run database seeder:

```sh
php artisan db:seed
```

Run artisan server:

```sh
php artisan serve
```

You're ready to go! [Visit Ping CRM](http://127.0.0.1:8000/) in your browser, and login with:

- **Username:** superman@agung.com
- **Password:** 123

## Credits

- Original work by Jonathan Reinink (@reinink) and contributors
- Port to Ruby on Rails by Georg Ledermann (@ledermann)
- Port to React by Lado Lomidze (@landish)
