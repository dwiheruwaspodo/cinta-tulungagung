<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class MarriageAgeKabupatenExport implements FromView
{
    private $query;
    private $title;

    public function __construct($query, $title)
    {
        $this->query = $query;
        $this->title = $title;
    }

    public function view(): View
    {
        return view('exports.marriage_age_kabupaten', [
            'data' => $this->query,
            'title' => $this->title
        ]);
    }
}
