<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class PkkRegulationExport implements FromView
{
    private $query;
    private $title;

    public function __construct($query, $title)
    {
        $this->query = $query;
        $this->title = $title;
    }

    public function view(): View
    {
        return view('exports.pkk_regulation', [
            'data' => $this->query,
            'title' => $this->title
        ]);
    }
}
