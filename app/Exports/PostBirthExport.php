<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class PostBirthExport implements FromView
{
    private $query;
    private $title;

    public function __construct($query, $title)
    {
        $this->query = $query;
        $this->title = $title;
    }

    public function view(): View
    {
        return view('exports.post_birth', [
            'data' => $this->query,
            'title' => $this->title
        ]);
    }
}
