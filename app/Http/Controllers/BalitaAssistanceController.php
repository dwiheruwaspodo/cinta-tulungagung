<?php

namespace App\Http\Controllers;

use App\Exports\BalitaAssistanceExport;
use App\Http\Requests\BalitaAssistances\BalitaAssistanceStoreRequest;
use App\Http\Requests\BalitaAssistances\BalitaAssistanceUpdateRequest;
use App\Http\Resources\BalitaAssistances\BalitaAssistanceCollection;
use App\Http\Resources\BalitaAssistances\BalitaAssistanceResource;
use App\Http\Resources\Districts\DistrictCollection;
use App\Http\Resources\Villages\VillageCollection;
use App\Models\BalitaAssistance;
use App\Models\District;
use App\Models\Village;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;
use Inertia\Inertia;
use Maatwebsite\Excel\Facades\Excel;

class BalitaAssistanceController extends Controller
{
    private function queryListMain()
    {
        return BalitaAssistance::with('village')
            ->with('district')
            ->orderByDate()
            ->filter(Request::only('search', 'date_start', 'date_end'));
    }

    public function index(): \Inertia\Response
    {
        return Inertia::render('BalitaAssistances/Index', [
            'filters' => \Illuminate\Support\Facades\Request::all('search', 'date_start', 'date_end'),
            'balitas' => new BalitaAssistanceCollection($this->queryList($this->queryListMain())),
        ]);
    }

    public function create(): \Inertia\Response
    {
        return Inertia::render('BalitaAssistances/Create', [
            'districts' => new DistrictCollection(
                District::orderBy('name')->get()
            ),
            'villages' => new VillageCollection(
                Village::orderBy('name')->get()
            ),
            'opt_status' => BalitaAssistance::OPT_STATUS,
            'opt_posyandu' => BalitaAssistance::optPosyandu(),
        ]);
    }

    public function store(BalitaAssistanceStoreRequest $request): \Illuminate\Http\RedirectResponse
    {
        $input = Arr::add($request->validated(), 'created_by', Auth::user()->id);

        BalitaAssistance::create($input);

        return Redirect::route('balita-asisstances')->with('success', 'Pendampingan Balita ditambahkan.');
    }

    public function edit(BalitaAssistance $balitaAssistance): \Inertia\Response
    {
        return Inertia::render('BalitaAssistances/Edit', [
            'balita' => new BalitaAssistanceResource($balitaAssistance),
            'districts' => new DistrictCollection(
                District::orderBy('name')->get()
            ),
            'villages' => new VillageCollection(
                Village::orderBy('name')->get()
            ),
            'opt_status' => BalitaAssistance::OPT_STATUS,
            'opt_posyandu' => BalitaAssistance::optPosyandu(),
        ]);
    }

    public function update(BalitaAssistance $balitaAssistance, BalitaAssistanceUpdateRequest $request): \Illuminate\Http\RedirectResponse
    {
        $input = $request->validated();

        $balitaAssistance->update($input);

        return Redirect::back()->with('success', 'Pendampingan Balita diperbarui.');
    }

    public function destroy(BalitaAssistance $balitaAssistance): \Illuminate\Http\RedirectResponse
    {
        $balitaAssistance->delete();

        return Redirect::route('balita-asisstances')->with('success', 'Pendampingan Balita dihapus.');
    }

    public function export(): \Symfony\Component\HttpFoundation\BinaryFileResponse
    {
        return Excel::download(new BalitaAssistanceExport($this->queryListMain()->get()->all(),
            $this->titleExport('Pendampingan Balita')),
            $this->titleExport('Pendampingan Balita'). '.xlsx');
    }
}
