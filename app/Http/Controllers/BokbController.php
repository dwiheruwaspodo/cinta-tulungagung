<?php

namespace App\Http\Controllers;

use App\Exports\BokbExport;
use App\Exports\PoktanExport;
use App\Http\Requests\Bokbs\BokbStoreRequest;
use App\Http\Requests\Bokbs\BokbUpdateRequest;
use App\Http\Requests\Poktans\PoktanStoreRequest;
use App\Http\Requests\Poktans\PoktanUpdateRequest;
use App\Http\Resources\Bokbs\BokbCollection;
use App\Http\Resources\Bokbs\BokbResource;
use App\Http\Resources\Districts\DistrictCollection;
use App\Http\Resources\Poktans\PoktanCollection;
use App\Http\Resources\Poktans\PoktanResource;
use App\Http\Resources\Villages\VillageCollection;
use App\Models\bokb;
use App\Models\District;
use App\Models\Poktan;
use App\Models\Role;
use App\Models\Village;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Inertia\Inertia;
use Maatwebsite\Excel\Facades\Excel;

class BokbController extends Controller
{
    private function queryListMain()
    {
        $query = Bokb::with('district')
            ->with('village')
            ->orderByDate()
            ->filter(\Illuminate\Support\Facades\Request::only('search', 'date_start', 'date_end'));

        if (Role::isKecamatan(Auth::user()))
        {
            $query = $query->where('district_id', '=', Auth::user()->district_id);
        }

        return $query;
    }

    public function index(): \Inertia\Response
    {
        return Inertia::render('Bokbs/Index', [
            'filters' => \Illuminate\Support\Facades\Request::all('search', 'date_start', 'date_end'),
            'bokb' => new BokbCollection($this->queryList($this->queryListMain())),
        ]);
    }

    public function create(): \Inertia\Response
    {
        return Inertia::render('Bokbs/Create', [
            'districts' => new DistrictCollection(
                District::orderBy('name')->get()
            ),
            'villages' => new VillageCollection(
                Village::orderBy('name')->get()
            ),
            'opt_activities' => Bokb::ACTIVITIES,
            'opt_locations' => Bokb::LOCATIONS,
            'opt_source_person_from' => Bokb::SOURCE_PERSON_FROMS
        ]);
    }

    public function store(BokbStoreRequest $request): \Illuminate\Http\RedirectResponse
    {
        $data = Arr::add($request->validated(), 'created_by', Auth::user()->id);
        $data['date'] = date('Y-m-d H:i:s', strtotime($data['date']));

        if ($this->checkAlreadyCreatedByMonth($data))
        {
            return Redirect::back()->with('error', 'Bokb bulan terpilih sudah ada.');
        }

        Bokb::create($data);

        return Redirect::route('bokbs')->with('success', 'Pertemuan BOKB ditambahkan.');
    }

    private function checkAlreadyCreatedByMonth($post): bool
    {
        $exist = Bokb::where('date', '=', $post['date'])
            ->where('district_id', '=', $post['district_id'])
            ->where('village_id', '=', $post['village_id'])
            ->get()->first();

        return isset($exist);
    }

    public function edit(Bokb $bokb): \Inertia\Response
    {
        return Inertia::render('Bokbs/Edit', [
            'bokb' => new BokbResource($bokb),
            'districts' => new DistrictCollection(
                District::orderBy('name')->get()
            ),
            'villages' => new VillageCollection(
                Village::orderBy('name')->get()
            ),
            'opt_activities' => Bokb::ACTIVITIES,
            'opt_locations' => Bokb::LOCATIONS,
            'opt_source_person_from' => Bokb::SOURCE_PERSON_FROMS
        ]);
    }

    public function update(Bokb $bokb, BokbUpdateRequest $request): \Illuminate\Http\RedirectResponse
    {
        $bokb->update(
            $request->validated()
        );

        return Redirect::back()->with('success', 'Pertemuan BOKB diperbarui.');
    }

    public function destroy(Bokb $bokb): \Illuminate\Http\RedirectResponse
    {
        $bokb->delete();

        return Redirect::route('bokbs')->with('success', 'Pertemuan BOKB dihapus.');
    }

    public function export(): \Symfony\Component\HttpFoundation\BinaryFileResponse
    {
        return Excel::download(new BokbExport($this->queryListMain()->get()->all(),
            $this->titleExport('Pertemuan Bokb')),
            $this->titleExport('Pertemuan Bokb'). '.xlsx');
    }
}
