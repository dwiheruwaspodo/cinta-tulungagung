<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Auth;
use Illuminate\Support\Facades\Request;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function queryList($query)
    {
        return $query->paginate()->appends(Request::all());
    }

    public function titleExport($defaultTitle = ''): string
    {
        $request = Request::all();
        $title = $defaultTitle;

        if (isset($request['date_start']) && isset($request['date_end']))
        {
            $title = $title. ' ('. date_format(date_create($request['date_start']),"d M Y") . ' - ';
            $title = $title. date_format(date_create($request['date_end']),"d M Y") .')';
        }

        return $title;
    }
}
