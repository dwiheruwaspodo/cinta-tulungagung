<?php

namespace App\Http\Controllers;

use App\Exports\MarriageAgeExport;
use App\Exports\MarriageAgeKabupatenExport;
use App\Http\Requests\MarriageAges\MarriageAgeStoreRequest;
use App\Http\Requests\MarriageAges\MarriageAgeUpdateRequest;
use App\Http\Resources\Districts\DistrictCollection;
use App\Http\Resources\MarriageAges\MarriageAgeCollection;
use App\Http\Resources\MarriageAges\MarriageAgeKabupatenCollection;
use App\Http\Resources\MarriageAges\MarriageAgeResource;
use App\Http\Resources\Villages\VillageCollection;
use App\Models\District;
use App\Models\MarriageAge;
use App\Models\Role;
use App\Models\Village;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Inertia\Inertia;
use Maatwebsite\Excel\Facades\Excel;

class MarriageAgeController extends Controller
{
    private function queryListMain()
    {
        $query = MarriageAge::with('district')
            ->with('village')
            ->orderByDate()
            ->filter(\Illuminate\Support\Facades\Request::only('search', 'date_start', 'date_end'));

        if (Role::isKecamatan(Auth::user()))
        {
            $query = $query->where('district_id', '=', Auth::user()->district_id);
        }

        return $query;
    }

    private function queryKabupatenPov()
    {
        return MarriageAge::with('district')
            ->with('village')
            ->groupBy('district_id', 'month', 'year')
            ->select(
                DB::raw('sum(less_20) as sum_less_20'),
                DB::raw('sum(age_21_25) as sum_age_21_25'),
                DB::raw('sum(age_26_30) as sum_age_26_30'),
                DB::raw('sum(more_30) as sum_more_30'),
                DB::raw('YEAR(date) year, MONTH(date) month'),
                'district_id'
            );
    }

    public function index(): \Inertia\Response
    {
        if (Role::isKabupaten(Auth::user()))
        {
            return Inertia::render('MarriageAges/IndexKabupaten', [
                'filters' => \Illuminate\Support\Facades\Request::all('search', 'date_start', 'date_end'),
                'ages' => new MarriageAgeKabupatenCollection($this->queryList($this->queryKabupatenPov())),
            ]);
        }
        else
        {
            return Inertia::render('MarriageAges/Index', [
                'filters' => \Illuminate\Support\Facades\Request::all('search', 'date_start', 'date_end'),
                'ages' => new MarriageAgeCollection($this->queryList($this->queryListMain())),
            ]);
        }
    }

    public function create(): \Inertia\Response
    {
        return Inertia::render('MarriageAges/Create', [
            'districts' => new DistrictCollection(
                District::orderBy('name')->get()
            ),
            'villages' => new VillageCollection(
                Village::orderBy('name')->get()
            )
        ]);
    }

    public function store(MarriageAgeStoreRequest $request): \Illuminate\Http\RedirectResponse
    {
        $data = Arr::add($request->validated(), 'created_by', Auth::user()->id);
        $data['date'] = date('Y-m-d H:i:s', strtotime($data['date']));

        if ($this->checkAlreadyCreatedByMonth($data))
        {
            return Redirect::back()->with('error', 'Usia Kawin bulan terpilih sudah ada.');
        }

        MarriageAge::create($data);

        return Redirect::route('marriage-ages')->with('success', 'Data Usia Kawin ditambahkan.');
    }

    private function checkAlreadyCreatedByMonth($post): bool
    {
        $exist = MarriageAge::where('date', '=', $post['date'])
            ->where('district_id', '=', $post['district_id'])
            ->where('village_id', '=', $post['village_id'])
            ->get()->first();

        return isset($exist);
    }

    public function edit(MarriageAge $marriageAge): \Inertia\Response
    {
        return Inertia::render('MarriageAges/Edit', [
            'age' => new MarriageAgeResource($marriageAge),
            'districts' => new DistrictCollection(
                District::orderBy('name')->get()
            ),
            'villages' => new VillageCollection(
                Village::orderBy('name')->get()
            )
        ]);
    }

    public function update(MarriageAge $marriageAge, MarriageAgeUpdateRequest $request): \Illuminate\Http\RedirectResponse
    {
        $marriageAge->update(
            $request->validated()
        );

        return Redirect::back()->with('success', 'Data Usia Kawin diperbarui.');
    }

    public function destroy(MarriageAge $marriageAge): \Illuminate\Http\RedirectResponse
    {
        $marriageAge->delete();

        return Redirect::route('marriage-ages')->with('success', 'Data Usia Kawin dihapus.');
    }

    public function export(): \Symfony\Component\HttpFoundation\BinaryFileResponse
    {
        if (Role::isKabupaten(Auth::user()))
        {
            return Excel::download(new MarriageAgeKabupatenExport($this->queryKabupatenPov()->get()->all(),
                $this->titleExport('Usia Kawin')),
                $this->titleExport('Usia Kawin'). '.xlsx');
        }
        else
        {
            return Excel::download(new MarriageAgeExport($this->queryListMain()->get()->all(),
                $this->titleExport('Usia Kawin')),
                $this->titleExport('Usia Kawin'). '.xlsx');
        }

    }
}
