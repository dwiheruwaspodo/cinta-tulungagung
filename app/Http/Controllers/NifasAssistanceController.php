<?php

namespace App\Http\Controllers;

use App\Exports\NifasAssistanceExport;
use App\Http\Requests\Nifas\NifasStoreRequest;
use App\Http\Requests\Nifas\NifasUpdateRequest;
use App\Http\Resources\Districts\DistrictCollection;
use App\Http\Resources\NifasAssistances\NifasAssistanceCollection;
use App\Http\Resources\NifasAssistances\NifasAssistanceResource;
use App\Http\Resources\Villages\VillageCollection;
use App\Models\District;
use App\Models\NifasAssistance;
use App\Models\Village;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;
use Inertia\Inertia;
use Maatwebsite\Excel\Facades\Excel;

class NifasAssistanceController extends Controller
{
    private function queryListMain()
    {
        return NifasAssistance::with('village')
            ->with('district')
            ->orderByDate()
            ->filter(Request::only('search', 'date_start', 'date_end'));
    }

    public function index(): \Inertia\Response
    {
        return Inertia::render('NifasAssistances/Index', [
            'filters' => \Illuminate\Support\Facades\Request::all('search', 'date_start', 'date_end'),
            'nifas' => new NifasAssistanceCollection($this->queryList($this->queryListMain()))
        ]);
    }

    public function create(): \Inertia\Response
    {
        return Inertia::render('NifasAssistances/Create', [
            'districts' => new DistrictCollection(
                District::orderBy('name')->get()
            ),
            'villages' => new VillageCollection(
                Village::orderBy('name')->get()
            ),
            'opt_kb' => NifasAssistance::OPT_KB
        ]);
    }

    public function store(NifasStoreRequest $request)
    {
        $input = Arr::add($request->all(), 'created_by', Auth::user()->id);

        if ($input['is_contraception'] == 0) unset($input['contraception']);
        NifasAssistance::create($input);

        return Redirect::route('nifas-asisstances')->with('success', 'Pendampingan Nikah ditambahkan.');
    }

    public function edit(NifasAssistance $nifasAssistance): \Inertia\Response
    {
        return Inertia::render('NifasAssistances/Edit', [
            'nifas' => new NifasAssistanceResource($nifasAssistance),
            'districts' => new DistrictCollection(
                District::orderBy('name')->get()
            ),
            'villages' => new VillageCollection(
                Village::orderBy('name')->get()
            ),
            'opt_kb' => NifasAssistance::OPT_KB
        ]);
    }

    public function update(NifasAssistance $nifasAssistance, NifasUpdateRequest $request): \Illuminate\Http\RedirectResponse
    {
        $input = $request->validated();

        if ($input['is_contraception'] == 0) unset($input['contraception']);

        $nifasAssistance->update($input);

        return Redirect::back()->with('success', 'Pendampingan Nifas diperbarui.');
    }

    public function destroy(NifasAssistance $nifasAssistance): \Illuminate\Http\RedirectResponse
    {
        $nifasAssistance->delete();

        return Redirect::route('nifas-asisstances')->with('success', 'Pendampingan Nifas dihapus.');
    }

    public function export(): \Symfony\Component\HttpFoundation\BinaryFileResponse
    {
        return Excel::download(new NifasAssistanceExport($this->queryListMain()->get()->all(),
            $this->titleExport('Pendampingan Nifas')),
            $this->titleExport('Pendampingan Nifas'). '.xlsx');
    }

}
