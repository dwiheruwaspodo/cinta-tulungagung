<?php

namespace App\Http\Controllers;

use App\Exports\PkkActivityExport;
use App\Http\Requests\PkkActivities\PkkActivityStoreRequest;
use App\Http\Requests\PkkActivities\PkkActivityUpdateRequest;
use App\Http\Resources\Districts\DistrictCollection;
use App\Http\Resources\PkkActivities\PkkActivityCategoryCollection;
use App\Http\Resources\PkkActivities\PkkActivityCollection;
use App\Http\Resources\PkkActivities\PkkActivityResource;
use App\Http\Resources\Villages\VillageCollection;
use App\Models\District;
use App\Models\PkkActivity;
use App\Models\PkkActivityCategory;
use App\Models\Village;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;
use Inertia\Inertia;
use Maatwebsite\Excel\Facades\Excel;

class PkkActivityController extends Controller
{
    private function queryListMain()
    {
        return PkkActivity::with('pkk_activity_category')
            ->with('district')
            ->with('village')
            ->orderByDate()
            ->filter(Request::only('search', 'date_start', 'date_end'));
    }

    public function index(): \Inertia\Response
    {
        return Inertia::render('PkkActivities/Index', [
            'filters' => \Illuminate\Support\Facades\Request::all('search', 'date_start', 'date_end'),
            'pkk' => new PkkActivityCollection($this->queryList($this->queryListMain())),
        ]);
    }

    public function create(): \Inertia\Response
    {
        return Inertia::render('PkkActivities/Create', [
            'categories' => new PkkActivityCategoryCollection(
                PkkActivityCategory::orderBy('category')->get()
            ),
            'districts' => new DistrictCollection(
                District::orderBy('name')->get()
            ),
            'villages' => new VillageCollection(
                Village::orderBy('name')->get()
            ),
            'levels' => PkkActivity::OPT_LEVEL
        ]);
    }

    public function store(PkkActivityStoreRequest $request): \Illuminate\Http\RedirectResponse
    {
        $activity = Arr::add($request->validated(), 'created_by', Auth::user()->id);

        if ($activity['level'] == PkkActivity::KABUPATEN) {
            unset($activity['district_id']);
            unset($activity['village_id']);
        }

        PkkActivity::create($activity);

        return Redirect::route('pkk-activities')->with('success', 'Kegiatan PKK ditambahkan.');
    }

    public function edit(PkkActivity $pkkActivity): \Inertia\Response
    {
        return Inertia::render('PkkActivities/Edit', [
            'pkk' => new PkkActivityResource($pkkActivity),
            'categories' => new PkkActivityCategoryCollection(
                PkkActivityCategory::orderBy('category')->get()
            ),
            'districts' => new DistrictCollection(
                District::orderBy('name')->get()
            ),
            'villages' => new VillageCollection(
                Village::orderBy('name')->get()
            ),
            'levels' => PkkActivity::OPT_LEVEL
        ]);
    }

    public function update(PkkActivity $pkkActivity, PkkActivityUpdateRequest $request): \Illuminate\Http\RedirectResponse
    {
        $activity = $request->validated();

        if ($activity['level'] == PkkActivity::KABUPATEN) {
            $activity['district_id'] = null;
            $activity['village_id'] = null;
        }

        $pkkActivity->update($activity);

        return Redirect::back()->with('success', 'Kegiatan PKK diperbarui.');
    }

    public function destroy(PkkActivity $pkkActivity): \Illuminate\Http\RedirectResponse
    {
        $pkkActivity->delete();

        return Redirect::route('pkk-activities')->with('success', 'Kegiatan PKK dihapus.');
    }

    public function export(): \Symfony\Component\HttpFoundation\BinaryFileResponse
    {
        return Excel::download(new PkkActivityExport($this->queryListMain()->get()->all(),
            $this->titleExport('Aktivitas PKK & Desa')),
            $this->titleExport('Aktivitas PKK & Desa'). '.xlsx');
    }
}
