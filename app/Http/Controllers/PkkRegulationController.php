<?php

namespace App\Http\Controllers;
use App\Exports\PkkActivityExport;
use App\Exports\PkkRegulationExport;
use App\Http\Requests\PkkRegulations\PkkRegulationStoreRequest;
use App\Http\Requests\PkkRegulations\PkkRegulationUpdateRequest;
use App\Http\Resources\PkkRegulations\PkkRegulationCollection;
use App\Http\Resources\PkkRegulations\PkkRegulationResource;
use App\Models\PkkRegulation;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;
use Inertia\Inertia;
use Maatwebsite\Excel\Facades\Excel;

class PkkRegulationController extends Controller
{
    private function queryListMain()
    {
        return PkkRegulation::orderByDate()
            ->filter(Request::only('search', 'date_start', 'date_end'));
    }

    public function index(): \Inertia\Response
    {
        return Inertia::render('PkkRegulations/Index', [
            'filters' => \Illuminate\Support\Facades\Request::all('search', 'date_start', 'date_end'),
            'pkk' => new PkkRegulationCollection($this->queryList($this->queryListMain())),
        ]);
    }

    public function create(): \Inertia\Response
    {
        return Inertia::render('PkkRegulations/Create');
    }

    public function store(PkkRegulationStoreRequest $request): \Illuminate\Http\RedirectResponse
    {
        $activity = Arr::add($request->validated(), 'created_by', Auth::user()->id);
        PkkRegulation::create($activity);

        return Redirect::route('pkk-regulations')->with('success', 'Regulasi ditambahkan.');
    }

    public function edit(PkkRegulation $pkkRegulation): \Inertia\Response
    {
        return Inertia::render('PkkRegulations/Edit', [
            'pkk' => new PkkRegulationResource($pkkRegulation)
        ]);
    }

    public function update(PkkRegulation $pkkRegulation, PkkRegulationUpdateRequest $request): \Illuminate\Http\RedirectResponse
    {
        $pkkRegulation->update(
            $request->validated()
        );

        return Redirect::back()->with('success', 'Regulasi diperbarui.');
    }

    public function destroy(PkkRegulation $pkkRegulation): \Illuminate\Http\RedirectResponse
    {
        $pkkRegulation->delete();

        return Redirect::route('pkk-regulations')->with('success', 'Regulasi dihapus.');
    }

    public function export(): \Symfony\Component\HttpFoundation\BinaryFileResponse
    {
        return Excel::download(new PkkRegulationExport($this->queryListMain()->get()->all(),
            $this->titleExport('Regulasi')), $this->titleExport('Regulasi'). '.xlsx');
    }
}
