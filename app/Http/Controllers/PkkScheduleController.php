<?php

namespace App\Http\Controllers;

use App\Exports\PkkActivityExport;
use App\Exports\PkkScheduleExport;
use App\Http\Requests\PkkSchedules\PkkScheduleStoreRequest;
use App\Http\Requests\PkkSchedules\PkkScheduleUpdateRequest;
use App\Http\Resources\Districts\DistrictCollection;
use App\Http\Resources\PkkActivities\PkkActivityCategoryCollection;
use App\Http\Resources\PkkSchedules\PkkScheduleCollection;
use App\Http\Resources\PkkSchedules\PkkScheduleResource;
use App\Http\Resources\Villages\VillageCollection;
use App\Models\District;
use App\Models\PkkActivityCategory;
use App\Models\PkkSchedule;
use App\Models\Village;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;
use Inertia\Inertia;
use Maatwebsite\Excel\Facades\Excel;

class PkkScheduleController extends Controller
{
    private function queryListMain()
    {
        return PkkSchedule::with('pkk_activity_category')
            ->with('district')
            ->with('village')
            ->orderByDate()
            ->filter(Request::only('search', 'date_start', 'date_end'));
    }

    public function index(): \Inertia\Response
    {
        return Inertia::render('PkkSchedules/Index', [
            'filters' => \Illuminate\Support\Facades\Request::all('search', 'date_start', 'date_end'),
            'pkk' => new PkkScheduleCollection($this->queryList($this->queryListMain())),
        ]);
    }

    public function create(): \Inertia\Response
    {
        return Inertia::render('PkkSchedules/Create', [
            'categories' => new PkkActivityCategoryCollection(
                PkkActivityCategory::orderBy('category')->get()
            ),
            'districts' => new DistrictCollection(
                District::orderBy('name')->get()
            ),
            'villages' => new VillageCollection(
                Village::orderBy('name')->get()
            ),
        ]);
    }

    public function store(PkkScheduleStoreRequest $request): \Illuminate\Http\RedirectResponse
    {
        $activity = Arr::add($request->validated(), 'created_by', Auth::user()->id);
        PkkSchedule::create($activity);

        return Redirect::route('pkk-schedules')->with('success', 'Jadwal PKK ditambahkan.');
    }

    public function edit(PkkSchedule $pkkSchedule): \Inertia\Response
    {
        return Inertia::render('PkkSchedules/Edit', [
            'pkk' => new PkkScheduleResource($pkkSchedule),
            'categories' => new PkkActivityCategoryCollection(
                PkkActivityCategory::orderBy('category')->get()
            ),
            'districts' => new DistrictCollection(
                District::orderBy('name')->get()
            ),
            'villages' => new VillageCollection(
                Village::orderBy('name')->get()
            ),
        ]);
    }

    public function update(PkkSchedule $pkkActivity, PkkScheduleUpdateRequest $request): \Illuminate\Http\RedirectResponse
    {
        $pkkActivity->update(
            $request->validated()
        );

        return Redirect::back()->with('success', 'Jadwal PKK diperbarui.');
    }

    public function destroy(PkkSchedule $pkkActivity): \Illuminate\Http\RedirectResponse
    {
        $pkkActivity->delete();

        return Redirect::route('pkk-schedules')->with('success', 'Jadwal PKK dihapus.');
    }


    public function export(): \Symfony\Component\HttpFoundation\BinaryFileResponse
    {
        return Excel::download(new PkkScheduleExport($this->queryListMain()->get()->all(),
            $this->titleExport('Rencana Kerja PKK & Desa')),
            $this->titleExport('Rencana Kerja PKK & Desa'). '.xlsx');
    }
}

