<?php

namespace App\Http\Controllers;

use App\Exports\PoktanExport;
use App\Exports\PoktanKabupatenExport;
use App\Http\Requests\Poktans\PoktanStoreRequest;
use App\Http\Requests\Poktans\PoktanUpdateRequest;
use App\Http\Resources\Districts\DistrictCollection;
use App\Http\Resources\Poktans\PoktanCollection;
use App\Http\Resources\Poktans\PoktanKabupatenCollection;
use App\Http\Resources\Poktans\PoktanResource;
use App\Http\Resources\Villages\VillageCollection;
use App\Models\District;
use App\Models\Poktan;
use App\Models\Role;
use App\Models\Village;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Inertia\Inertia;
use Maatwebsite\Excel\Facades\Excel;

class PoktanController extends Controller
{
    private function queryListMain()
    {
        $query = Poktan::with('district')
            ->with('village')
            ->orderByDate()
            ->filter(\Illuminate\Support\Facades\Request::only('search', 'date_start', 'date_end'));

        if (Role::isKecamatan(Auth::user()))
        {
            $query = $query->where('district_id', '=', Auth::user()->district_id);
        }

        return $query;
    }

    private function queryKabupatenPov()
    {
        return Poktan::with('district')
            ->with('village')
            ->groupBy('district_id', 'month', 'year')
            ->select(
                DB::raw('sum(keluarga_bkb) as sum_keluarga_bkb'),
                DB::raw('sum(anggota_hadir_bkb) as sum_anggota_hadir_bkb'),
                DB::raw('sum(keluarga_bkr) as sum_keluarga_bkr'),
                DB::raw('sum(anggota_hadir_bkr) as sum_anggota_hadir_bkr'),
                DB::raw('sum(keluarga_bkl) as sum_keluarga_bkl'),
                DB::raw('sum(anggota_hadir_bkl) as sum_anggota_hadir_bkl'),
                DB::raw('sum(keluarga_uppks) as sum_keluarga_uppks'),
                DB::raw('sum(anggota_hadir_uppks) as sum_anggota_hadir_uppks'),
                DB::raw('sum(keluarga_pikr) as sum_keluarga_pikr'),
                DB::raw('sum(anggota_hadir_pikr) as sum_anggota_hadir_pikr'),
                DB::raw('YEAR(date) year, MONTH(date) month'),
                'district_id'
            );
    }

    public function index(): \Inertia\Response
    {
        if (Role::isKabupaten(Auth::user()))
        {
            return Inertia::render('Poktans/IndexKabupaten', [
                'filters' => \Illuminate\Support\Facades\Request::all('search', 'date_start', 'date_end'),
                'poktan' => new PoktanKabupatenCollection($this->queryList($this->queryKabupatenPov())),
            ]);
        }
        else
        {
            return Inertia::render('Poktans/Index', [
                'filters' => \Illuminate\Support\Facades\Request::all('search', 'date_start', 'date_end'),
                'poktan' => new PoktanCollection($this->queryList($this->queryListMain())),
            ]);
        }
    }

    public function create(): \Inertia\Response
    {
        return Inertia::render('Poktans/Create', [
            'districts' => new DistrictCollection(
                District::orderBy('name')->get()
            ),
            'villages' => new VillageCollection(
                Village::orderBy('name')->get()
            )
        ]);
    }

    public function store(PoktanStoreRequest $request): \Illuminate\Http\RedirectResponse
    {
        $data = Arr::add($request->validated(), 'created_by', Auth::user()->id);
        $data['date'] = date('Y-m-d H:i:s', strtotime($data['date']));

        if ($this->checkAlreadyCreatedByMonth($data))
        {
            return Redirect::back()->with('error', 'Poktan bulan terpilih sudah ada.');
        }

        Poktan::create($data);

        return Redirect::route('poktans')->with('success', 'Data Poktan ditambahkan.');
    }

    private function checkAlreadyCreatedByMonth($post): bool
    {
        $exist = Poktan::where('date', '=', $post['date'])
            ->where('district_id', '=', $post['district_id'])
            ->where('village_id', '=', $post['village_id'])
            ->get()->first();

        return isset($exist);
    }

    public function edit(Poktan $poktan): \Inertia\Response
    {
        return Inertia::render('Poktans/Edit', [
            'poktan' => new PoktanResource($poktan),
            'districts' => new DistrictCollection(
                District::orderBy('name')->get()
            ),
            'villages' => new VillageCollection(
                Village::orderBy('name')->get()
            )
        ]);
    }

    public function update(Poktan $poktan, PoktanUpdateRequest $request): \Illuminate\Http\RedirectResponse
    {
        $poktan->update(
            $request->validated()
        );

        return Redirect::back()->with('success', 'Data Poktan diperbarui.');
    }

    public function destroy(Poktan $poktan): \Illuminate\Http\RedirectResponse
    {
        $poktan->delete();

        return Redirect::route('poktans')->with('success', 'Data Poktan dihapus.');
    }

    public function export(): \Symfony\Component\HttpFoundation\BinaryFileResponse
    {
        if (Role::isKabupaten(Auth::user()))
        {
            return Excel::download(new PoktanKabupatenExport($this->queryKabupatenPov()->get()->all(),
                $this->titleExport('Pertemuan Poktan')),
                $this->titleExport('Pertemuan Poktan'). '.xlsx');
        }
        else
        {
            return Excel::download(new PoktanExport($this->queryListMain()->get()->all(),
                $this->titleExport('Pertemuan Poktan')),
                $this->titleExport('Pertemuan Poktan'). '.xlsx');

        }
    }
}
