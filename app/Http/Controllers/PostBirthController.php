<?php

namespace App\Http\Controllers;

use App\Exports\PostBirthExport;
use App\Exports\PostBirthKabupatenExport;
use App\Http\Requests\PostBirths\PostBirthStoreRequest;
use App\Http\Requests\PostBirths\PostBirthUpdateRequest;
use App\Http\Resources\Districts\DistrictCollection;
use App\Http\Resources\PostBirths\PostBirthCollection;
use App\Http\Resources\PostBirths\PostBirthKabupatenCollection;
use App\Http\Resources\PostBirths\PostBirthResource;
use App\Http\Resources\Villages\VillageCollection;
use App\Models\PostBirth;
use App\Models\District;
use App\Models\Role;
use App\Models\Village;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;
use Inertia\Inertia;
use Maatwebsite\Excel\Facades\Excel;
use phpDocumentor\Reflection\Types\Boolean;


class PostBirthController extends Controller
{
    private function queryListMain()
    {
        $query = PostBirth::with('district')
            ->with('village')
            ->orderByDate()
            ->filter(\Illuminate\Support\Facades\Request::only('search', 'date_start', 'date_end'));

        if (Role::isKecamatan(Auth::user()))
        {
            $query = $query->where('district_id', '=', Auth::user()->district_id);
        }

        return $query;
    }

    private function queryKabupatenPov()
    {
        return PostBirth::with('district')
            ->with('village')
            ->groupBy('district_id', 'month', 'year')
            ->select(
                DB::raw('sum(birth_suntik) as sum_birth_suntik'),
                DB::raw('sum(birth_pil) as sum_birth_pil'),
                DB::raw('sum(birth_kondom) as sum_birth_kondom'),
                DB::raw('sum(birth_iud) as sum_birth_iud'),
                DB::raw('sum(birth_implan) as sum_birth_implan'),
                DB::raw('sum(birth_mop) as sum_birth_mop'),
                DB::raw('sum(birth_mow) as sum_birth_mow'),
                DB::raw('sum(miscarriage_suntik) as sum_miscarriage_suntik'),
                DB::raw('sum(miscarriage_pil) as sum_miscarriage_pil'),
                DB::raw('sum(miscarriage_kondom) as sum_miscarriage_kondom'),
                DB::raw('sum(miscarriage_iud) as sum_miscarriage_iud'),
                DB::raw('sum(miscarriage_implan) as sum_miscarriage_implan'),
                DB::raw('sum(miscarriage_mop) as sum_miscarriage_mop'),
                DB::raw('sum(miscarriage_mow) as sum_miscarriage_mow'),
                DB::raw('YEAR(date) year, MONTH(date) month'),
                'district_id'
            );
    }

    public function index(): \Inertia\Response
    {
        if (Role::isKabupaten(Auth::user()))
        {
            return Inertia::render('PostBirths/IndexKabupaten', [
                'filters' => \Illuminate\Support\Facades\Request::all('search', 'date_start', 'date_end'),
                'postBirth' => new PostBirthKabupatenCollection($this->queryList($this->queryKabupatenPov())),
            ]);
        }
        else
        {
            return Inertia::render('PostBirths/Index', [
                'filters' => \Illuminate\Support\Facades\Request::all('search', 'date_start', 'date_end'),
                'postBirth' => new PostBirthCollection($this->queryList($this->queryListMain())),
            ]);
        }
    }

    public function create(): \Inertia\Response
    {
        return Inertia::render('PostBirths/Create', [
            'districts' => new DistrictCollection(
                District::orderBy('name')->get()
            ),
            'villages' => new VillageCollection(
                Village::orderBy('name')->get()
            ),
        ]);
    }

    public function store(PostBirthStoreRequest $request): \Illuminate\Http\RedirectResponse
    {
        $post = Arr::add($request->validated(), 'created_by', Auth::user()->id);
        $post['date'] = date('Y-m-d H:i:s', strtotime($post['date']));

        if ($this->checkAlreadyCreatedByMonth($post))
        {
            return Redirect::back()->with('error', 'Pasca Persalinan bulan terpilih sudah ada.');
        }

        PostBirth::create($post);

        return Redirect::route('post-births')->with('success', 'Pasca Persalinan ditambahkan.');
    }

    private function checkAlreadyCreatedByMonth($post): bool
    {
        $exist = PostBirth::where('date', '=', $post['date'])
            ->where('district_id', '=', $post['district_id'])
            ->where('village_id', '=', $post['village_id'])
            ->get()->first();

        return isset($exist);
    }

    public function edit(PostBirth $postBirth): \Inertia\Response
    {
        return Inertia::render('PostBirths/Edit', [
            'postBirth' => new PostBirthResource($postBirth),
            'districts' => new DistrictCollection(
                District::orderBy('name')->get()
            ),
            'villages' => new VillageCollection(
                Village::orderBy('name')->get()
            ),
        ]);
    }

    public function update(PostBirth $postBirth, PostBirthUpdateRequest $request): \Illuminate\Http\RedirectResponse
    {
        $postBirth->update(
            $request->validated()
        );

        return Redirect::back()->with('success', 'Pasca Persalinan diperbarui.');
    }

    public function destroy(PostBirth $postBirth): \Illuminate\Http\RedirectResponse
    {
        $postBirth->delete();

        return Redirect::route('post-births')->with('success', 'Pasca Persalinan dihapus.');
    }


    public function export(): \Symfony\Component\HttpFoundation\BinaryFileResponse
    {
        if (Role::isKabupaten(Auth::user()))
        {
            return Excel::download(new PostBirthKabupatenExport($this->queryKabupatenPov()->get()->all(),
                $this->titleExport('Pasca Persalinan')),
                $this->titleExport('Pasca Persalinan'). '.xlsx');
        }
        else
        {
            return Excel::download(new PostBirthExport($this->queryListMain()->get()->all(),
                $this->titleExport('Pasca Persalinan')),
                $this->titleExport('Pasca Persalinan'). '.xlsx');
        }
    }
}
