<?php

namespace App\Http\Controllers;

use App\Exports\PregnancyAssistanceExport;
use App\Http\Requests\Pregnancies\PregnancyStoreRequest;
use App\Http\Requests\Pregnancies\PregnancyUpdateRequest;
use App\Http\Resources\Districts\DistrictCollection;
use App\Http\Resources\PregnancyAssistances\PregnancyAssistanceCollection;
use App\Http\Resources\PregnancyAssistances\PregnancyAssistanceResource;
use App\Http\Resources\Villages\VillageCollection;
use App\Models\District;
use App\Models\PregnancyAssistance;
use App\Models\Village;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;
use Inertia\Inertia;
use Maatwebsite\Excel\Facades\Excel;

class PregnancyAssistanceController extends Controller
{
    private function queryListMain()
    {
        return PregnancyAssistance::with('village')
            ->with('district')
            ->orderByDate()
            ->filter(Request::only('search', 'date_start', 'date_end'));
    }

    public function index(): \Inertia\Response
    {
        return Inertia::render('PregnancyAssistances/Index', [
            'filters' => \Illuminate\Support\Facades\Request::all('search', 'date_start', 'date_end'),
            'pregnancies' => new PregnancyAssistanceCollection($this->queryList($this->queryListMain())),
        ]);
    }

    public function create(): \Inertia\Response
    {
        return Inertia::render('PregnancyAssistances/Create', [
            'districts' => new DistrictCollection(
                District::orderBy('name')->get()
            ),
            'villages' => new VillageCollection(
                Village::orderBy('name')->get()
            )
        ]);
    }

    public function store(PregnancyStoreRequest $request): \Illuminate\Http\RedirectResponse
    {
        $input = Arr::add($request->validated(), 'created_by', Auth::user()->id);

        PregnancyAssistance::create($input);

        return Redirect::route('pregnancy-asisstances')->with('success', 'Pendampingan Bumil ditambahkan.');
    }

    public function edit(PregnancyAssistance $pregnancyAssistance): \Inertia\Response
    {
        return Inertia::render('PregnancyAssistances/Edit', [
            'pregnancy' => new PregnancyAssistanceResource($pregnancyAssistance),
            'districts' => new DistrictCollection(
                District::orderBy('name')->get()
            ),
            'villages' => new VillageCollection(
                Village::orderBy('name')->get()
            )
        ]);
    }

    public function update(PregnancyAssistance $pregnancyAssistance, PregnancyUpdateRequest $request): \Illuminate\Http\RedirectResponse
    {
        $pregnancyAssistance->update($request->validated());

        return Redirect::back()->with('success', 'Pendampingan Bumil diperbarui.');
    }

    public function destroy(PregnancyAssistance $pregnancyAssistance): \Illuminate\Http\RedirectResponse
    {
        $pregnancyAssistance->delete();

        return Redirect::route('pregnancy-asisstances')->with('success', 'Pendampingan Bumil dihapus.');
    }

    public function export(): \Symfony\Component\HttpFoundation\BinaryFileResponse
    {
        return Excel::download(new PregnancyAssistanceExport($this->queryListMain()->get()->all(),
            $this->titleExport('Pendampingan Bumil')),
            $this->titleExport('Pendampingan Bumil'). '.xlsx');
    }
}
