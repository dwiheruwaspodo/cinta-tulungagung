<?php

namespace App\Http\Controllers;

use App\Exports\PriorityExport;
use App\Http\Requests\Priorities\PriorityStoreRequest;
use App\Http\Requests\Priorities\PriorityUpdateRequest;
use App\Http\Resources\Districts\DistrictCollection;
use App\Http\Resources\Priorities\PriorityCollection;
use App\Http\Resources\Priorities\PriorityResource;
use App\Http\Resources\Villages\VillageCollection;
use App\Models\District;
use App\Models\Priority;
use App\Models\Role;
use App\Models\Village;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Inertia\Inertia;
use Maatwebsite\Excel\Facades\Excel;

class PriorityController extends Controller
{
    private function queryListMain()
    {
        $query = Priority::with('district')
            ->with('village')
            ->orderByDate()
            ->filter(\Illuminate\Support\Facades\Request::only('search', 'date_start', 'date_end'));

        if (Role::isKecamatan(Auth::user()))
        {
            $query = $query->where('district_id', '=', Auth::user()->district_id);
        }

        return $query;
    }

    public function index(): \Inertia\Response
    {
        $ages = new PriorityCollection($this->queryList($this->queryListMain()));

        return Inertia::render('Priorities/Index', [
            'filters' => \Illuminate\Support\Facades\Request::all('search', 'date_start', 'date_end'),
            'priority' => $ages,
        ]);
    }

    public function create(): \Inertia\Response
    {
        return Inertia::render('Priorities/Create', [
            'districts' => new DistrictCollection(
                District::orderBy('name')->get()
            ),
            'villages' => new VillageCollection(
                Village::orderBy('name')->get()
            )
        ]);
    }

    public function store(PriorityStoreRequest $request): \Illuminate\Http\RedirectResponse
    {
        $data = Arr::add($request->validated(), 'created_by', Auth::user()->id);
        $data['date'] = date('Y-m-d H:i:s', strtotime($data['date']));

        if ($this->checkAlreadyCreatedByMonth($data))
        {
            return Redirect::back()->with('error', 'Prioritas bulan terpilih sudah ada.');
        }

        Priority::create($data);

        return Redirect::route('priorities')->with('success', 'Data Prioritas ditambahkan.');
    }

    private function checkAlreadyCreatedByMonth($post): bool
    {
        $exist = Priority::where('date', '=', $post['date'])
            ->where('district_id', '=', $post['district_id'])
            ->where('village_id', '=', $post['village_id'])
            ->get()->first();

        return isset($exist);
    }

    public function edit(Priority $priority): \Inertia\Response
    {
        return Inertia::render('Priorities/Edit', [
            'priority' => new PriorityResource($priority),
            'districts' => new DistrictCollection(
                District::orderBy('name')->get()
            ),
            'villages' => new VillageCollection(
                Village::orderBy('name')->get()
            )
        ]);
    }

    public function update(Priority $priority, PriorityUpdateRequest $request): \Illuminate\Http\RedirectResponse
    {
        $priority->update(
            $request->validated()
        );

        return Redirect::back()->with('success', 'Data Prioritas diperbarui.');
    }

    public function destroy(Priority $priority): \Illuminate\Http\RedirectResponse
    {
        $priority->delete();

        return Redirect::route('priorities')->with('success', 'Data Prioritas dihapus.');
    }

    public function export(): \Symfony\Component\HttpFoundation\BinaryFileResponse
    {
        return Excel::download(new PriorityExport($this->queryListMain()->get()->all(),
            $this->titleExport('Prioritas')),
            $this->titleExport('Prioritas'). '.xlsx');
    }
}
