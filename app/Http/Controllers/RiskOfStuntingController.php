<?php

namespace App\Http\Controllers;

use App\Exports\RiskOfStuntingExport;
use App\Exports\VillageProfileExport;
use App\Http\Requests\RiskOfStuntings\RiskOfStuntingStoreRequest;
use App\Http\Requests\RiskOfStuntings\RiskOfStuntingUpdateRequest;
use App\Http\Resources\Districts\DistrictCollection;
use App\Http\Resources\RiskOfStuntings\RiskOfStuntingCollection;
use App\Http\Resources\RiskOfStuntings\RiskOfStuntingResource;
use App\Http\Resources\Villages\VillageCollection;
use App\Models\District;
use App\Models\RiskOfStunting;
use App\Models\Village;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;
use Inertia\Inertia;
use Maatwebsite\Excel\Facades\Excel;

class RiskOfStuntingController extends Controller
{
    private function queryListMain()
    {
        return RiskOfStunting::with('village')
            ->with('district')
            ->orderByDate()
            ->filter(Request::only('search'));
    }

    public function index(): \Inertia\Response
    {
        return Inertia::render('RiskOfStuntings/Index', [
            'filters' => \Illuminate\Support\Facades\Request::all('search'),
            'risks' => new RiskOfStuntingCollection($this->queryList($this->queryListMain())),
        ]);
    }

    public function create(): \Inertia\Response
    {
        return Inertia::render('RiskOfStuntings/Create', [
            'districts' => new DistrictCollection(
                District::orderBy('name')->get()
            ),
            'villages' => new VillageCollection(
                Village::orderBy('name')->get()
            ),
            'opt_source_water' => RiskOfStunting::OPT_SOURCE_WATER,
            'opt_toilet' => RiskOfStunting::OPT_TOILET,
        ]);
    }

    public function store(RiskOfStuntingStoreRequest $request): \Illuminate\Http\RedirectResponse
    {
        $data = Arr::add($request->validated(), 'created_by', Auth::user()->id);
        RiskOfStunting::create($data);

        return Redirect::route('risk-of-stunting')->with('success', 'Data Keluarga Stunting ditambahkan.');
    }

    public function edit(RiskOfStunting $riskOfStunting): \Inertia\Response
    {
        return Inertia::render('RiskOfStuntings/Edit', [
            'risk' => new RiskOfStuntingResource($riskOfStunting),
            'districts' => new DistrictCollection(
                District::orderBy('name')->get()
            ),
            'villages' => new VillageCollection(
                Village::orderBy('name')->get()
            ),
            'opt_source_water' => RiskOfStunting::OPT_SOURCE_WATER,
            'opt_toilet' => RiskOfStunting::OPT_TOILET,
        ]);
    }

    public function update(RiskOfStunting $riskOfStunting, RiskOfStuntingUpdateRequest $request): \Illuminate\Http\RedirectResponse
    {
        $riskOfStunting->update(
            $request->validated()
        );

        return Redirect::back()->with('success', 'Data Keluarga Stunting diperbarui.');
    }

    public function destroy(RiskOfStunting $riskOfStunting): \Illuminate\Http\RedirectResponse
    {
        $riskOfStunting->delete();

        return Redirect::route('risk-of-stunting')->with('success', 'Data Keluarga Stunting dihapus.');
    }

    public function export(): \Symfony\Component\HttpFoundation\BinaryFileResponse
    {
        return Excel::download(new RiskOfStuntingExport($this->queryListMain()->get()->all(),
            $this->titleExport('Keluarga Resiko Stunting')),
            $this->titleExport('Keluarga Resiko Stunting'). '.xlsx');
    }
}
