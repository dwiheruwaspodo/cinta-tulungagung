<?php

namespace App\Http\Controllers;

use App\Exports\UnMeetNeedExport;
use App\Exports\UnMeetNeedKabupatenExport;
use App\Http\Requests\UnMeetNeeds\UnMeetNeedStoreRequest;
use App\Http\Requests\UnMeetNeeds\UnMeetNeedUpdateRequest;
use App\Http\Resources\Districts\DistrictCollection;
use App\Http\Resources\UnMeetNeeds\UnMeetNeedCollection;
use App\Http\Resources\UnMeetNeeds\UnMeetNeedKabupatenCollection;
use App\Http\Resources\UnMeetNeeds\UnMeetNeedResource;
use App\Http\Resources\Villages\VillageCollection;
use App\Models\District;
use App\Models\Role;
use App\Models\UnMeetNeed;
use App\Models\Village;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;
use Inertia\Inertia;
use Maatwebsite\Excel\Facades\Excel;

class UnMeetNeedController extends Controller
{
    private function queryListMain()
    {
        $query = UnMeetNeed::with('district')
            ->with('village')
            ->orderByDate()
            ->filter(\Illuminate\Support\Facades\Request::only('search', 'date_start', 'date_end'));

        if (Role::isKecamatan(Auth::user()))
        {
            $query = $query->where('district_id', '=', Auth::user()->district_id);
        }

        return $query;
    }

    private function queryKabupatenPov()
    {
        return UnMeetNeed::with('district')
            ->with('village')
            ->groupBy('district_id', 'month', 'year')
            ->select(
                DB::raw('sum(iat) as sum_iat'),
                DB::raw('sum(tial) as sum_tial'),
                DB::raw('sum(ias) as sum_ias'),
                DB::raw('sum(hamil) as sum_hamil'),
                DB::raw('YEAR(date) year, MONTH(date) month'),
                'district_id'
            );
    }

    public function index(): \Inertia\Response
    {
        if (Role::isKabupaten(Auth::user()))
        {
            return Inertia::render('UnMeetNeeds/IndexKabupaten', [
                'filters' => \Illuminate\Support\Facades\Request::all('search', 'date_start', 'date_end'),
                'unMeet' => new UnMeetNeedKabupatenCollection($this->queryList($this->queryKabupatenPov())),
            ]);
        }
        else
        {
            return Inertia::render('UnMeetNeeds/Index', [
                'filters' => \Illuminate\Support\Facades\Request::all('search', 'date_start', 'date_end'),
                'unMeet' => new UnMeetNeedCollection($this->queryList($this->queryListMain())),
            ]);
        }
    }

    public function create(): \Inertia\Response
    {
        return Inertia::render('UnMeetNeeds/Create', [
            'districts' => new DistrictCollection(
                District::orderBy('name')->get()
            ),
            'villages' => new VillageCollection(
                Village::orderBy('name')->get()
            )
        ]);
    }

    public function store(UnMeetNeedStoreRequest $request): \Illuminate\Http\RedirectResponse
    {
        $data = Arr::add($request->validated(), 'created_by', Auth::user()->id);
        $data['date'] = date('Y-m-d H:i:s', strtotime($data['date']));

        if ($this->checkAlreadyCreatedByMonth($data))
        {
            return Redirect::back()->with('error', 'Unmeet bulan terpilih sudah ada.');
        }

        UnMeetNeed::create($data);

        return Redirect::route('unmeet-needs')->with('success', 'Data Unmeet Needs ditambahkan.');
    }

    private function checkAlreadyCreatedByMonth($post): bool
    {
        $exist = UnMeetNeed::where('date', '=', $post['date'])
            ->where('district_id', '=', $post['district_id'])
            ->where('village_id', '=', $post['village_id'])
            ->get()->first();

        return isset($exist);
    }

    public function edit(UnMeetNeed $unMeetNeed): \Inertia\Response
    {
        return Inertia::render('UnMeetNeeds/Edit', [
            'unmeet' => new UnMeetNeedResource($unMeetNeed),
            'districts' => new DistrictCollection(
                District::orderBy('name')->get()
            ),
            'villages' => new VillageCollection(
                Village::orderBy('name')->get()
            )
        ]);
    }

    public function update(UnMeetNeed $unMeetNeed, UnMeetNeedUpdateRequest $request): \Illuminate\Http\RedirectResponse
    {
        $unMeetNeed->update(
            $request->validated()
        );

        return Redirect::back()->with('success', 'Data Unmeet Needs diperbarui.');
    }

    public function destroy(UnMeetNeed $unMeetNeed): \Illuminate\Http\RedirectResponse
    {
        $unMeetNeed->delete();

        return Redirect::route('unmeet-needs')->with('success', 'Data Unmeet Needs dihapus.');
    }

    public function export(): \Symfony\Component\HttpFoundation\BinaryFileResponse
    {
        if (Role::isKabupaten(Auth::user()))
        {
            return Excel::download(new UnMeetNeedKabupatenExport($this->queryKabupatenPov()->get()->all(),
                $this->titleExport('Unmeet Needs')),
                $this->titleExport('Unmeet Needs'). '.xlsx');
        }
        else {
            return Excel::download(new UnMeetNeedExport($this->queryListMain()->get()->all(),
                $this->titleExport('Unmeet Needs')),
                $this->titleExport('Unmeet Needs'). '.xlsx');
        }
    }
}
