<?php

namespace App\Http\Controllers;

use App\Http\Requests\Users\UserDeleteRequest;
use App\Http\Requests\Users\UserStoreRequest;
use App\Http\Requests\Users\UserUpdateRequest;
use App\Http\Resources\Districts\DistrictCollection;
use App\Http\Resources\Users\UserCollection;
use App\Http\Resources\Users\UserResource;
use App\Models\District;
use App\Models\Role;
use App\Models\User;
use App\Models\UserRole;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;
use Inertia\Inertia;

class UsersController extends Controller
{
    public function index(): \Inertia\Response
    {
        return Inertia::render('Users/Index', [
            'filters' => Request::all('search', 'trashed'),
            'users' => new UserCollection(
                User::with('role')
                    ->with('district')
                    ->orderByName()
                    ->filter(Request::only('search', 'trashed'))
                    ->paginate()
                    ->appends(Request::all())
            ),
        ]);
    }

    public function create(): \Inertia\Response
    {
        return Inertia::render('Users/Create', [
            'districts' => new DistrictCollection(District::orderBy('name')->get()),
            'roles' => new DistrictCollection(Role::all())
        ]);
    }

    public function store(UserStoreRequest $request): \Illuminate\Http\RedirectResponse
    {
        $user = Arr::add($request->validated(), 'created_by', Auth::user()->id);
        User::create($user);

        return Redirect::route('users')->with('success', 'User ditambahkan.');
    }

    public function edit(User $user): \Inertia\Response
    {
        return Inertia::render('Users/Edit', [
            'user' => new UserResource($user),
            'districts' => new DistrictCollection(District::orderBy('name')->get()),
            'roles' => new DistrictCollection(Role::all())
        ]);
    }

    public function update(User $user, UserUpdateRequest $request): \Illuminate\Http\RedirectResponse
    {
        $payload = $request->validated();
        if (empty($request->validated()['district_id'])) $payload['district_id'] = null;

        $user->update($payload);

        return Redirect::back()->with('success', 'User updated.');
    }

    public function destroy(User $user, UserDeleteRequest $request): \Illuminate\Http\RedirectResponse
    {
        // update with mark delete
        $user->email = 'deleted-'.time().'-'.$user->email;
        $user->save();
        $user->delete();

        return Redirect::route('users')->with('success', 'User deleted.');
    }

    public function restore(User $user): \Illuminate\Http\RedirectResponse
    {
        $user->restore();

        return Redirect::route('users')->with('success', 'User restored.');
    }
}
