<?php

namespace App\Http\Controllers;
use App\Exports\VillageProfileExport;
use App\Http\Requests\VillageProfiles\VillageProfileStoreRequest;
use App\Http\Requests\VillageProfiles\VillageProfileUpdateRequest;
use App\Http\Resources\Districts\DistrictCollection;
use App\Http\Resources\VillageProfiles\VillageProfileCollection;
use App\Http\Resources\VillageProfiles\VillageProfileResource;
use App\Http\Resources\Villages\VillageCollection;
use App\Models\District;
use App\Models\Village;
use App\Models\VillageProfile;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;
use Inertia\Inertia;
use Maatwebsite\Excel\Facades\Excel;

class VillageProfileController extends Controller
{
    private function queryListMain()
    {
        return VillageProfile::with('village')
            ->orderByDate()
            ->filter(Request::only('search'));
    }

    public function index(): \Inertia\Response
    {
        return Inertia::render('VillageProfiles/Index', [
            'filters' => \Illuminate\Support\Facades\Request::all('search'),
            'villages' => new VillageProfileCollection($this->queryList($this->queryListMain())),
        ]);
    }

    public function create(): \Inertia\Response
    {
        return Inertia::render('VillageProfiles/Create', [
            'villages' => new VillageCollection(
                Village::orderBy('name')->get()
            ),
            'districts' => new DistrictCollection(
                District::orderBy('name')->get()
            ),
            'opt_kampung_kb' => VillageProfile::OPT_KAMPUNG_KB,
            'opt_rumah_dataku' => VillageProfile::OPT_RUMAH_DATAKU,
            'opt_desa_membangun' => VillageProfile::OPT_DESA_MEMBANGUN,
            'opt_potensi' => VillageProfile::OPT_POTENSI,
        ]);
    }

    public function store(VillageProfileStoreRequest $request): \Illuminate\Http\RedirectResponse
    {
        $profile = Arr::add($request->validated(), 'created_by', Auth::user()->id);
        VillageProfile::create($profile);

        return Redirect::route('village-profiles')->with('success', 'Profil Desa ditambahkan.');
    }

    public function edit(VillageProfile $villageProfile): \Inertia\Response
    {
        return Inertia::render('VillageProfiles/Edit', [
            'profile' => new VillageProfileResource($villageProfile),
            'villages' => new VillageCollection(
                Village::orderBy('name')->get()
            ),
            'districts' => new DistrictCollection(
                District::orderBy('name')->get()
            ),
            'opt_kampung_kb' => VillageProfile::OPT_KAMPUNG_KB,
            'opt_rumah_dataku' => VillageProfile::OPT_RUMAH_DATAKU,
            'opt_desa_membangun' => VillageProfile::OPT_DESA_MEMBANGUN,
            'opt_potensi' => VillageProfile::OPT_POTENSI,
        ]);
    }

    public function update(VillageProfile $villageProfile, VillageProfileUpdateRequest $request): \Illuminate\Http\RedirectResponse
    {
        $villageProfile->update(
            $request->validated()
        );

        return Redirect::back()->with('success', 'Profil Desa diperbarui.');
    }

    public function destroy(VillageProfile $villageProfile): \Illuminate\Http\RedirectResponse
    {
        $villageProfile->delete();

        return Redirect::route('village-profiles')->with('success', 'Profil Desa dihapus.');
    }

    public function export(): \Symfony\Component\HttpFoundation\BinaryFileResponse
    {
        return Excel::download(new VillageProfileExport($this->queryListMain()->get()->all(),
            $this->titleExport('Profil Desa')),
            $this->titleExport('Profil Desa'). '.xlsx');
    }
}
