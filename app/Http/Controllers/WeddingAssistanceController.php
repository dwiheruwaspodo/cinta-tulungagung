<?php

namespace App\Http\Controllers;

use App\Exports\WeddingAssistanceExport;
use App\Http\Requests\WeddingAssistances\WeddingAssistanceStoreRequest;
use App\Http\Requests\WeddingAssistances\WeddingAssistanceUpdateRequest;
use App\Http\Resources\Districts\DistrictCollection;
use App\Http\Resources\Villages\VillageCollection;
use App\Http\Resources\WeddingAssistances\WeddingAssistanceCollection;
use App\Http\Resources\WeddingAssistances\WeddingAssistanceResource;
use App\Models\District;
use App\Models\Village;
use App\Models\WeddingAssistance;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;
use Inertia\Inertia;
use Maatwebsite\Excel\Facades\Excel;

class WeddingAssistanceController extends Controller
{
    private function queryListMain()
    {
        return WeddingAssistance::with('village')
            ->with('district')
            ->orderByDate()
            ->filter(Request::only('search', 'date_start', 'date_end'));
    }

    public function index(): \Inertia\Response
    {
        return Inertia::render('WeddingAssistances/Index', [
            'filters' => \Illuminate\Support\Facades\Request::all('search', 'date_start', 'date_end'),
            'weddings' => new WeddingAssistanceCollection($this->queryList($this->queryListMain())),
        ]);
    }

    public function create(): \Inertia\Response
    {
        return Inertia::render('WeddingAssistances/Create', [
            'districts' => new DistrictCollection(
                District::orderBy('name')->get()
            ),
            'villages' => new VillageCollection(
                Village::orderBy('name')->get()
            )
        ]);
    }

    public function store(WeddingAssistanceStoreRequest $request): \Illuminate\Http\RedirectResponse
    {
        $input = Arr::add($request->validated(), 'created_by', Auth::user()->id);

        if ($input['gender'] == 1)
        {
            unset($input['anemia']);
            unset($input['lila']);
            unset($input['imt']);
        }

        WeddingAssistance::create($input);

        return Redirect::route('wedding-asisstances')->with('success', 'Pendampingan Nikah ditambahkan.');
    }

    public function edit(WeddingAssistance $weddingAssistance): \Inertia\Response
    {
        return Inertia::render('WeddingAssistances/Edit', [
            'wedding' => new WeddingAssistanceResource($weddingAssistance),
            'districts' => new DistrictCollection(
                District::orderBy('name')->get()
            ),
            'villages' => new VillageCollection(
                Village::orderBy('name')->get()
            )
        ]);
    }

    public function update(WeddingAssistance $weddingAssistance, WeddingAssistanceUpdateRequest $request): \Illuminate\Http\RedirectResponse
    {
        $input = $request->validated();

        if ($input['gender'] == 1)
        {
            $input['anemia'] = null;
            $input['lila'] = null;
            $input['imt'] = null;
        }

        $weddingAssistance->update($input);

        return Redirect::back()->with('success', 'Pendampingan Nikah diperbarui.');
    }

    public function destroy(WeddingAssistance $weddingAssistance): \Illuminate\Http\RedirectResponse
    {
        $weddingAssistance->delete();

        return Redirect::route('wedding-asisstances')->with('success', 'Pendampingan Nikah dihapus.');
    }

    public function export(): \Symfony\Component\HttpFoundation\BinaryFileResponse
    {
        return Excel::download(new WeddingAssistanceExport($this->queryListMain()->get()->all(),
            $this->titleExport('Pendampingan Nikah')),
            $this->titleExport('Pendampingan Nikah'). '.xlsx');
    }
}
