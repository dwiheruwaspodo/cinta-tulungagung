<?php

namespace App\Http\Middleware;

use App\Http\Resources\Users\UserResource;
use App\Models\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Inertia\Middleware;

class HandleInertiaRequests extends Middleware
{
    /**
     * The root template that's loaded on the first page visit.
     *
     * @see https://inertiajs.com/server-side-setup#root-template
     * @var string
     */
    protected $rootView = 'app';

    /**
     * Determines the current asset version.
     *
     * @see https://inertiajs.com/asset-versioning
     * @param  \Illuminate\Http\Request  $request
     * @return string|null
     */
    public function version(Request $request)
    {
        return parent::version($request);
    }

    /**
     * Defines the props that are shared by default.
     *
     * @see https://inertiajs.com/shared-data
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function share(Request $request): array
    {
        $currentUser = Auth::check() ? new UserResource(Auth::user()->load('role')->load('district')) : null;


        return array_merge(parent::share($request), [
            'auth' => function () use ($currentUser) {
                return [
                    'user' => $currentUser,
                    'isAdmin' => ($currentUser) ? Role::isAdmin(Auth::user()) : null,
                    'isKB' => ($currentUser) ? Role::isKB(Auth::user()) : null,
                    'isKS' => ($currentUser) ? Role::isKS(Auth::user()) : null,
                    'isPKK' => ($currentUser) ? Role::isPKK(Auth::user()) : null,
                    'isKecamatan' => ($currentUser) ? Role::isKecamatan(Auth::user()) : null,
                    'isKabupaten' => ($currentUser) ? Role::isKabupaten(Auth::user()) : null,
                ];
            },
            'flash' => function () use ($request) {
                return [
                    'success' => $request->session()->get('success'),
                    'error' => $request->session()->get('error'),
                ];
            },
        ]);
    }
}
