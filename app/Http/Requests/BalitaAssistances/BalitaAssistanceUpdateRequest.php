<?php

namespace App\Http\Requests\BalitaAssistances;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class BalitaAssistanceUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'date' => ['required'],
            'name' => ['required'],
            'gender' => ['required'],
            'posyandu' => ['required'],
            'age' => ['required'],
            'weight' => ['required'],
            'height' => ['required'],
            'parent_name' => ['required'],
            'gakin' => ['required'],
            'status' => ['required'],
            'district_id' => ['required', Rule::exists('districts', 'id')],
            'village_id' => ['required', Rule::exists('villages', 'id')],
        ];
    }
}
