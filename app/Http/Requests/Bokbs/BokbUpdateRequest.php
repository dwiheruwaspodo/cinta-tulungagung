<?php

namespace App\Http\Requests\Bokbs;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class BokbUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'district_id' => ['required', Rule::exists('districts', 'id')],
            'village_id' => ['required', Rule::exists('villages', 'id')],
            'date' => ['required'],
            'activity' => ['required'],
            'location' => ['required'],
            'link_document' => ['required'],
            'source_person' => ['required'],
            'source_person_from' => ['required'],
            'warung' => ['required']
        ];
    }
}
