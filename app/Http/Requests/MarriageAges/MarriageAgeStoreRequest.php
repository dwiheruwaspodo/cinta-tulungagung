<?php

namespace App\Http\Requests\MarriageAges;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class MarriageAgeStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'district_id' => ['required', Rule::exists('districts', 'id')],
            'village_id' => ['required', Rule::exists('villages', 'id')],
            'date' => ['required'],
            'less_20' => ['required', 'numeric'],
            'age_21_25' => ['required', 'numeric'],
            'age_26_30' => ['required', 'numeric'],
            'more_30' => ['required', 'numeric']

        ];
    }
}
