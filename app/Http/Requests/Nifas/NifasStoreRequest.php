<?php

namespace App\Http\Requests\Nifas;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class NifasStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'district_id' => ['required', Rule::exists('districts', 'id')],
            'village_id' => ['required', Rule::exists('villages', 'id')],
            'date' => ['required'],
            'nik' => ['required', 'digits:16'],
            'name' => ['required'],
            'birthday' => ['required', 'before:today'],
            'born' => ['required', 'before:tomorrow'],
            'is_contraception' => ['required'],
            'contraception' => ['required_if:is_contraception,==,1'],
            'description' => ['required'],
        ];
    }
}
