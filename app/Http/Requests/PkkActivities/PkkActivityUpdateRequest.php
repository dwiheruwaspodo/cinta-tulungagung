<?php

namespace App\Http\Requests\PkkActivities;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class PkkActivityUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'pkk_activity_category_id' => ['required', Rule::exists('pkk_activity_categories', 'id')],
            'district_id' => [
                'sometimes',
                'required_if:level,==,kecamatan',
            ],
            'village_id' => [
                'sometimes',
                'required_if:level,==,desa',
            ],
            'date' => ['required', 'before:today'],
            'description' => ['required', 'max:255'],
            'source_person' => ['required', 'max:150'],
            'source_person_from' => ['required', 'max:150'],
            'target' => ['required'],
            'target_amount' => ['required', 'max:100', 'min:1'],
            'location' => ['required', 'max:25'],
            'level' => ['required', 'in:kabupaten,kecamatan,desa'],
            'photo' => ['nullable', 'image']
        ];
    }
}
