<?php

namespace App\Http\Requests\PkkRegulations;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class PkkRegulationUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'date' => ['required', 'before:tomorrow'],
            'number' => ['required', 'max:100',
                Rule::unique('pkk_regulations')->ignore($this->route('pkkRegulation')->id)
            ],
            'name' => ['required', 'max:255'],
            'description' => ['required', 'max:255'],
            'issued_by' => ['required', 'max:150']
        ];
    }
}
