<?php

namespace App\Http\Requests\PkkSchedules;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class PkkScheduleUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'pkk_activity_category_id' => ['required', Rule::exists('pkk_activity_categories', 'id')],
            'district_id' => ['required', Rule::exists('districts', 'id')],
            'village_id' => ['required', Rule::exists('villages', 'id')],
            'date' => ['required', 'after:today'],
            'description' => ['required', 'max:255'],
            'location' => ['required', 'max:25'],
        ];
    }
}
