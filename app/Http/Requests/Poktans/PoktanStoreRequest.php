<?php

namespace App\Http\Requests\Poktans;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class PoktanStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'district_id' => ['required', Rule::exists('districts', 'id')],
            'village_id' => ['required', Rule::exists('villages', 'id')],
            'date' => ['required'],
            'keluarga_bkb' => ['required', 'numeric'],
            'anggota_hadir_bkb' => ['required', 'numeric'],
            'keluarga_bkr' => ['required', 'numeric'],
            'anggota_hadir_bkr' => ['required', 'numeric'],
            'keluarga_bkl' => ['required', 'numeric'],
            'anggota_hadir_bkl' => ['required', 'numeric'],
            'keluarga_uppks' => ['required', 'numeric'],
            'anggota_hadir_uppks' => ['required', 'numeric'],
            'keluarga_pikr' => ['required', 'numeric'],
            'anggota_hadir_pikr' => ['required', 'numeric'],

        ];
    }
}
