<?php

namespace App\Http\Requests\PostBirths;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class PostBirthUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'district_id' => ['required', Rule::exists('districts', 'id')],
            'village_id' => ['required', Rule::exists('villages', 'id')],
            'date' => ['required'],
            'birth_suntik' => ['required', 'numeric'],
            'birth_pil' => ['required', 'numeric'],
            'birth_kondom' => ['required', 'numeric'],
            'birth_iud' => ['required', 'numeric'],
            'birth_implan' => ['required', 'numeric'],
            'birth_mop' => ['required', 'numeric'],
            'birth_mow' => ['required', 'numeric'],
            'miscarriage_suntik' => ['required', 'numeric'],
            'miscarriage_pil' => ['required', 'numeric'],
            'miscarriage_kondom' => ['required', 'numeric'],
            'miscarriage_iud' => ['required', 'numeric'],
            'miscarriage_implan' => ['required', 'numeric'],
            'miscarriage_mop' => ['required', 'numeric'],
            'miscarriage_mow' => ['required', 'numeric'],

        ];
    }
}
