<?php

namespace App\Http\Requests\Pregnancies;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class PregnancyStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'date' => ['required'],
            'nik' => ['required', 'digits:16'],
            'name' => ['required'],
            'birthday' => ['required', 'before:today'],
            'description' => ['required'],
            'pregnancy_age' => ['required', 'numeric', 'min:1' ,'max:9'],
            'kek' => ['required'],
            'stunting' => ['required'],
            'inhibited_fetal_growth' => ['required'],
            'four_too' => ['required'],
            'anemia' => ['required'],
            'district_id' => ['required', Rule::exists('districts', 'id')],
            'village_id' => ['required', Rule::exists('villages', 'id')],
        ];
    }
}
