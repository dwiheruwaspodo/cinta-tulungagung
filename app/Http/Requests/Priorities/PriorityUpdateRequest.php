<?php

namespace App\Http\Requests\Priorities;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class PriorityUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'district_id' => ['required', Rule::exists('districts', 'id')],
            'village_id' => ['required', Rule::exists('villages', 'id')],
            'date' => ['required'],
            'ssk' => ['required', 'numeric'],
            'dahsat' => ['required', 'numeric'],
            'kampung_kb' => ['required', 'numeric'],
            'rumah_dataku' => ['required', 'numeric']
        ];
    }
}
