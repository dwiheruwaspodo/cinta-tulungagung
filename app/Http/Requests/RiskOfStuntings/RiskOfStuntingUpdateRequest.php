<?php

namespace App\Http\Requests\RiskOfStuntings;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class RiskOfStuntingUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'rw' => ['required'],
            'rt' => ['required'],
            'head_of_family' => ['required'],
            'baduta' => ['required'],
            'balita' => ['required'],
            'pus' => ['required'],
            'pus_hamil' => ['required'],
            'source_of_water' => ['required'],
            'toilet' => ['required'],
            'too_young' => ['required'],
            'too_old' => ['required'],
            'district_id' => ['required', Rule::exists('districts', 'id')],
            'village_id' => ['required', Rule::exists('villages', 'id')],
            'too_near' => ['required'],
            'too_tight' => ['required'],
            'risk' => ['required'],
        ];
    }
}
