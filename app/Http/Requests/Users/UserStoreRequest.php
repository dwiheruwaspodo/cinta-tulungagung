<?php

namespace App\Http\Requests\Users;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UserStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => ['required', 'max:50'],
            'last_name' => ['required', 'max:50'],
            'email' => ['required', 'max:50', 'email', Rule::unique('users')->where(function ($query) {
                return $query->whereNull('deleted_at');
            })],
            'password' => ['nullable'],
            'role_id' => ['required', Rule::exists('roles', 'id')],
            'district_id' => ['nullable', Rule::exists('districts', 'id')],
            'photo' => ['nullable', 'image'],
        ];
    }

}
