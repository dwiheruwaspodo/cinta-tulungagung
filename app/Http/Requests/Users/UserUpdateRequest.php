<?php

namespace App\Http\Requests\Users;

use App\Traits\AdminUser;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UserUpdateRequest extends FormRequest
{
    use AdminUser;

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => ['required', 'max:50'],
            'last_name' => ['required', 'max:50'],
            'email' => ['required', 'max:50', 'email',
                Rule::unique('users')->whereNull('deleted_at')->ignore($this->route('user')->id)
            ],
            'password' => ['nullable'],
            'role_id' => ['required', Rule::exists('roles', 'id')],
            'district_id' => ['nullable', Rule::exists('districts', 'id')],
            'photo' => ['nullable', 'image'],
        ];
    }

}
