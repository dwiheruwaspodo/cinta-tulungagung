<?php

namespace App\Http\Requests\VillageProfiles;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class VillageProfileStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'headman' => ['required', 'max:100'],
            'kampung_kb' => ['required'],
            'desa_membangun' => ['required'],
            'rumah_dataku' => ['required'],
            'potensi' => ['required'],
            'dapur_stunting' => ['required'],
            'district_id' => ['required', Rule::exists('districts', 'id')],
            'village_id' => ['required', Rule::exists('villages', 'id')],
            'attach_kampung_kb' => ['nullable'],
            'attach_rumah_dataku' => ['nullable']
        ];
    }
}
