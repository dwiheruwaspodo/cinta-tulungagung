<?php

namespace App\Http\Requests\WeddingAssistances;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class WeddingAssistanceUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'date' => ['required'],
            'nik' => ['required', 'digits:16'],
            'name' => ['required'],
            'gender' => ['required'],
            'birthday' => ['required', 'before:today'],
            'to' => ['required'],
            'smoking' => ['required'],
            'anemia' => [''],
            'lila' => [''],
            'imt' => [''],
            'stunting' => [''],
            'district_id' => ['required', Rule::exists('districts', 'id')],
            'village_id' => ['required', Rule::exists('villages', 'id')],
        ];
    }
}
