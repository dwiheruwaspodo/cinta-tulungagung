<?php

namespace App\Http\Resources\BalitaAssistances;

use Illuminate\Http\Resources\Json\ResourceCollection;

class BalitaAssistanceCollection extends ResourceCollection
{
    public function toArray($request)
    {
        return $this->collection->map->only('id', 'date', 'name', 'gender', 'posyandu', 'age', 'weight', 'height',
            'parent_name', 'gakin', 'status', 'village', 'district', 'village_id', 'district_id');
    }
}
