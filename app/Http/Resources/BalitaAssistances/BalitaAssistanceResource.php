<?php

namespace App\Http\Resources\BalitaAssistances;

use Illuminate\Http\Resources\Json\JsonResource;

class BalitaAssistanceResource extends JsonResource
{

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'date' => $this->date,
            'name' => $this->name,
            'gender' => $this->gender,
            'posyandu' => $this->posyandu,
            'age' => $this->age,
            'weight' => $this->weight,
            'height' => $this->height,
            'parent_name' => $this->parent_name,
            'gakin' => $this->gakin,
            'status' => $this->status,
            'village_id' => $this->village_id,
            'district_id' => $this->district_id,
        ];
    }
}
