<?php

namespace App\Http\Resources\Bokbs;

use Illuminate\Http\Resources\Json\ResourceCollection;

class BokbCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return $this->collection->map->only(
            'id','district_id', 'village_id', 'date', 'created_by', 'activity', 'location', 'link_document',
            'source_person', 'source_person_from', 'warung', 'village', 'district',
        );
    }
}
