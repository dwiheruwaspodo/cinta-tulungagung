<?php

namespace App\Http\Resources\Bokbs;

use Illuminate\Http\Resources\Json\JsonResource;

class BokbResource extends JsonResource
{

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'district_id' => $this->district_id,
            'village_id' => $this->village_id,
            'date' => $this->date,
            'created_by' => $this->created_by,
            'activity' => $this->activity,
            'location' => $this->location,
            'link_document' => $this->link_document,
            'source_person' => $this->source_person,
            'source_person_from' => $this->source_person_from,
            'warung' => $this->warung
        ];
    }
}
