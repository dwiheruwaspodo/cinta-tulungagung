<?php

namespace App\Http\Resources\MarriageAges;

use Illuminate\Http\Resources\Json\ResourceCollection;

class MarriageAgeCollection extends ResourceCollection
{
    public function toArray($request)
    {
        return $this->collection->map->only('id', 'created_by', 'district_id', 'village_id', 'date',
            'created_by', 'less_20', 'age_21_25', 'age_26_30', 'more_30', 'village', 'district');
    }
}
