<?php

namespace App\Http\Resources\MarriageAges;

use Illuminate\Http\Resources\Json\ResourceCollection;

class MarriageAgeKabupatenCollection extends ResourceCollection
{
    public function toArray($request)
    {
        return $this->collection->map->only('id', 'created_by', 'district_id', 'village_id', 'date',
            'created_by', 'sum_less_20', 'sum_age_21_25', 'sum_age_26_30', 'sum_more_30', 'village', 'district', 'month', 'year');
    }
}
