<?php

namespace App\Http\Resources\MarriageAges;

use Illuminate\Http\Resources\Json\JsonResource;

class MarriageAgeResource extends JsonResource
{

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'district_id' => $this->district_id,
            'village_id' => $this->village_id,
            'date' => $this->date,
            'created_by' => $this->created_by,
            'less_20' => $this->less_20,
            'age_21_25' => $this->age_21_25,
            'age_26_30' => $this->age_26_30,
            'more_30' => $this->more_30
        ];
    }
}
