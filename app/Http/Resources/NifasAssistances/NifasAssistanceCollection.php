<?php

namespace App\Http\Resources\NifasAssistances;

use Illuminate\Http\Resources\Json\ResourceCollection;

class NifasAssistanceCollection extends ResourceCollection
{
    public function toArray($request)
    {
        return $this->collection->map->only('id', 'date', 'nik', 'name', 'born', 'birthday', 'created_by',
            'is_contraception', 'contraception', 'description', 'village', 'district', 'village_id', 'district_id');
    }
}
