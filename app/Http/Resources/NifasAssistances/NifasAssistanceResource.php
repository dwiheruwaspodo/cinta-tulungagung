<?php

namespace App\Http\Resources\NifasAssistances;

use Illuminate\Http\Resources\Json\JsonResource;

class NifasAssistanceResource extends JsonResource
{

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'date' => $this->date,
            'nik' => $this->nik,
            'name' => $this->name,
            'born' => $this->born,
            'birthday' => $this->birthday,
            'created_by' => $this->created_by,
            'is_contraception' => $this->is_contraception,
            'contraception' => $this->contraception,
            'description' => $this->description,
            'district_id' => $this->district_id,
            'village_id' => $this->village_id,
        ];
    }
}
