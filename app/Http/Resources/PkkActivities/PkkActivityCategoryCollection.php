<?php

namespace App\Http\Resources\PkkActivities;

use Illuminate\Http\Resources\Json\ResourceCollection;

class PkkActivityCategoryCollection extends ResourceCollection
{
    public function toArray($request)
    {
        return $this->collection->map->only('id', 'category');
    }
}
