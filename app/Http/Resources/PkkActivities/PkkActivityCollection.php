<?php

namespace App\Http\Resources\PkkActivities;

use Illuminate\Http\Resources\Json\ResourceCollection;

class PkkActivityCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return $this->collection->map->only(
            'id', 'date', 'source_person', 'source_person_from', 'location', 'district', 'village', 'description',
            'pkk_activity_category', 'photo_url', 'photo', 'target', 'target_amount', 'level'
        );
    }
}
