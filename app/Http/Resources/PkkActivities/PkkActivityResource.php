<?php

namespace App\Http\Resources\PkkActivities;

use Illuminate\Http\Resources\Json\JsonResource;

class PkkActivityResource extends JsonResource
{

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'date' => $this->date,
            'source_person' => $this->source_person,
            'source_person_from' => $this->source_person_from,
            'location' => $this->location,
            'district_id' => $this->district_id,
            'village_id' => $this->village_id,
            'description' => $this->description,
            'pkk_activity_category_id' => $this->pkk_activity_category_id,
            'target' => $this->target,
            'target_amount' => $this->target_amount,
            'level' => $this->level,
            'photo' => $this->photo,
        ];
    }
}
