<?php

namespace App\Http\Resources\PkkRegulations;

use Illuminate\Http\Resources\Json\ResourceCollection;

class PkkRegulationCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return $this->collection->map->only(
            'id', 'date', 'name', 'number', 'description', 'created_by', 'issued_by'
        );
    }
}
