<?php

namespace App\Http\Resources\PkkRegulations;

use Illuminate\Http\Resources\Json\JsonResource;

class PkkRegulationResource extends JsonResource
{

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'date' => $this->date,
            'number' => $this->number,
            'name' => $this->name,
            'description' => $this->description,
            'issued_by' => $this->issued_by
        ];
    }
}
