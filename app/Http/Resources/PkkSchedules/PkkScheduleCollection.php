<?php

namespace App\Http\Resources\PkkSchedules;

use Illuminate\Http\Resources\Json\ResourceCollection;

class PkkScheduleCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return $this->collection->map->only(
            'id', 'date', 'location', 'district', 'village', 'description', 'pkk_activity_category'
        );
    }
}
