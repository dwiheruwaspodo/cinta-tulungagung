<?php

namespace App\Http\Resources\PkkSchedules;

use Illuminate\Http\Resources\Json\JsonResource;

class PkkScheduleResource extends JsonResource
{

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'date' => $this->date,
            'location' => $this->location,
            'district_id' => $this->district_id,
            'village_id' => $this->village_id,
            'description' => $this->description,
            'pkk_activity_category_id' => $this->pkk_activity_category_id
        ];
    }
}
