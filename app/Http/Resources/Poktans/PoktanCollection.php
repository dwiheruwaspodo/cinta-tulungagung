<?php

namespace App\Http\Resources\Poktans;

use Illuminate\Http\Resources\Json\ResourceCollection;

class PoktanCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return $this->collection->map->only(
            'id', 'date', 'location', 'district', 'village', 'description', 'keluarga_bkb', 'anggota_hadir_bkb',
            'keluarga_bkr', 'anggota_hadir_bkr', 'keluarga_bkl', 'anggota_hadir_bkl', 'keluarga_uppks',
            'anggota_hadir_uppks', 'keluarga_pikr', 'anggota_hadir_pikr'
        );
    }
}
