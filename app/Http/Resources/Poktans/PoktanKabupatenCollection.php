<?php

namespace App\Http\Resources\Poktans;

use Illuminate\Http\Resources\Json\ResourceCollection;

class PoktanKabupatenCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return $this->collection->map->only(
            'id', 'date', 'location', 'district', 'village', 'description', 'sum_keluarga_bkb', 'sum_anggota_hadir_bkb',
            'sum_keluarga_bkr', 'sum_anggota_hadir_bkr', 'sum_keluarga_bkl', 'sum_anggota_hadir_bkl', 'sum_keluarga_uppks',
            'sum_anggota_hadir_uppks', 'sum_keluarga_pikr', 'sum_anggota_hadir_pikr', 'year', 'month'
        );
    }
}
