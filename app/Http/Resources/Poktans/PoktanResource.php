<?php

namespace App\Http\Resources\Poktans;

use Illuminate\Http\Resources\Json\JsonResource;

class PoktanResource extends JsonResource
{

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'date' => $this->date,
            'location' => $this->location,
            'district_id' => $this->district_id,
            'village_id' => $this->village_id,
            'keluarga_bkb' => $this->keluarga_bkb,
            'anggota_hadir_bkb' => $this->anggota_hadir_bkb,
            'keluarga_bkr' => $this->keluarga_bkr,
            'anggota_hadir_bkr' => $this->anggota_hadir_bkr,
            'keluarga_bkl' => $this->keluarga_bkl,
            'anggota_hadir_bkl' => $this->anggota_hadir_bkl,
            'keluarga_uppks' => $this->keluarga_uppks,
            'anggota_hadir_uppks' => $this->anggota_hadir_uppks,
            'keluarga_pikr' => $this->keluarga_pikr,
            'anggota_hadir_pikr' => $this->anggota_hadir_pikr
        ];
    }
}
