<?php

namespace App\Http\Resources\PostBirths;

use Illuminate\Http\Resources\Json\ResourceCollection;

class PostBirthCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return $this->collection->map->only(
            'id', 'date', 'district', 'village', 'birth_suntik', 'birth_pil', 'birth_kondom', 'birth_iud',
            'birth_implan', 'birth_mop', 'birth_mow', 'miscarriage_suntik', 'miscarriage_pil', 'miscarriage_kondom',
            'miscarriage_iud', 'miscarriage_implan', 'miscarriage_mop', 'miscarriage_mow', 'created_by'
        );
    }
}
