<?php

namespace App\Http\Resources\PostBirths;

use Illuminate\Http\Resources\Json\ResourceCollection;

class PostBirthKabupatenCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return $this->collection->map->only(
            'id', 'date', 'district', 'village', 'created_by', 'sum_birth_suntik', 'sum_birth_pil',
            'sum_birth_kondom', 'sum_birth_iud', 'sum_birth_implan', 'sum_birth_mop', 'sum_birth_mow',
            'sum_miscarriage_suntik', 'sum_miscarriage_pil', 'sum_miscarriage_kondom', 'sum_miscarriage_iud',
            'sum_miscarriage_implan', 'sum_miscarriage_mop','sum_miscarriage_mow', 'month', 'year'
        );
    }
}
