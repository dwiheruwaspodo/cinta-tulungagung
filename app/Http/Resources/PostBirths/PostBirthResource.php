<?php

namespace App\Http\Resources\PostBirths;

use Illuminate\Http\Resources\Json\JsonResource;

class PostBirthResource extends JsonResource
{

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'date' => $this->date,
            'district_id' => $this->district_id,
            'village_id' => $this->village_id,
            'birth_suntik' => $this->birth_suntik,
            'birth_pil' => $this->birth_pil,
            'birth_kondom' => $this->birth_kondom,
            'birth_iud' => $this->birth_iud,
            'birth_implan' => $this->birth_implan,
            'birth_mop' => $this->birth_mop,
            'birth_mow' => $this->birth_mow,
            'miscarriage_suntik' => $this->miscarriage_suntik,
            'miscarriage_pil' => $this->miscarriage_pil,
            'miscarriage_kondom' => $this->miscarriage_kondom,
            'miscarriage_iud' => $this->miscarriage_iud,
            'miscarriage_implan' => $this->miscarriage_implan,
            'miscarriage_mop' => $this->miscarriage_mop,
            'miscarriage_mow' => $this->miscarriage_mow,
        ];
    }
}
