<?php

namespace App\Http\Resources\PregnancyAssistances;

use Illuminate\Http\Resources\Json\ResourceCollection;

class PregnancyAssistanceCollection extends ResourceCollection
{
    public function toArray($request)
    {
        return $this->collection->map->only('id', 'date', 'nik', 'name', 'birthday', 'created_by', 'description',
            'village_id', 'district_id', 'pregnancy_age', 'kek', 'stunting', 'inhibited_fetal_growth',
            'four_too', 'anemia', 'village', 'district');
    }
}
