<?php

namespace App\Http\Resources\PregnancyAssistances;

use Illuminate\Http\Resources\Json\JsonResource;

class PregnancyAssistanceResource extends JsonResource
{

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'date' => $this->date,
            'nik' => $this->nik,
            'name' => $this->name,
            'birthday' => $this->birthday,
            'created_by' => $this->created_by,
            'description' => $this->description,
            'village_id' => $this->village_id,
            'district_id' => $this->district_id,
            'pregnancy_age' => $this->pregnancy_age,
            'kek' => $this->kek,
            'stunting' => $this->stunting,
            'inhibited_fetal_growth' => $this->inhibited_fetal_growth,
            'four_too' => $this->four_too,
            'anemia' => $this->anemia,
        ];
    }
}
