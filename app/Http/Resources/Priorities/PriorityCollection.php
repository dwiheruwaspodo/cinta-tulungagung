<?php

namespace App\Http\Resources\Priorities;

use Illuminate\Http\Resources\Json\ResourceCollection;

class PriorityCollection extends ResourceCollection
{
    public function toArray($request)
    {
        return $this->collection->map->only('id', 'created_by', 'district_id', 'village_id', 'date',
            'created_by', 'ssk', 'dahsat', 'kampung_kb', 'rumah_dataku', 'village', 'district');
    }
}
