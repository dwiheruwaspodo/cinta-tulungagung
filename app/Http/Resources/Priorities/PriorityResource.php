<?php

namespace App\Http\Resources\Priorities;

use Illuminate\Http\Resources\Json\JsonResource;

class PriorityResource extends JsonResource
{

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'district_id' => $this->district_id,
            'village_id' => $this->village_id,
            'date' => $this->date,
            'created_by' => $this->created_by,
            'ssk' => $this->ssk,
            'dahsat' => $this->dahsat,
            'kampung_kb' => $this->kampung_kb,
            'rumah_dataku' => $this->rumah_dataku
        ];
    }
}
