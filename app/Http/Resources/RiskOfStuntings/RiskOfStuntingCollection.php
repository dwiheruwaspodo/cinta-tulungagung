<?php

namespace App\Http\Resources\RiskOfStuntings;

use Illuminate\Http\Resources\Json\ResourceCollection;

class RiskOfStuntingCollection extends ResourceCollection
{
    public function toArray($request)
    {
        return $this->collection->map->only('id', 'created_by', 'rw', 'rt', 'head_of_family', 'baduta', 'balita',
            'pus', 'pus_hamil', 'source_of_water', 'toilet', 'too_young', 'too_old', 'district_id',
            'village_id', 'village', 'district', 'too_near', 'too_tight', 'risk');
    }
}
