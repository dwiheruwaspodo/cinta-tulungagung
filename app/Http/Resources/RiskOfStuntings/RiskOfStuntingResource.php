<?php

namespace App\Http\Resources\RiskOfStuntings;

use Illuminate\Http\Resources\Json\JsonResource;

class RiskOfStuntingResource extends JsonResource
{

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'rw' => $this->rw,
            'rt' => $this->rt,
            'head_of_family' => $this->head_of_family,
            'baduta' => $this->baduta,
            'balita' => $this->balita,
            'pus' => $this->pus,
            'pus_hamil' => $this->pus_hamil,
            'source_of_water' => $this->source_of_water,
            'toilet' => $this->toilet,
            'too_young' => $this->too_young,
            'too_old' => $this->too_old,
            'district_id' => $this->district_id,
            'village_id' => $this->village_id,
            'too_near' => $this->too_near,
            'too_tight' => $this->too_tight,
            'risk' => $this->risk,
        ];
    }
}
