<?php

namespace App\Http\Resources\UnMeetNeeds;

use Illuminate\Http\Resources\Json\ResourceCollection;

class UnMeetNeedCollection extends ResourceCollection
{
    public function toArray($request)
    {
        return $this->collection->map->only('id', 'created_by', 'district_id', 'village_id', 'date',
            'created_by', 'iat', 'tial', 'ias', 'hamil', 'village', 'district');
    }
}
