<?php

namespace App\Http\Resources\UnMeetNeeds;

use Illuminate\Http\Resources\Json\ResourceCollection;

class UnMeetNeedKabupatenCollection extends ResourceCollection
{
    public function toArray($request)
    {
        return $this->collection->map->only('id', 'created_by', 'district_id', 'village_id', 'date',
            'created_by', 'sum_iat', 'sum_tial', 'sum_ias', 'sum_hamil', 'village', 'district', 'month', 'year');
    }
}
