<?php

namespace App\Http\Resources\UnMeetNeeds;

use Illuminate\Http\Resources\Json\JsonResource;

class UnMeetNeedResource extends JsonResource
{

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'district_id' => $this->district_id,
            'village_id' => $this->village_id,
            'date' => $this->date,
            'created_by' => $this->created_by,
            'iat' => $this->iat,
            'tial' => $this->tial,
            'ias' => $this->ias,
            'hamil' => $this->hamil
        ];
    }
}
