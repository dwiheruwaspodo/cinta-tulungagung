<?php

namespace App\Http\Resources\VillageProfiles;

use Illuminate\Http\Resources\Json\ResourceCollection;

class VillageProfileCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return $this->collection->map->only(
            'id', 'headman', 'kampung_kb', 'desa_membangun', 'rumah_dataku', 'potensi', 'dapur_stunting',
            'village', 'district', 'attach_kampung_kb', 'attach_kampung_kb_url',
            'attach_rumah_dataku', 'attach_rumah_dataku_url'
        );
    }
}
