<?php

namespace App\Http\Resources\VillageProfiles;

use Illuminate\Http\Resources\Json\JsonResource;

class VillageProfileResource extends JsonResource
{

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'headman' => $this->headman,
            'kampung_kb' => $this->kampung_kb,
            'desa_membangun' => $this->desa_membangun,
            'rumah_dataku' => $this->rumah_dataku,
            'potensi' => $this->potensi,
            'dapur_stunting' => $this->dapur_stunting,
            'village_id' => $this->village_id,
            'district_id' => $this->district_id,
            'attach_kampung_kb' => $this->attach_kampung_kb,
            'attach_rumah_dataku' => $this->attach_rumah_dataku,
        ];
    }
}
