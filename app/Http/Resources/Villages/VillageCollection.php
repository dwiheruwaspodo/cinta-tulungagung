<?php

namespace App\Http\Resources\Villages;

use Illuminate\Http\Resources\Json\ResourceCollection;

class VillageCollection extends ResourceCollection
{
    public function toArray($request)
    {
        return $this->collection->map->only('id', 'name', 'district_id');
    }
}
