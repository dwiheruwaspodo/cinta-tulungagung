<?php

namespace App\Http\Resources\WeddingAssistances;

use Illuminate\Http\Resources\Json\ResourceCollection;

class WeddingAssistanceCollection extends ResourceCollection
{
    public function toArray($request)
    {
        return $this->collection->map->only('id', 'date', 'nik', 'name', 'gender', 'birthday', 'created_by',
            'to', 'smoking', 'anemia', 'lila', 'imt', 'stunting', 'village', 'district', 'village_id', 'district_id');
    }
}
