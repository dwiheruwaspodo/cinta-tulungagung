<?php

namespace App\Http\Resources\WeddingAssistances;

use Illuminate\Http\Resources\Json\JsonResource;

class WeddingAssistanceResource extends JsonResource
{

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'date' => $this->date,
            'nik' => $this->nik,
            'name' => $this->name,
            'gender' => $this->gender,
            'birthday' => $this->birthday,
            'to' => $this->to,
            'smoking' => $this->smoking,
            'anemia' => $this->anemia,
            'lila' => $this->lila,
            'imt' => $this->imt,
            'stunting' => $this->stunting,
            'village_id' => $this->village_id,
            'district_id' => $this->district_id,
        ];
    }
}
