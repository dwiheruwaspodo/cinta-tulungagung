<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BalitaAssistance extends Model
{
    use HasFactory;

    public const OPT_STATUS = [
      'Sangat Pendek', 'Pendek', 'Gizi Buruk', 'Gizi Kurang', 'Sangat Kurus', 'Kurus', 'Gemuk'
    ];

    public static function optPosyandu()
    {
        $posyandu = [];

        for ($x=1; $x <= 20; $x++)
        {
            $posyandu[] = 'Posyandu '.$x;
        }

        return $posyandu;
    }

    protected $fillable = ['date', 'name', 'gender', 'posyandu', 'age', 'weight', 'parent_name', 'gakin', 'height',
        'status', 'village_id', 'district_id', 'created_by'];

    public function village(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Village::class);
    }

    public function district(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(District::class);
    }

    public function scopeOrderByDate($query)
    {
        $query->orderBy('date', 'desc');
    }

    public function scopeFilter($query, array $filters)
    {
        $query->when($filters['search'] ?? null, function ($query, $search) {
            $query->where(function ($query) use ($search) {
                $query->where('posyandu', 'like', '%'.$search.'%')
                    ->orWhere('name', 'like', '%'.$search.'%')
                    ->orWhere('age', 'like', '%'.$search.'%')
                    ->orWhere('weight', 'like', '%'.$search.'%')
                    ->orWhere('parent_name', 'like', '%'.$search.'%')
                    ->orWhereHas('district', function ($query) use ($search) {
                        $query->where('name', 'like', '%'.$search.'%');
                    })
                    ->orWhereHas('village', function ($query) use ($search) {
                        $query->where('name', 'like', '%'.$search.'%');
                    })
                ;
            });
        });

        if (isset($filters['date_start']) && isset($filters['date_end']))
        {
            $query = $query->whereBetween('date', [$filters['date_start'], $filters['date_end']] );
        }

        return $query;
    }
}
