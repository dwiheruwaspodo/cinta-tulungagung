<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Bokb extends Model
{
    use HasFactory;

    protected $fillable = ['district_id', 'village_id', 'date', 'created_by', 'activity', 'location', 'link_document',
        'source_person', 'source_person_from', 'warung'];

    public const ACTIVITIES = [
        'Penyuluhan KB', 'Mini Lokakarya', 'Pokja Kampung KB', 'Ketahanan Keluarga'
    ];

    public const LOCATIONS = [
        'Balai', 'Gedung Pertemuan Kecamatan', 'Rumah Warga', 'Balai Desa', 'Lainnya'
    ];

    public const SOURCE_PERSON_FROMS = [
        'OPD KB', 'DPMD', 'DINKES', 'KUA', 'PUSKESMAS', 'Lainnya'
    ];


    public function village(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Village::class);
    }

    public function district(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(District::class);
    }

    public function scopeOrderByDate($query)
    {
        $query->orderBy('date', 'desc');
    }

    public function scopeFilter($query, array $filters)
    {
        $query->when($filters['search'] ?? null, function ($query, $search) {
            $query->where(function ($query) use ($search) {
                $query->where('activity', 'like', '%'.$search.'%')
                    ->orWhere('location', 'like', '%'.$search.'%')
                    ->orWhere('link_document', 'like', '%'.$search.'%')
                    ->orWhere('source_person', 'like', '%'.$search.'%')
                    ->orWhere('source_person_from', 'like', '%'.$search.'%')
                    ->orWhere('warung', 'like', '%'.$search.'%')
                    ->orWhereHas('district', function ($query) use ($search) {
                        $query->where('name', 'like', '%'.$search.'%');
                    })
                    ->orWhereHas('village', function ($query) use ($search) {
                        $query->where('name', 'like', '%'.$search.'%');
                    })
                ;
            });
        });

        if (isset($filters['date_start']) && isset($filters['date_end']))
        {
            $query = $query->whereBetween('date', [$filters['date_start'], $filters['date_end']] );
        }

        return $query;
    }
}
