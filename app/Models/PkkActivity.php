<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\URL;
use League\Glide\Server;

class PkkActivity extends Model
{
    use HasFactory;

    public const KABUPATEN = 'kabupaten';
    public const KECAMATAN = 'kecamatan';
    public const DESA = 'desa';
    public const OPT_LEVEL = [self::KABUPATEN, self::KECAMATAN, self::DESA];

    protected $fillable = ['pkk_activity_category_id', 'district_id', 'village_id', 'date', 'description', 'source_person',
        'source_person_from', 'target', 'target_amount', 'location', 'photo', 'created_by', 'level'];

    public function pkk_activity_category(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(PkkActivityCategory::class);
    }

    public function village(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Village::class);
    }

    public function district(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(District::class);
    }

    public function scopeOrderByDate($query)
    {
        $query->orderBy('date', 'desc');
    }

    public function setPhotoAttribute($photo)
    {
        if(!$photo) return;

        $this->attributes['photo_path'] = $photo instanceof UploadedFile ? $photo->store('pkk') : $photo;
    }

    public function getPhotoAttribute() {
        return $this->photoUrl(['w' => 40, 'h' => 40, 'fit' => 'crop']);
    }

    public function photoUrl(array $attributes)
    {
        if ($this->photo_path) {
            return URL::to(App::make(Server::class)->fromPath($this->photo_path, $attributes));
        }
    }

    public function scopeFilter($query, array $filters)
    {
        $query->when($filters['search'] ?? null, function ($query, $search) {
            $query->where(function ($query) use ($search) {
                $query->where('description', 'like', '%'.$search.'%')
                    ->orWhere('level', 'like', '%'.$search.'%')
                    ->orWhere('source_person', 'like', '%'.$search.'%')
                    ->orWhere('source_person_from', 'like', '%'.$search.'%')
                    ->orWhere('location', 'like', '%'.$search.'%')
                    ->orWhereHas('pkk_activity_category', function ($query) use ($search) {
                        $query->where('category', 'like', '%'.$search.'%');
                    })
                    ->orWhereHas('district', function ($query) use ($search) {
                        $query->where('name', 'like', '%'.$search.'%');
                    })
                    ->orWhereHas('village', function ($query) use ($search) {
                        $query->where('name', 'like', '%'.$search.'%');
                    })
                ;
            });
        });

        if (isset($filters['date_start']) && isset($filters['date_end']))
        {
            $query = $query->whereBetween('date', [$filters['date_start'], $filters['date_end']] );
        }

        return $query;
    }
}
