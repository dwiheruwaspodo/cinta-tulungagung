<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PkkRegulation extends Model
{
    use HasFactory;

    protected $fillable = ['date', 'number', 'name', 'description', 'issued_by', 'created_by'];

    public function scopeOrderByDate($query)
    {
        $query->orderBy('date', 'desc');
    }

    public function scopeFilter($query, array $filters)
    {
        $query->when($filters['search'] ?? null, function ($query, $search) {
            $query->where(function ($query) use ($search) {
                $query->where('description', 'like', '%'.$search.'%')
                    ->orWhere('name', 'like', '%'.$search.'%')
                    ->orWhere('issued_by', 'like', '%'.$search.'%')
                    ->orWhere('number', 'like', '%'.$search.'%')
                ;
            });
        });

        if (isset($filters['date_start']) && isset($filters['date_end']))
        {
            $query = $query->whereBetween('date', [$filters['date_start'], $filters['date_end']] );
        }

        return $query;
    }
}
