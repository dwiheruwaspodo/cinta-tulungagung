<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Poktan extends Model
{
    use HasFactory;

    protected $fillable = ['pkk_activity_category_id', 'district_id', 'village_id', 'created_by', 'date', 'keluarga_bkb',
        'anggota_hadir_bkb', 'keluarga_bkr', 'anggota_hadir_bkr', 'keluarga_bkl', 'anggota_hadir_bkl', 'keluarga_uppks',
        'anggota_hadir_uppks', 'keluarga_pikr', 'anggota_hadir_pikr'];

    public function village(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Village::class);
    }

    public function district(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(District::class);
    }

    public function scopeOrderByDate($query)
    {
        $query->orderBy('date', 'desc');
    }

    public function scopeFilter($query, array $filters)
    {
        $query->when($filters['search'] ?? null, function ($query, $search) {
            $query->where(function ($query) use ($search) {
                $query->whereHas('district', function ($query) use ($search) {
                        $query->where('name', 'like', '%'.$search.'%');
                    })
                    ->orWhereHas('village', function ($query) use ($search) {
                        $query->where('name', 'like', '%'.$search.'%');
                    });
            });
        });

        if (isset($filters['date_start']) && isset($filters['date_end']))
        {
            $query = $query->whereBetween('date', [$filters['date_start'], $filters['date_end']] );
        }

        return $query;
    }
}
