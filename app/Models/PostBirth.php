<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PostBirth extends Model
{
    use HasFactory;

    protected $fillable = ['date', 'birth_suntik', 'birth_pil', 'birth_kondom', 'birth_iud', 'birth_implan',
        'birth_mop', 'birth_mow', 'miscarriage_suntik', 'miscarriage_pil', 'miscarriage_kondom', 'miscarriage_iud',
        'miscarriage_implan', 'miscarriage_mop', 'miscarriage_mow', 'village_id', 'district_id', 'created_by'];

    public function scopeOrderByDate($query)
    {
        $query->orderBy('date', 'desc');
    }

    public function village(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Village::class);
    }

    public function district(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(District::class);
    }

    public function scopeFilter($query, array $filters)
    {
        $query->when($filters['search'] ?? null, function ($query, $search) {
            $query->where(function ($query) use ($search) {
                $query->whereHas('district', function ($query) use ($search) {
                    $query->where('name', 'like', '%'.$search.'%');
                })
                    ->orWhereHas('village', function ($query) use ($search) {
                        $query->where('name', 'like', '%'.$search.'%');
                    });
            });
        });

        if (isset($filters['date_start']) && isset($filters['date_end']))
        {
            $query = $query->whereBetween('date', [$filters['date_start'], $filters['date_end']] );
        }

        return $query;
    }
}
