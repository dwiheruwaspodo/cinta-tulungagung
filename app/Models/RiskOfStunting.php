<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RiskOfStunting extends Model
{
    use HasFactory;

    protected $fillable = ['created_by', 'rw', 'rt', 'head_of_family', 'baduta', 'balita', 'pus', 'pus_hamil',
        'source_of_water', 'toilet', 'too_young', 'too_old', 'district_id', 'village_id', 'too_near',
        'too_tight', 'risk'];

    public const OPT_SOURCE_WATER = [
        'Air Minum Kemasan', 'Ledeng/PAM', 'Sumur Bor', 'Sumur Terlindungi', 'Sumur Tidak Terlindungi', 'Air Permukaan',
        'Air Hujan', 'Lainnya',
    ];

    public const OPT_TOILET = [
         'Dengan Septictank', 'Tanpa Septictank', 'Jamban Umum', 'Lainnya'
    ];

    public function village(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Village::class);
    }

    public function district(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(District::class);
    }

    public function scopeOrderByDate($query)
    {
        $query->orderBy('created_at', 'desc');
    }

    public function scopeFilter($query, array $filters)
    {
        $query->when($filters['search'] ?? null, function ($query, $search) {
            $query->where(function ($query) use ($search) {
                $query->where('rw', 'like', '%'.$search.'%')
                    ->orWhere('rt', 'like', '%'.$search.'%')
                    ->orWhere('head_of_family', 'like', '%'.$search.'%')
                    ->orWhere('baduta', 'like', '%'.$search.'%')
                    ->orWhere('balita', 'like', '%'.$search.'%')
                    ->orWhere('pus', 'like', '%'.$search.'%')
                    ->orWhere('pus_hamil', 'like', '%'.$search.'%')
                    ->orWhere('source_of_water', 'like', '%'.$search.'%')
                    ->orWhere('toilet', 'like', '%'.$search.'%')
                    ->orWhere('too_young', 'like', '%'.$search.'%')
                    ->orWhere('too_old', 'like', '%'.$search.'%')
                    ->orWhereHas('district', function ($query) use ($search) {
                        $query->where('name', 'like', '%'.$search.'%');
                    })
                    ->orWhereHas('village', function ($query) use ($search) {
                        $query->where('name', 'like', '%'.$search.'%');
                    })
                ;
            });
        });
    }
}
