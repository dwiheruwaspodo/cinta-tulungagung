<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    use HasFactory;

    public const SA = 'Super Admin';
    public const PKK_KAB = 'PKK Kabupaten';
    public const PKK_KEC = 'PKK Kecamatan';
    public const KS_KAB = 'Kesehatan Kabupaten';
    public const KS_KEC = 'Kesehatan Kecamatan';
    public const KB_KAB = 'KB Kabupaten';
    public const KB_KEC = 'KB Kecamatan';

    public const ALL = [
        self::SA, self::PKK_KAB, self::PKK_KEC, self::KS_KAB, self::KS_KEC, self::KB_KAB, self::KB_KEC
    ];

    public const KECAMATAN = [self::PKK_KEC, self::KS_KEC, self::KB_KEC];
    public const KABUPATEN = [self::PKK_KAB, self::KS_KAB, self::KB_KAB];
    public const ADMIN = [self::SA];

    public const PKK = [self::PKK_KAB, self::PKK_KEC];
    public const KS = [self::KS_KAB, self::KS_KEC];
    public const KB = [self::KB_KAB, self::KB_KEC];

    public static function isAdmin($user): bool
    {
        return in_array(self::find($user->role_id)->name, self::ADMIN);
    }

    public static function isKecamatan($user): bool
    {
        return in_array(self::find($user->role_id)->name, self::KECAMATAN);
    }

    public static function isKabupaten($user): bool
    {
        return in_array(self::find($user->role_id)->name, self::KABUPATEN);
    }

    public static function isKB($user): bool
    {
        return in_array(self::find($user->role_id)->name, self::KB);
    }

    public static function isKS($user): bool
    {
        return in_array(self::find($user->role_id)->name, self::KS);
    }

    public static function isPKK($user): bool
    {
        return in_array(self::find($user->role_id)->name, self::PKK);
    }
}
