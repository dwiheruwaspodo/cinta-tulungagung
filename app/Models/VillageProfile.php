<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\URL;
use League\Glide\Server;
use Illuminate\Support\Facades\Storage;

class VillageProfile extends Model
{
    use HasFactory;

    protected $fillable = ['headman', 'kampung_kb', 'desa_membangun', 'rumah_dataku', 'potensi',
        'dapur_stunting', 'created_by', 'village_id', 'district_id', 'attach_kampung_kb', 'attach_rumah_dataku'];

    public const OPT_KAMPUNG_KB = [
        'Dasar', 'Berkembang', 'Mandiri', 'Berkelanjutan', 'Bukan Kampung KB'
    ];

    public const OPT_RUMAH_DATAKU = [
        'Belum Ada', 'Sederhana', 'Lengkap', 'Paripurna'
    ];

    public const OPT_DESA_MEMBANGUN = [
        'Mandiri', 'Maju', 'Berkembang', 'Tertinggal', 'Sangat Tertinggal'
    ];

    public const OPT_POTENSI = [
        'Wisata', 'Pertanian', 'Perkebunan', 'Peternakan', 'Produk', 'Edukasi', 'Ramah Anak', 'Bumdes',
        'Ekonomi Berdaya', 'Lingkungan Sehat', 'Germas', 'Lainnya'
    ];

    public function village(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Village::class);
    }

    public function district(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(District::class);
    }

    public function scopeOrderByDate($query)
    {
        $query->orderBy('created_at', 'desc');
    }

    public function setAttachKampungKbAttribute($attach_kampung_kb)
    {
        if(!$attach_kampung_kb) return;

        $this->attributes['attach_kampung_kb_path'] = $attach_kampung_kb instanceof UploadedFile ? $attach_kampung_kb->store('public/kampung_kb') : $attach_kampung_kb;
    }

    public function attachKampungKbUrl()
    {
        if ($this->attach_kampung_kb_path) {
            return URL::to(Storage::url($this->attach_kampung_kb_path));
        }
    }

    public function getAttachKampungKbAttribute() {
        return $this->attachKampungKbUrl();
    }

    public function setAttachRumahDatakuAttribute($attach_rumah_dataku)
    {
        if(!$attach_rumah_dataku) return;

        $this->attributes['attach_rumah_dataku_path'] = $attach_rumah_dataku instanceof UploadedFile ? $attach_rumah_dataku->store('public/rumah_dataku') : $attach_rumah_dataku;
    }

    public function attachRumahDatakuUrl()
    {
        if ($this->attach_rumah_dataku_path) {
            return URL::to(Storage::url($this->attach_rumah_dataku_path));
        }
    }

    public function getAttachRumahDatakuAttribute() {
        return $this->attachRumahDatakuUrl();
    }

    public function scopeFilter($query, array $filters)
    {
        $query->when($filters['search'] ?? null, function ($query, $search) {
            $query->where(function ($query) use ($search) {
                $query->where('headman', 'like', '%'.$search.'%')
                    ->orWhere('kampung_kb', 'like', '%'.$search.'%')
                    ->orWhere('desa_membangun', 'like', '%'.$search.'%')
                    ->orWhere('rumah_dataku', 'like', '%'.$search.'%')
                    ->orWhere('potensi', 'like', '%'.$search.'%')
                    ->orWhere('dapur_stunting', 'like', '%'.$search.'%')
                    ->orWhereHas('district', function ($query) use ($search) {
                        $query->where('name', 'like', '%'.$search.'%');
                    })
                    ->orWhereHas('village', function ($query) use ($search) {
                        $query->where('name', 'like', '%'.$search.'%');
                    })
                ;
            });
        });
    }
}
