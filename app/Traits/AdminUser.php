<?php

namespace App\Traits;

use App\Models\Role;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;

trait AdminUser {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Role::isAdmin(Auth::user());
    }

    public function failedAuthorization() {

        $this->session()->flash('error', 'Updating or deleting the user is not allowed.');

        // Note: This is required, otherwise demo user will update
        // and both, success and error messages will be returned.
        throw ValidationException::withMessages([]);
    }
}
