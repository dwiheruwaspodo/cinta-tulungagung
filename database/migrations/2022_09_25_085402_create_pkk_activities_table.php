<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePkkActivitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pkk_activities', function (Blueprint $table) {
            $table->increments('id');
            $table->foreignIdFor(\App\Models\PkkActivityCategory::class)->index();
            $table->foreignIdFor(\App\Models\District::class)->nullable()->index();
            $table->foreignIdFor(\App\Models\Village::class)->nullable()->index();
            $table->integer('created_by')->index();
            $table->dateTime('date');
            $table->string('description');
            $table->string('source_person')->nullable();
            $table->string('source_person_from')->nullable();
            $table->string('target')->nullable();
            $table->integer('target_amount')->default(0);
            $table->string('location');
            $table->string('photo_path')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pkk_activities');
    }
}
