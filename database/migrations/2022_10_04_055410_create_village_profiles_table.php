<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVillageProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('village_profiles', function (Blueprint $table) {
            $table->increments('id');
            $table->foreignIdFor(\App\Models\Village::class)->nullable()->index();
            $table->string('headman', 150);
            $table->string('kampung_kb', 100)->index();
            $table->string('desa_membangun', 150)->index();
            $table->string('rumah_dataku', 150)->index();
            $table->string('potensi', 150)->index();
            $table->tinyInteger('dapur_stunting')->default(0)->index();
            $table->integer('created_by')->index();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('village_profiles');
    }
}
