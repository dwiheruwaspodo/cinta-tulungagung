<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRiskOfStuntingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('risk_of_stuntings', function (Blueprint $table) {
            $table->increments('id');
            $table->foreignIdFor(\App\Models\District::class)->nullable()->index();
            $table->foreignIdFor(\App\Models\Village::class)->nullable()->index();
            $table->integer('created_by')->index();
            $table->string('rw', 50);
            $table->string('rt', 5);
            $table->string('head_of_family', 150);
            $table->tinyInteger('baduta')->default(0)->index();
            $table->tinyInteger('balita')->default(0)->index();
            $table->tinyInteger('pus')->default(0)->index();
            $table->tinyInteger('pus_hamil')->default(0)->index();
            $table->string('source_of_water', 50)->index();
            $table->string('toilet', 50)->index();
            $table->tinyInteger('too_young')->default(0)->index();
            $table->tinyInteger('too_old')->default(0)->index();
            $table->tinyInteger('too_near')->default(0)->index();
            $table->tinyInteger('too_tight')->default(0)->index();
            $table->tinyInteger('risk')->default(0)->index();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('risk_of_stuntings');
    }
}
