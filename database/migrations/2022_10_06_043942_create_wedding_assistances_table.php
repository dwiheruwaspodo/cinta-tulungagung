<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWeddingAssistancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wedding_assistances', function (Blueprint $table) {
            $table->increments('id');
            $table->dateTime('date');
            $table->foreignIdFor(\App\Models\District::class)->nullable()->index();
            $table->foreignIdFor(\App\Models\Village::class)->nullable()->index();
            $table->string('nik', 20);
            $table->string('name', 100);
            $table->tinyInteger('gender')->default(0)->index();
            $table->dateTime('birthday');
            $table->integer('created_by')->index();
            $table->tinyInteger('to')->default(0);
            $table->tinyInteger('smoking')->default(0)->index();
            $table->tinyInteger('anemia')->nullable();
            $table->tinyInteger('lila')->nullable();
            $table->tinyInteger('imt')->nullable();
            $table->tinyInteger('stunting')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wedding_assistances');
    }
}
