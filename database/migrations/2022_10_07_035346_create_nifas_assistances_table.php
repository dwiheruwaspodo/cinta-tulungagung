<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNifasAssistancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nifas_assistances', function (Blueprint $table) {
            $table->increments('id');
            $table->dateTime('date');
            $table->foreignIdFor(\App\Models\District::class)->nullable()->index();
            $table->foreignIdFor(\App\Models\Village::class)->nullable()->index();
            $table->string('nik', 20);
            $table->string('name', 100);
            $table->dateTime('birthday');
            $table->dateTime('born');
            $table->integer('created_by')->index();
            $table->tinyInteger('is_contraception')->default(0)->index();
            $table->string('contraception', 150)->nullable();
            $table->string('description');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('nifas_assistances');
    }
}
