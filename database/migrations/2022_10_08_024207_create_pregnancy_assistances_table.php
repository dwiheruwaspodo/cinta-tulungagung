<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePregnancyAssistancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pregnancy_assistances', function (Blueprint $table) {
            $table->increments('id');
            $table->dateTime('date');
            $table->foreignIdFor(\App\Models\District::class)->nullable()->index();
            $table->foreignIdFor(\App\Models\Village::class)->nullable()->index();
            $table->string('nik', 20);
            $table->string('name', 100);
            $table->dateTime('birthday');
            $table->integer('pregnancy_age')->default(1);
            $table->integer('created_by')->index();
            $table->tinyInteger('kek')->default(0)->index();
            $table->tinyInteger('anemia')->default(0)->index();
            $table->tinyInteger('stunting')->default(0)->index();
            $table->tinyInteger('inhibited_fetal_growth')->default(0)->index();
            $table->tinyInteger('four_too')->default(0)->index();
            $table->string('description');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pregnancy_assistances');
    }
}
