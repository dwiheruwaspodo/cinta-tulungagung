<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBalitaAssistancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('balita_assistances', function (Blueprint $table) {
            $table->increments('id');
            $table->dateTime('date');
            $table->foreignIdFor(\App\Models\District::class)->nullable()->index();
            $table->foreignIdFor(\App\Models\Village::class)->nullable()->index();
            $table->string('name', 200);
            $table->tinyInteger('gender')->default(0)->index();
            $table->string('posyandu', 100)->index();
            $table->integer('age');
            $table->float('weight');
            $table->float('height');
            $table->string('parent_name', 200);
            $table->tinyInteger('gakin')->default(0);
            $table->string('status');
            $table->integer('created_by')->index();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('balita_assistances');
    }
}
