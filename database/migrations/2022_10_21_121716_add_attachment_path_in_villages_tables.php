<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddAttachmentPathInVillagesTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('village_profiles', function (Blueprint $table) {
            $table->string('attach_kampung_kb_path')->nullable();
            $table->string('attach_rumah_dataku_path')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('village_profiles', function (Blueprint $table) {
            $table->dropColumn('attach_kampung_kb_path');
            $table->dropColumn('attach_rumah_dataku_path');
        });
    }
}
