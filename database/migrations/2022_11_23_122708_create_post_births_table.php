<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePostBirthsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('post_births', function (Blueprint $table) {
            $table->id();
            $table->dateTime('date');
            $table->foreignIdFor(\App\Models\District::class)->nullable()->index();
            $table->foreignIdFor(\App\Models\Village::class)->nullable()->index();
            $table->integer('birth_suntik');
            $table->integer('birth_pil');
            $table->integer('birth_kondom');
            $table->integer('birth_iud');
            $table->integer('birth_implan');
            $table->integer('birth_mop');
            $table->integer('birth_mow');
            $table->integer('miscarriage_suntik');
            $table->integer('miscarriage_pil');
            $table->integer('miscarriage_kondom');
            $table->integer('miscarriage_iud');
            $table->integer('miscarriage_implan');
            $table->integer('miscarriage_mop');
            $table->integer('miscarriage_mow');
            $table->integer('created_by')->index();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('post_births');
    }
}
