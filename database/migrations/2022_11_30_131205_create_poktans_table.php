<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePoktansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('poktans', function (Blueprint $table) {
            $table->id();
            $table->dateTime('date');
            $table->foreignIdFor(\App\Models\District::class)->nullable()->index();
            $table->foreignIdFor(\App\Models\Village::class)->nullable()->index();
            $table->integer('keluarga_bkb');
            $table->integer('anggota_hadir_bkb');
            $table->integer('keluarga_bkr');
            $table->integer('anggota_hadir_bkr');
            $table->integer('keluarga_bkl');
            $table->integer('anggota_hadir_bkl');
            $table->integer('keluarga_uppks');
            $table->integer('anggota_hadir_uppks');
            $table->integer('keluarga_pikr');
            $table->integer('anggota_hadir_pikr');
            $table->integer('created_by')->index();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('poktans');
    }
}
