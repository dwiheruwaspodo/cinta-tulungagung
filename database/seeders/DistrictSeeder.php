<?php

namespace Database\Seeders;

use App\Models\District;
use Illuminate\Database\Seeder;

class DistrictSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        District::create([
            'id' => 1,
            'code' => '350401',
            'name' => 'TULUNGAGUNG',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        District::create([
            'id' => 2,
            'code' => '350402',
            'name' => 'BOYOLANGU',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        District::create([
            'id' => 3,
            'code' => '350403',
            'name' => 'KEDUNGWARU',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        District::create([
            'id' => 4,
            'code' => '350404',
            'name' => 'NGANTRU',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        District::create([
            'id' => 5,
            'code' => '350405',
            'name' => 'KAUMAN',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        District::create([
            'id' => 6,
            'code' => '350406',
            'name' => 'PAGERWOJO',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        District::create([
            'id' => 7,
            'code' => '350407',
            'name' => 'SENDANG',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        District::create([
            'id' => 8,
            'code' => '350408',
            'name' => 'KARANGREJO',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        District::create([
            'id' => 9,
            'code' => '350409',
            'name' => 'GONDANG',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        District::create([
            'id' => 10,
            'code' => '350410',
            'name' => 'SUMBERGEMPOL',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        District::create([
            'id' => 11,
            'code' => '350411',
            'name' => 'NGUNUT',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        District::create([
            'id' => 12,
            'code' => '350412',
            'name' => 'PUCANGLABAN',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        District::create([
            'id' => 13,
            'code' => '350413',
            'name' => 'REJOTANGAN',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        District::create([
            'id' => 14,
            'code' => '350414',
            'name' => 'KALIDAWIR',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        District::create([
            'id' => 15,
            'code' => '350415',
            'name' => 'BESUKI',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        District::create([
            'id' => 16,
            'code' => '350416',
            'name' => 'CAMPURDARAT',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        District::create([
            'id' => 17,
            'code' => '350417',
            'name' => 'BANDUNG',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        District::create([
            'id' => 18,
            'code' => '350418',
            'name' => 'PAKEL',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        District::create([
            'id' => 19,
            'code' => '350419',
            'name' => 'TANGGUNGGUNUNG',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
    }
}
