<?php

namespace Database\Seeders;

use App\Models\Role;
use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('roles')->delete();

        array_map(function ($role, $key) {
            Role::firstOrCreate(['name' => $role, 'id' => $key + 1]);
        }, Role::ALL, array_keys(Role::ALL));
    }
}
