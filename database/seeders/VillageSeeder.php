<?php

namespace Database\Seeders;

use App\Models\Village;
use Illuminate\Database\Seeder;

class VillageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Village::create([
            'id' => 1,
            'code' => '3504011001',
            'name' => 'KEDUNGSOKO',
            'district_id' => 1,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 2,
            'code' => '3504011002',
            'name' => 'TRETEK',
            'district_id' => 1,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 3,
            'code' => '3504011003',
            'name' => 'KARANGWARU',
            'district_id' => 1,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 4,
            'code' => '3504011004',
            'name' => 'TAMANAN',
            'district_id' => 1,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 5,
            'code' => '3504011005',
            'name' => 'JEPUN',
            'district_id' => 1,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 6,
            'code' => '3504011006',
            'name' => 'BAGO',
            'district_id' => 1,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 7,
            'code' => '3504011007',
            'name' => 'KEPATIHAN',
            'district_id' => 1,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 8,
            'code' => '3504011008',
            'name' => 'KENAYAN',
            'district_id' => 1,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 9,
            'code' => '3504011009',
            'name' => 'KAMPUNGDALEM',
            'district_id' => 1,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 10,
            'code' => '3504011010',
            'name' => 'KAUMAN',
            'district_id' => 1,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 11,
            'code' => '3504011011',
            'name' => 'KUTOANYAR',
            'district_id' => 1,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 12,
            'code' => '3504011012',
            'name' => 'SEMBUNG',
            'district_id' => 1,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 13,
            'code' => '3504011013',
            'name' => 'PANGGUNGREJO',
            'district_id' => 1,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 14,
            'code' => '3504011014',
            'name' => 'BOTORAN',
            'district_id' => 1,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 15,
            'code' => '3504022001',
            'name' => 'WAJAKKIDUL',
            'district_id' => 2,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 16,
            'code' => '3504022002',
            'name' => 'SANGGRAHAN',
            'district_id' => 2,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 17,
            'code' => '3504022003',
            'name' => 'PUCUNGKIDUL',
            'district_id' => 2,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 18,
            'code' => '3504022004',
            'name' => 'BOYOLANGU',
            'district_id' => 2,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 19,
            'code' => '3504022005',
            'name' => 'NGRANTI',
            'district_id' => 2,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 20,
            'code' => '3504022006',
            'name' => 'KENDALBULUR',
            'district_id' => 2,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 21,
            'code' => '3504022007',
            'name' => 'BONO ',
            'district_id' => 2,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 22,
            'code' => '3504022008',
            'name' => 'WAUNG',
            'district_id' => 2,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 23,
            'code' => '3504022009',
            'name' => 'MOYOKETEN',
            'district_id' => 2,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 24,
            'code' => '3504022010',
            'name' => 'WAJAK LOR',
            'district_id' => 2,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 25,
            'code' => '3504022011',
            'name' => 'KARANGREJO',
            'district_id' => 2,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 26,
            'code' => '3504022012',
            'name' => 'KEPUH',
            'district_id' => 2,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 27,
            'code' => '3504022013',
            'name' => 'TANJUNGSARI',
            'district_id' => 2,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 28,
            'code' => '3504022014',
            'name' => 'SERUT',
            'district_id' => 2,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 29,
            'code' => '3504022015',
            'name' => 'BEJI',
            'district_id' => 2,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 30,
            'code' => '3504022016',
            'name' => 'SOBONTORO',
            'district_id' => 2,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 31,
            'code' => '3504022017',
            'name' => 'GEDANGSEWU',
            'district_id' => 2,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 32,
            'code' => '3504032001',
            'name' => 'PLOSOKANDANG',
            'district_id' => 3,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 33,
            'code' => '3504032002',
            'name' => 'TUNGGULSARI',
            'district_id' => 3,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 34,
            'code' => '3504032003',
            'name' => 'RINGINPITU',
            'district_id' => 3,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 35,
            'code' => '3504032004',
            'name' => 'LODERESAN',
            'district_id' => 3,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 36,
            'code' => '3504032005',
            'name' => 'BULUSARI',
            'district_id' => 3,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 37,
            'code' => '3504032006',
            'name' => 'BANGOAN',
            'district_id' => 3,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 38,
            'code' => '3504032007',
            'name' => 'BORO',
            'district_id' => 3,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 39,
            'code' => '3504032008',
            'name' => 'TAPAN',
            'district_id' => 3,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 40,
            'code' => '3504032009',
            'name' => 'REJOAGUNG',
            'district_id' => 3,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 41,
            'code' => '3504032010',
            'name' => 'KEDUNGWARU',
            'district_id' => 3,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 42,
            'code' => '3504032011',
            'name' => 'PLANDAAN',
            'district_id' => 3,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 43,
            'code' => '3504032012',
            'name' => 'KETANON',
            'district_id' => 3,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 44,
            'code' => '3504032013',
            'name' => 'TAWANGSARI',
            'district_id' => 3,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 45,
            'code' => '3504032014',
            'name' => 'MANGUNSARI',
            'district_id' => 3,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 46,
            'code' => '3504032015',
            'name' => 'WINONG',
            'district_id' => 3,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 47,
            'code' => '3504032016',
            'name' => 'MAJAN',
            'district_id' => 3,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 48,
            'code' => '3504032017',
            'name' => 'SIMO',
            'district_id' => 3,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 49,
            'code' => '3504032018',
            'name' => 'GENDINGAN',
            'district_id' => 3,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 50,
            'code' => '3504032019',
            'name' => 'NGUJANG',
            'district_id' => 3,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 51,
            'code' => '3504042001',
            'name' => 'PAKEL',
            'district_id' => 4,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 52,
            'code' => '3504042002',
            'name' => 'PUCUNGLOR',
            'district_id' => 4,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 53,
            'code' => '3504042003',
            'name' => 'SRIKATON',
            'district_id' => 4,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 54,
            'code' => '3504042004',
            'name' => 'PADANGAN',
            'district_id' => 4,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 55,
            'code' => '3504042005',
            'name' => 'BANJARSARI',
            'district_id' => 4,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 56,
            'code' => '3504042006',
            'name' => 'PULEREJO',
            'district_id' => 4,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 57,
            'code' => '3504042007',
            'name' => 'BENDOSARI',
            'district_id' => 4,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 58,
            'code' => '3504042008',
            'name' => 'NGANTRU',
            'district_id' => 4,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 59,
            'code' => '3504042009',
            'name' => 'MOJOAGUNG',
            'district_id' => 4,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 60,
            'code' => '3504042010',
            'name' => 'BATOKAN',
            'district_id' => 4,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 61,
            'code' => '3504042011',
            'name' => 'KEPUHREJO',
            'district_id' => 4,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 62,
            'code' => '3504042012',
            'name' => 'POJOK',
            'district_id' => 4,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 63,
            'code' => '3504042013',
            'name' => 'PINGGIRSARI',
            'district_id' => 4,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 64,
            'code' => '3504052001',
            'name' => 'BOLOREJO',
            'district_id' => 5,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 65,
            'code' => '3504052002',
            'name' => 'KAUMAN',
            'district_id' => 5,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 66,
            'code' => '3504052003',
            'name' => 'BALEREJO',
            'district_id' => 5,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 67,
            'code' => '3504052004',
            'name' => 'BATANGSAREN',
            'district_id' => 5,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 68,
            'code' => '3504052005',
            'name' => 'PANGGUNGREJO',
            'district_id' => 5,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 69,
            'code' => '3504052006',
            'name' => 'KALANGBRET',
            'district_id' => 5,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 70,
            'code' => '3504052007',
            'name' => 'SIDOREJO',
            'district_id' => 5,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 71,
            'code' => '3504052008',
            'name' => 'MOJOSARI',
            'district_id' => 5,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 72,
            'code' => '3504052009',
            'name' => 'KARANGANOM',
            'district_id' => 5,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 73,
            'code' => '3504052010',
            'name' => 'PUCANGAN',
            'district_id' => 5,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 74,
            'code' => '3504052011',
            'name' => 'KATES',
            'district_id' => 5,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 75,
            'code' => '3504052012',
            'name' => 'BANARAN',
            'district_id' => 5,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 76,
            'code' => '3504052013',
            'name' => 'JATIMULYO',
            'district_id' => 5,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 77,
            'code' => '3504062001',
            'name' => 'WONOREJO',
            'district_id' => 6,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 78,
            'code' => '3504062002',
            'name' => 'KEDUNGCANGKRING',
            'district_id' => 6,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 79,
            'code' => '3504062003',
            'name' => 'MULYOSARI',
            'district_id' => 6,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 80,
            'code' => '3504062004',
            'name' => 'SEGAWE',
            'district_id' => 6,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 81,
            'code' => '3504062005',
            'name' => 'SAMAR',
            'district_id' => 6,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 82,
            'code' => '3504062006',
            'name' => 'PENJOR',
            'district_id' => 6,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 83,
            'code' => '3504062007',
            'name' => 'PAGERWOJO',
            'district_id' => 6,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 84,
            'code' => '3504062008',
            'name' => 'KRADINAN',
            'district_id' => 6,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 85,
            'code' => '3504062009',
            'name' => 'SIDOMULYO',
            'district_id' => 6,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 86,
            'code' => '3504062010',
            'name' => 'GONDANGGUNUNG',
            'district_id' => 6,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 87,
            'code' => '3504062011',
            'name' => 'GAMBIRAN',
            'district_id' => 6,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 88,
            'code' => '3504072001',
            'name' => 'KEDOYO',
            'district_id' => 7,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 89,
            'code' => '3504072002',
            'name' => 'NGLUTUNG',
            'district_id' => 7,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 90,
            'code' => '3504072003',
            'name' => 'TALANG',
            'district_id' => 7,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 91,
            'code' => '3504072004',
            'name' => 'KROSOK',
            'district_id' => 7,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 92,
            'code' => '3504072005',
            'name' => 'DONO',
            'district_id' => 7,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 93,
            'code' => '3504072006',
            'name' => 'TUGU',
            'district_id' => 7,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 94,
            'code' => '3504072007',
            'name' => 'PICISAN',
            'district_id' => 7,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 95,
            'code' => '3504072008',
            'name' => 'NYAWANGAN',
            'district_id' => 7,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 96,
            'code' => '3504072009',
            'name' => 'SENDANG',
            'district_id' => 7,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 97,
            'code' => '3504072010',
            'name' => 'NGLURUP',
            'district_id' => 7,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 98,
            'code' => '3504072011',
            'name' => 'GEGER',
            'district_id' => 7,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 99,
            'code' => '3504082001',
            'name' => 'BUNGUR',
            'district_id' => 8,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 100,
            'code' => '3504082002',
            'name' => 'BABADAN',
            'district_id' => 8,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 101,
            'code' => '3504082003',
            'name' => 'SUKOWIYONO',
            'district_id' => 8,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 102,
            'code' => '3504082004',
            'name' => 'SEMBON',
            'district_id' => 8,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 103,
            'code' => '3504082005',
            'name' => 'SUKOWIDODO',
            'district_id' => 8,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 104,
            'code' => '3504082006',
            'name' => 'TANJUNGSARI',
            'district_id' => 8,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 105,
            'code' => '3504082007',
            'name' => 'GEDANGAN',
            'district_id' => 8,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 106,
            'code' => '3504082008',
            'name' => 'SUKODONO',
            'district_id' => 8,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 107,
            'code' => '3504082009',
            'name' => 'KARANGREJO',
            'district_id' => 8,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 108,
            'code' => '3504082010',
            'name' => 'SUKOREJO',
            'district_id' => 8,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 109,
            'code' => '3504082011',
            'name' => 'PUNJUL',
            'district_id' => 8,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 110,
            'code' => '3504082012',
            'name' => 'JELI',
            'district_id' => 8,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 111,
            'code' => '3504082013',
            'name' => 'TULUNGREJO',
            'district_id' => 8,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 112,
            'code' => '3504092001',
            'name' => 'KENDAL',
            'district_id' => 9,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 113,
            'code' => '3504092002',
            'name' => 'TAWING',
            'district_id' => 9,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 114,
            'code' => '3504092003',
            'name' => 'GONDOSULI',
            'district_id' => 9,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 115,
            'code' => '3504092004',
            'name' => 'DUKUH',
            'district_id' => 9,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 116,
            'code' => '3504092005',
            'name' => 'SEPATAN',
            'district_id' => 9,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 117,
            'code' => '3504092006',
            'name' => 'MACANBANG',
            'district_id' => 9,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 118,
            'code' => '3504092007',
            'name' => 'KIPING',
            'district_id' => 9,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 119,
            'code' => '3504092008',
            'name' => 'REJOSARI',
            'district_id' => 9,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 120,
            'code' => '3504092009',
            'name' => 'BENDO',
            'district_id' => 9,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 121,
            'code' => '3504092010',
            'name' => 'NGRENDENG',
            'district_id' => 9,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 122,
            'code' => '3504092011',
            'name' => 'GONDANG',
            'district_id' => 9,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 123,
            'code' => '3504092012',
            'name' => 'BENDUNGAN',
            'district_id' => 9,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 124,
            'code' => '3504092013',
            'name' => 'NOTOREJO',
            'district_id' => 9,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 125,
            'code' => '3504092014',
            'name' => 'SIDEM',
            'district_id' => 9,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 126,
            'code' => '3504092015',
            'name' => 'SIDOMULYO',
            'district_id' => 9,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 127,
            'code' => '3504092016',
            'name' => 'BLENDIS',
            'district_id' => 9,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 128,
            'code' => '3504092017',
            'name' => 'MOJOARUM',
            'district_id' => 9,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 129,
            'code' => '3504092018',
            'name' => 'TIUDAN',
            'district_id' => 9,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 130,
            'code' => '3504092019',
            'name' => 'JARAKAN',
            'district_id' => 9,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 131,
            'code' => '3504092020',
            'name' => 'WONOKROMO',
            'district_id' => 9,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 132,
            'code' => '3504102001',
            'name' => 'SAMBIDOPLANG',
            'district_id' => 10,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 133,
            'code' => '3504102002',
            'name' => 'WATES',
            'district_id' => 10,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 134,
            'code' => '3504102003',
            'name' => 'MIRIGAMBAR',
            'district_id' => 10,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 135,
            'code' => '3504102004',
            'name' => 'TRENCENG',
            'district_id' => 10,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 136,
            'code' => '3504102005',
            'name' => 'BENDILWUNGU',
            'district_id' => 10,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 137,
            'code' => '3504102006',
            'name' => 'SAMBIJAJAR',
            'district_id' => 10,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 138,
            'code' => '3504102007',
            'name' => 'PODOREJO',
            'district_id' => 10,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 139,
            'code' => '3504102008',
            'name' => 'DOROAMPEL',
            'district_id' => 10,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 140,
            'code' => '3504102009',
            'name' => 'JUNJUNG',
            'district_id' => 10,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 141,
            'code' => '3504102010',
            'name' => 'TAMBAKREJO',
            'district_id' => 10,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 142,
            'code' => '3504102011',
            'name' => 'WONOREJO',
            'district_id' => 10,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 143,
            'code' => '3504102012',
            'name' => 'BENDILJATI KULON',
            'district_id' => 10,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 144,
            'code' => '3504102013',
            'name' => 'BENJATI WETAN',
            'district_id' => 10,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 145,
            'code' => '3504102014',
            'name' => 'SUMBERDADI',
            'district_id' => 10,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 146,
            'code' => '3504102015',
            'name' => 'JABALSARI',
            'district_id' => 10,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 147,
            'code' => '3504102016',
            'name' => 'SAMBIROBYONG',
            'district_id' => 10,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 148,
            'code' => '3504102017',
            'name' => 'BUKUR',
            'district_id' => 10,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 149,
            'code' => '3504112001',
            'name' => 'KARANGSONO',
            'district_id' => 11,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 150,
            'code' => '3504112002',
            'name' => 'SAMIR',
            'district_id' => 11,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 151,
            'code' => '3504112003',
            'name' => 'KACANGAN',
            'district_id' => 11,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 152,
            'code' => '3504112004',
            'name' => 'SELOREJO',
            'district_id' => 11,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 153,
            'code' => '3504112005',
            'name' => 'BALESONO',
            'district_id' => 11,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 154,
            'code' => '3504112006',
            'name' => 'PANDANSARI',
            'district_id' => 11,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 155,
            'code' => '3504112007',
            'name' => 'SUMBERINGIN KULON',
            'district_id' => 11,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 156,
            'code' => '3504112008',
            'name' => 'SUMBERINGIN KIDUL',
            'district_id' => 11,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 157,
            'code' => '3504112009',
            'name' => 'KALIWUNGU',
            'district_id' => 11,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 158,
            'code' => '3504112010',
            'name' => 'SUMBEREJO WETAN',
            'district_id' => 11,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 159,
            'code' => '3504112011',
            'name' => 'NGUNUT',
            'district_id' => 11,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 160,
            'code' => '3504112012',
            'name' => 'KALANGAN',
            'district_id' => 11,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 161,
            'code' => '3504112013',
            'name' => 'GILANG',
            'district_id' => 11,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 162,
            'code' => '3504112014',
            'name' => 'SUMBEREJO KULON',
            'district_id' => 11,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 163,
            'code' => '3504112015',
            'name' => 'PURWOREJO',
            'district_id' => 11,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 164,
            'code' => '3504112016',
            'name' => 'KROMASAN',
            'district_id' => 11,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 165,
            'code' => '3504112017',
            'name' => 'PULOSARI',
            'district_id' => 11,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 166,
            'code' => '3504112018',
            'name' => 'PULOTONDO',
            'district_id' => 11,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 167,
            'code' => '3504122001',
            'name' => 'PUCANGLABAN',
            'district_id' => 12,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 168,
            'code' => '3504122002',
            'name' => 'KALIDAWE',
            'district_id' => 12,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 169,
            'code' => '3504122003',
            'name' => 'PANGGUNGKALAK',
            'district_id' => 12,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 170,
            'code' => '3504122004',
            'name' => 'KALIGENTONG',
            'district_id' => 12,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 171,
            'code' => '3504122005',
            'name' => 'SUMBERBENDO',
            'district_id' => 12,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 172,
            'code' => '3504122006',
            'name' => 'MANDING',
            'district_id' => 12,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 173,
            'code' => '3504122007',
            'name' => 'PANGGUNGUNI',
            'district_id' => 12,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 174,
            'code' => '3504122008',
            'name' => 'SUMBERDADAP',
            'district_id' => 12,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 175,
            'code' => '3504122009',
            'name' => 'DEMUK',
            'district_id' => 12,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 176,
            'code' => '3504132001',
            'name' => 'TENGGUR',
            'district_id' => 13,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 177,
            'code' => '3504132002',
            'name' => 'PANJEREJO',
            'district_id' => 13,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 178,
            'code' => '3504132003',
            'name' => 'KARANGSARI',
            'district_id' => 13,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 179,
            'code' => '3504132004',
            'name' => 'TUGU',
            'district_id' => 13,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 180,
            'code' => '3504132005',
            'name' => 'SUKOREJO WETAN',
            'district_id' => 13,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 181,
            'code' => '3504132006',
            'name' => 'JATIDOWO',
            'district_id' => 13,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 182,
            'code' => '3504132007',
            'name' => 'BANJAREJO',
            'district_id' => 13,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 183,
            'code' => '3504132008',
            'name' => 'TANEN',
            'district_id' => 13,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 184,
            'code' => '3504132009',
            'name' => 'SUMBERAGUNG',
            'district_id' => 13,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 185,
            'code' => '3504132010',
            'name' => 'BLIMBING',
            'district_id' => 13,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 186,
            'code' => '3504132011',
            'name' => 'REJOTANGAN',
            'district_id' => 13,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 187,
            'code' => '3504132012',
            'name' => 'PAKISREJO',
            'district_id' => 13,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 188,
            'code' => '3504132013',
            'name' => 'TEGALREJO',
            'district_id' => 13,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 189,
            'code' => '3504132014',
            'name' => 'ARYOJEDING',
            'district_id' => 13,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 190,
            'code' => '3504132015',
            'name' => 'TENGGONG',
            'district_id' => 13,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 191,
            'code' => '3504132016',
            'name' => 'BUNTARAN',
            'district_id' => 13,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 192,
            'code' => '3504142001',
            'name' => 'KALIBATUR',
            'district_id' => 14,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 193,
            'code' => '3504142002',
            'name' => 'REJOSARI',
            'district_id' => 14,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 194,
            'code' => '3504142003',
            'name' => 'SUKOREJO KULON',
            'district_id' => 14,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 195,
            'code' => '3504142004',
            'name' => 'KALIDAWIR',
            'district_id' => 14,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 196,
            'code' => '3504142005',
            'name' => 'KARANGTALUN',
            'district_id' => 14,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 197,
            'code' => '3504142006',
            'name' => 'BANYUURIP',
            'district_id' => 14,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 198,
            'code' => '3504142007',
            'name' => 'WINONG',
            'district_id' => 14,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 199,
            'code' => '3504142008',
            'name' => 'JOHO',
            'district_id' => 14,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 200,
            'code' => '3504142009',
            'name' => 'PAKISAJI',
            'district_id' => 14,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 201,
            'code' => '3504142010',
            'name' => 'NGUBALAN',
            'district_id' => 14,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 202,
            'code' => '3504142011',
            'name' => 'TUNGGANGRI',
            'district_id' => 14,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 203,
            'code' => '3504142012',
            'name' => 'SALAKKEMBANG',
            'district_id' => 14,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 204,
            'code' => '3504142013',
            'name' => 'JABON',
            'district_id' => 14,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 205,
            'code' => '3504142014',
            'name' => 'DOMASAN',
            'district_id' => 14,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 206,
            'code' => '3504142015',
            'name' => 'TANJUNG',
            'district_id' => 14,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 207,
            'code' => '3504142016',
            'name' => 'BETAK',
            'district_id' => 14,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 208,
            'code' => '3504142017',
            'name' => 'PAGERSARI',
            'district_id' => 14,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 209,
            'code' => '3504152001',
            'name' => 'BESOLE',
            'district_id' => 15,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 210,
            'code' => '3504152002',
            'name' => 'TANGGULWELAHAN',
            'district_id' => 15,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 211,
            'code' => '3504152003',
            'name' => 'BESUKI',
            'district_id' => 15,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 212,
            'code' => '3504152004',
            'name' => 'KEBOIRENG',
            'district_id' => 15,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 213,
            'code' => '3504152005',
            'name' => 'TANGGULTURUS',
            'district_id' => 15,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 214,
            'code' => '3504152006',
            'name' => 'SEDAYUGUNUNG',
            'district_id' => 15,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 215,
            'code' => '3504152007',
            'name' => 'TANGGULKUNDUNG',
            'district_id' => 15,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 216,
            'code' => '3504152008',
            'name' => 'WATESKROYO',
            'district_id' => 15,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 217,
            'code' => '3504152009',
            'name' => 'SIYOTOBAGUS',
            'district_id' => 15,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 218,
            'code' => '3504152010',
            'name' => 'TULUNGREJO',
            'district_id' => 15,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 219,
            'code' => '3504162001',
            'name' => 'NGENTRONG',
            'district_id' => 16,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 220,
            'code' => '3504162002',
            'name' => 'GEDANGAN',
            'district_id' => 16,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 221,
            'code' => '3504162003',
            'name' => 'SAWO',
            'district_id' => 16,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 222,
            'code' => '3504162004',
            'name' => 'GAMPING',
            'district_id' => 16,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 223,
            'code' => '3504162005',
            'name' => 'CAMPURDARAT',
            'district_id' => 16,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 224,
            'code' => '3504162006',
            'name' => 'WATES',
            'district_id' => 16,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 225,
            'code' => '3504162007',
            'name' => 'PELEM',
            'district_id' => 16,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 226,
            'code' => '3504162008',
            'name' => 'POJOK',
            'district_id' => 16,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 227,
            'code' => '3504162009',
            'name' => 'TANGGUNG',
            'district_id' => 16,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 228,
            'code' => '3504172001',
            'name' => 'NGLAMPIR',
            'district_id' => 17,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 229,
            'code' => '3504172002',
            'name' => 'TALUNKULON',
            'district_id' => 17,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 230,
            'code' => '3504172003',
            'name' => 'BANTENGAN',
            'district_id' => 17,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 231,
            'code' => '3504172004',
            'name' => 'KEDUNGWILUT',
            'district_id' => 17,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 232,
            'code' => '3504172005',
            'name' => 'SUWARU',
            'district_id' => 17,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 233,
            'code' => '3504172006',
            'name' => 'NGUNGGAHAN',
            'district_id' => 17,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 234,
            'code' => '3504172007',
            'name' => 'SURUHAN KIDUL',
            'district_id' => 17,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 235,
            'code' => '3504172008',
            'name' => 'BANDUNG',
            'district_id' => 17,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 236,
            'code' => '3504172009',
            'name' => 'MERGAYU',
            'district_id' => 17,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 237,
            'code' => '3504172010',
            'name' => 'SEBALOR',
            'district_id' => 17,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 238,
            'code' => '3504172011',
            'name' => 'SUKOHARJO',
            'district_id' => 17,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 239,
            'code' => '3504172012',
            'name' => 'SOKO',
            'district_id' => 17,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 240,
            'code' => '3504172013',
            'name' => 'SINGGIT',
            'district_id' => 17,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 241,
            'code' => '3504172014',
            'name' => 'NGEPEH',
            'district_id' => 17,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 242,
            'code' => '3504172015',
            'name' => 'SURUHAN LOR',
            'district_id' => 17,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 243,
            'code' => '3504172016',
            'name' => 'BULUS',
            'district_id' => 17,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 244,
            'code' => '3504172017',
            'name' => 'KESAMBI',
            'district_id' => 17,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 245,
            'code' => '3504172018',
            'name' => 'GANDONG',
            'district_id' => 17,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 246,
            'code' => '3504182001',
            'name' => 'SAMBITAN',
            'district_id' => 18,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 247,
            'code' => '3504182002',
            'name' => 'BONO ',
            'district_id' => 18,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 248,
            'code' => '3504182003',
            'name' => 'SUKOANYAR',
            'district_id' => 18,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 249,
            'code' => '3504182004',
            'name' => 'DUWET',
            'district_id' => 18,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 250,
            'code' => '3504182005',
            'name' => 'TAMBAN',
            'district_id' => 18,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 251,
            'code' => '3504182006',
            'name' => 'NGEBONG',
            'district_id' => 18,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 252,
            'code' => '3504182007',
            'name' => 'SODO',
            'district_id' => 18,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 253,
            'code' => '3504182008',
            'name' => 'GOMBANG',
            'district_id' => 18,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 254,
            'code' => '3504182009',
            'name' => 'PAKEL',
            'district_id' => 18,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 255,
            'code' => '3504182010',
            'name' => 'SUWALUH',
            'district_id' => 18,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 256,
            'code' => '3504182011',
            'name' => 'PECUK',
            'district_id' => 18,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 257,
            'code' => '3504182012',
            'name' => 'BANGUNMULYO',
            'district_id' => 18,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 258,
            'code' => '3504182013',
            'name' => 'KASREMAN',
            'district_id' => 18,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 259,
            'code' => '3504182014',
            'name' => 'SANAN',
            'district_id' => 18,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 260,
            'code' => '3504182015',
            'name' => 'BANGUNJAYA',
            'district_id' => 18,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 261,
            'code' => '3504182016',
            'name' => 'NGRANCE',
            'district_id' => 18,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 262,
            'code' => '3504182017',
            'name' => 'GEBANG',
            'district_id' => 18,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 263,
            'code' => '3504182018',
            'name' => 'GESIKAN',
            'district_id' => 18,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 264,
            'code' => '3504182019',
            'name' => 'GEMPOLAN',
            'district_id' => 18,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 265,
            'code' => '3504192001',
            'name' => 'KRESIKAN',
            'district_id' => 19,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 266,
            'code' => '3504192002',
            'name' => 'JENGLUNGHARJO',
            'district_id' => 19,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 267,
            'code' => '3504192003',
            'name' => 'NGREJO',
            'district_id' => 19,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 268,
            'code' => '3504192004',
            'name' => 'TANGGUNGGUNUNG',
            'district_id' => 19,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 269,
            'code' => '3504192005',
            'name' => 'NGEPOH',
            'district_id' => 19,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 270,
            'code' => '3504192006',
            'name' => 'TENGGAREJO',
            'district_id' => 19,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Village::create([
            'id' => 271,
            'code' => '3504192007',
            'name' => 'PAKISREJO',
            'district_id' => 19,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
    }
}
