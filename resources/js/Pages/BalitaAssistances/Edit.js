import React, {useState} from 'react';
import { Inertia } from '@inertiajs/inertia';
import { InertiaLink, usePage, useForm } from '@inertiajs/inertia-react';
import Layout from '@/Shared/Layout';
import LoadingButton from '@/Shared/LoadingButton';
import TextInput from '@/Shared/TextInput';
import SelectInput from '@/Shared/SelectInput';
import DeleteButton from "@/Shared/DeleteButton";

const Edit = () => {
  const { balita, districts, villages, auth, opt_status, opt_posyandu } = usePage().props;
  const { data, setData, errors, put, processing } = useForm({
    date: balita.date || '',
    name: balita.name || '',
    gender: balita.gender,
    posyandu: balita.posyandu || '',
    age: balita.age || '',
    weight: balita.weight || '',
    parent_name: balita.parent_name || '',
    height: balita.height || '',
    gakin: balita.gakin,
    status: balita.status || '',
    village_id: balita.village_id || '',
    district_id: balita.district_id || '',
  });

  const currentDistrict = auth.user.district_id
  const defaultListVillages = currentDistrict ? villageByDistrict(currentDistrict) : villages
  const [dataVillages, setVillages] = useState(defaultListVillages);
  const [disableDistrict] = useState(currentDistrict ? true : false);

  function handleSubmit(e) {
    e.preventDefault();
    put(route('balita-asisstances.update', balita.id));
  }

  function selectDistrict(e) {
    setData('district_id', e.target.value)
    let filteredVillage = villageByDistrict(e.target.value);
    setVillages(filteredVillage)
  }

  function selectVillage(e) {
    setData('village_id', e.target.value)
  }

  function villageByDistrict(district_id) {
    return villages.filter(function(x) { return parseInt(district_id) === parseInt(x.district_id) })
  }

  function destroy() {
    if (confirm('Anda yakin ingin menghapus Pendampingan Balita ini?')) {
      Inertia.delete(route('balita-asisstances.destroy', balita.id));
    }
  }

  function dateFormat(date) {
    return new Date(date).toISOString().slice(0, 10);
  }

  function selectGender(e) {
    setData('gender', e.target.value)
  }

  return (
    <div>
      <h1 className="mb-8 text-3xl font-bold">
        <InertiaLink
          href={route('wedding-asisstances')}
          className="text-indigo-600 hover:text-indigo-700"
        >
          Pendampingan Balita
        </InertiaLink>
        <span className="font-medium text-indigo-600"> /</span> Edit
      </h1>
      <div className="overflow-hidden bg-white rounded shadow">
        <form onSubmit={handleSubmit}>
          <div className="flex flex-wrap p-8 -mb-8 -mr-6">
            <SelectInput
              className="w-full pb-8 pr-6 lg:w-1/2"
              label="Kecamatan"
              name="district_id"
              errors={errors.district_id}
              value={data.district_id}
              onChange={e => selectDistrict(e)}
              disabled={ disableDistrict }
            >
              <option value=""></option>
              {districts.map(({ id, name }) => (
                <option key={id} value={id}>
                  {name}
                </option>
              ))}
            </SelectInput>
            <SelectInput
              className="w-full pb-8 pr-6 lg:w-1/2"
              label="Desa"
              name="village_id"
              errors={errors.village_id}
              value={data.village_id}
              onChange={e => selectVillage(e)}
            >
              <option value=""></option>
              {dataVillages.map(({ id, name }) => (
                <option key={id} value={id}>
                  {name}
                </option>
              ))}
            </SelectInput>
            <TextInput
              className="w-full pb-8 pr-6 lg:w-1/2"
              label="Tanggal"
              name="date"
              type="date"
              errors={errors.date}
              value={dateFormat(data.date)}
              onChange={e => setData('date', e.target.value)}
            />
            <TextInput
              className="w-full pb-8 pr-6 lg:w-1/2"
              label="Nama"
              name="name"
              errors={errors.name}
              value={data.name}
              onChange={e => setData('name', e.target.value)}
            />
            <TextInput
              className="w-full pb-8 pr-6 lg:w-1/2"
              label="Umur (dalam bulan)"
              name="age"
              errors={errors.age}
              value={data.age}
              onChange={e => setData('age', e.target.value)}
            />
            <TextInput
              className="w-full pb-8 pr-6 lg:w-1/2"
              label="Tinggi"
              name="height"
              type="number"
              errors={errors.height}
              value={data.height}
              onChange={e => setData('height', e.target.value)}
            />
            <TextInput
              className="w-full pb-8 pr-6 lg:w-1/2"
              label="Berat"
              name="weight"
              type="number"
              errors={errors.weight}
              value={data.weight}
              onChange={e => setData('weight', e.target.value)}
            />
            <TextInput
              className="w-full pb-8 pr-6 lg:w-1/2"
              label="Nama Orang Tua"
              name="parent_name"
              errors={errors.parent_name}
              value={data.parent_name}
              onChange={e => setData('parent_name', e.target.value)}
            />
            <SelectInput
              className="w-full pb-8 pr-6 lg:w-1/2"
              label="Jenis Kelamin"
              name="gender"
              errors={errors.gender}
              value={data.gender}
              onChange={e => selectGender(e)}
            >
              <option value=""></option>
              <option value="0">Perempuan</option>
              <option value="1">Laki-laki</option>
            </SelectInput>
            <SelectInput
              className="w-full pb-8 pr-6 lg:w-1/2"
              label="Status"
              name="status"
              errors={errors.status}
              value={data.status}
              onChange={e => setData('status', e.target.value)}
            >
              <option value=""></option>
              {opt_status.map((value, index) => (
                <option key={index} value={value}>
                  {value}
                </option>
              ))}
            </SelectInput>
            <SelectInput
              className="w-full pb-8 pr-6 lg:w-1/2"
              label="Posyandu"
              name="posyandu"
              errors={errors.posyandu}
              value={data.posyandu}
              onChange={e => setData('posyandu', e.target.value)}
            >
              <option value=""></option>
              {opt_posyandu.map((value, index) => (
                <option key={index} value={value}>
                  {value}
                </option>
              ))}
            </SelectInput>
            <SelectInput
              className={"w-full pb-8 pr-6 lg:w-1/2"}
              label="Gakin"
              name="gakin"
              errors={errors.gakin}
              value={data.gakin}
              onChange={e => setData('gakin', e.target.value)}
            >
              <option value=""></option>
              <option value="1">Ya</option>
              <option value="0">Tidak</option>
            </SelectInput>
          </div>
          <div className="flex items-center px-8 py-4 bg-gray-100 border-t border-gray-200">
            <DeleteButton onDelete={destroy}>Hapus</DeleteButton>
            <LoadingButton
              loading={processing}
              type="submit"
              className="ml-auto btn-indigo"
            >
              Update
            </LoadingButton>
          </div>
        </form>
      </div>
    </div>
  );
};

Edit.layout = page => <Layout title="Edit Pendampingan Balita" children={page} />;

export default Edit;
