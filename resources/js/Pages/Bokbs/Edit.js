import React, {useState} from 'react';
import { Inertia } from '@inertiajs/inertia';
import { InertiaLink, usePage, useForm } from '@inertiajs/inertia-react';
import Layout from '@/Shared/Layout';
import LoadingButton from '@/Shared/LoadingButton';
import TextInput from '@/Shared/TextInput';
import SelectInput from '@/Shared/SelectInput';
import DeleteButton from "@/Shared/DeleteButton";

const Edit = () => {
  const { bokb, opt_activities, opt_locations, opt_source_person_from, districts, villages, auth } = usePage().props;
  const { data, setData, errors, put, processing } = useForm({
    activity: bokb.activity,
    location: bokb.location,
    link_document: bokb.link_document,
    source_person: bokb.source_person,
    source_person_from: bokb.source_person_from,
    warung: bokb.warung,
    district_id: bokb.district_id,
    village_id: bokb.village_id,
    date: bokb.date,
  });

  const currentDistrict = auth.user.district_id
  const defaultListVillages = data.district_id ? villageByDistrict(data.district_id) : villages
  const [dataVillages, setVillages] = useState(defaultListVillages);
  const [disableDistrict] = useState(currentDistrict ? true : false);

  function handleSubmit(e) {
    e.preventDefault();
    put(route('bokbs.update', bokb.id));
  }

  function destroy() {
    if (confirm('Anda yakin ingin menghapus Pertemuan BOKB ini?')) {
      Inertia.delete(route('bokbs.destroy', bokb.id));
    }
  }

  function selectDistrict(e) {
    setData('district_id', e.target.value)
    let filteredVillage = villageByDistrict(e.target.value);
    setVillages(filteredVillage)
  }

  function selectVillage(e) {
    setData('village_id', e.target.value)
  }

  function dateFormat(date) {
    return new Date(date).toISOString().slice(0, 10);
  }

  function villageByDistrict(district_id) {
    return villages.filter(function(x) { return parseInt(district_id) === parseInt(x.district_id) })
  }

  return (
    <div>
      <h1 className="mb-8 text-3xl font-bold">
        <InertiaLink
          href={route('bokbs')}
          className="text-indigo-600 hover:text-indigo-700"
        >
          Pertemuan BOKB
        </InertiaLink>
        <span className="font-medium text-indigo-600"> /</span> Edit
      </h1>
      <div className="overflow-hidden bg-white rounded shadow">
        <form onSubmit={handleSubmit}>
          <div className="flex flex-wrap p-8 -mb-8 -mr-6">
            <TextInput
              className="w-full pb-8 pr-6 lg:w-1/2"
              label="Tanggal"
              name="date"
              type="date"
              errors={errors.date}
              value={dateFormat(data.date)}
              onChange={e => setData('date', e.target.value)}
            />
            <SelectInput
              className="w-full pb-8 pr-6 lg:w-1/2"
              label="Kecamatan"
              name="district_id"
              errors={errors.district_id}
              value={data.district_id}
              onChange={e => selectDistrict(e)}
              disabled={ disableDistrict }
            >
              <option value=""></option>
              {districts.map(({ id, name }) => (
                <option key={id} value={id}>
                  {name}
                </option>
              ))}
            </SelectInput>
            <SelectInput
              className="w-full pb-8 pr-6 lg:w-1/2"
              label="Desa"
              name="village_id"
              errors={errors.village_id}
              value={data.village_id}
              onChange={e => selectVillage(e)}
            >
              <option value=""></option>
              {dataVillages.map(({ id, name }) => (
                <option key={id} value={id}>
                  {name}
                </option>
              ))}
            </SelectInput>
            <SelectInput
              className="w-full pb-8 pr-6 lg:w-1/2"
              label="Jenis kegiatan"
              name="activity"
              errors={errors.activity}
              value={data.activity}
              onChange={e => setData('activity', e.target.value)}
            >
              <option value=""></option>
              {opt_activities.map((value, index) => (
                <option key={index} value={value}>
                  {value}
                </option>
              ))}
            </SelectInput>
            <SelectInput
              className="w-full pb-8 pr-6 lg:w-1/2"
              label="Lokasi"
              name="location"
              errors={errors.location}
              value={data.location}
              onChange={e => setData('location', e.target.value)}
            >
              <option value=""></option>
              {opt_locations.map((value, index) => (
                <option key={index} value={value}>
                  {value}
                </option>
              ))}
            </SelectInput>
            <SelectInput
              className="w-full pb-8 pr-6 lg:w-1/2"
              label="Asal Narasumber"
              name="source_person_from"
              errors={errors.source_person_from}
              value={data.source_person_from}
              onChange={e => setData('source_person_from', e.target.value)}
            >
              <option value=""></option>
              {opt_source_person_from.map((value, index) => (
                <option key={index} value={value}>
                  {value}
                </option>
              ))}
            </SelectInput>
            <TextInput
              className="w-full pb-8 pr-6 lg:w-1/2"
              label="Narasumber"
              name="source_person"
              type="text"
              errors={errors.source_person}
              value={data.source_person}
              onChange={e => setData('source_person', e.target.value)}
            />
            <TextInput
              className="w-full pb-8 pr-6 lg:w-1/2"
              label="Warung"
              name="warung"
              type="text"
              errors={errors.warung}
              value={data.warung}
              onChange={e => setData('warung', e.target.value)}
            />
            <TextInput
              className="w-full pb-8 pr-6 lg:w-1/2"
              label="Link Document"
              name="link_document"
              type="text"
              errors={errors.link_document}
              value={data.link_document}
              onChange={e => setData('link_document', e.target.value)}
            />
          </div>
          <div className="flex items-center px-8 py-4 bg-gray-100 border-t border-gray-200">
            <DeleteButton onDelete={destroy}>Hapus</DeleteButton>
            <LoadingButton
              loading={processing}
              type="submit"
              className="ml-auto btn-indigo"
            >
              Update
            </LoadingButton>
          </div>
        </form>
      </div>
    </div>
  );
};

Edit.layout = page => <Layout title="Edit Pertemuan BOKB" children={page} />;

export default Edit;
