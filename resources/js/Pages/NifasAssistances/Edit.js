import React, {useState} from 'react';
import { Inertia } from '@inertiajs/inertia';
import { InertiaLink, usePage, useForm } from '@inertiajs/inertia-react';
import Layout from '@/Shared/Layout';
import LoadingButton from '@/Shared/LoadingButton';
import TextInput from '@/Shared/TextInput';
import SelectInput from '@/Shared/SelectInput';
import DeleteButton from "@/Shared/DeleteButton";

const Edit = () => {
  const { nifas, villages, districts, auth, opt_kb } = usePage().props;
  const { data, setData, errors, put, processing } = useForm({
    date: nifas.date,
    nik: nifas.nik,
    name: nifas.name,
    born: nifas.born,
    birthday: nifas.birthday,
    created_by: nifas.created_by,
    is_contraception: nifas.is_contraception,
    contraception: nifas.contraception,
    description: nifas.description,
    village_id: nifas.village_id,
    district_id: nifas.district_id,
  });

  const currentDistrict = auth.user.district_id
  const defaultListVillages = currentDistrict ? villageByDistrict(currentDistrict) : villages
  const [dataVillages, setVillages] = useState(defaultListVillages);
  const [disableDistrict] = useState(currentDistrict ? true : false);
  const [hideKb, setHiKb] = useState(parseInt(nifas.is_contraception) == 1 ? 'visible' : 'invisible');

  function handleSubmit(e) {
    e.preventDefault();
    put(route('nifas-asisstances.update', nifas.id));
  }

  function destroy() {
    if (confirm('Anda yakin ingin menghapus Pendampingan Nifas ini?')) {
      Inertia.delete(route('nifas-asisstances.destroy', nifas.id));
    }
  }

  function dateFormat(date) {
    return new Date(date).toISOString().slice(0, 10);
  }

  function selectKb(e) {
    setData('is_contraception', e.target.value)

    if (parseInt(e.target.value) == 1) {
      setHiKb('visible')
    } else {
      setHiKb('invisible')
    }
  }

  function selectDistrict(e) {
    setData('district_id', e.target.value)
    let filteredVillage = villageByDistrict(e.target.value);
    setVillages(filteredVillage)
  }

  function selectVillage(e) {
    setData('village_id', e.target.value)
  }

  function villageByDistrict(district_id) {
    return villages.filter(function(x) { return parseInt(district_id) === parseInt(x.district_id) })
  }

  return (
    <div>
      <h1 className="mb-8 text-3xl font-bold">
        <InertiaLink
          href={route('nifas-asisstances')}
          className="text-indigo-600 hover:text-indigo-700"
        >
          Pendampingan Nifas
        </InertiaLink>
        <span className="font-medium text-indigo-600"> /</span> Edit
      </h1>
      <div className="overflow-hidden bg-white rounded shadow">
        <form onSubmit={handleSubmit}>
          <div className="flex flex-wrap p-8 -mb-8 -mr-6">
            <SelectInput
              className="w-full pb-8 pr-6 lg:w-1/2"
              label="Kecamatan"
              name="district_id"
              errors={errors.district_id}
              value={data.district_id}
              onChange={e => selectDistrict(e)}
              disabled={ disableDistrict }
            >
              <option value=""></option>
              {districts.map(({ id, name }) => (
                <option key={id} value={id}>
                  {name}
                </option>
              ))}
            </SelectInput>
            <SelectInput
              className="w-full pb-8 pr-6 lg:w-1/2"
              label="Desa"
              name="village_id"
              errors={errors.village_id}
              value={data.village_id}
              onChange={e => selectVillage(e)}
            >
              <option value=""></option>
              {dataVillages.map(({ id, name }) => (
                <option key={id} value={id}>
                  {name}
                </option>
              ))}
            </SelectInput>
            <TextInput
              className="w-full pb-8 pr-6 lg:w-1/2"
              label="Tanggal Pencatatan"
              name="date"
              type="date"
              errors={errors.date}
              value={dateFormat(data.date)}
              onChange={e => setData('date', e.target.value)}
            />
            <TextInput
              className="w-full pb-8 pr-6 lg:w-1/2"
              label="NIK"
              name="nik"
              type="number"
              maxLength={16}
              errors={errors.nik}
              value={data.nik}
              onChange={e => setData('nik', e.target.value)}
            />
            <TextInput
              className="w-full pb-8 pr-6 lg:w-1/2"
              label="Nama"
              name="name"
              errors={errors.name}
              value={data.name}
              onChange={e => setData('name', e.target.value)}
            />
            <TextInput
              className="w-full pb-8 pr-6 lg:w-1/2"
              label="Tanggal Lahir"
              name="birthday"
              type="date"
              errors={errors.birthday}
              value={dateFormat(data.birthday)}
              onChange={e => setData('birthday', e.target.value)}
            />
            <TextInput
              className="w-full pb-8 pr-6 lg:w-1/2"
              label="Tanggal Persalinan"
              name="born"
              type="date"
              errors={errors.born}
              value={dateFormat(data.born)}
              onChange={e => setData('born', e.target.value)}
            />
            <TextInput
              className="w-full pb-8 pr-6 lg:w-1/2"
              label="Keterangan"
              name="description"
              errors={errors.description}
              value={data.description}
              onChange={e => setData('description', e.target.value)}
            />
            <SelectInput
              className="w-full pb-8 pr-6 lg:w-1/2"
              label="Kontrasepsi"
              name="is_contraception"
              errors={errors.is_contraception}
              value={data.is_contraception}
              onChange={e => selectKb(e)}
            >
              <option value=""></option>
              <option value="1">Ya</option>
              <option value="0">Tidak</option>
            </SelectInput>
            <SelectInput
              className={"w-full pb-8 pr-6 lg:w-1/2" + " " + hideKb}
              label="Alat Kontrasepsi"
              name="contraception"
              errors={errors.contraception}
              value={data.contraception}
              onChange={e => setData('contraception', e.target.value)}
            >
              <option value=""></option>
              {opt_kb.map((value, index) => (
                <option key={index} value={value}>
                  {value}
                </option>
              ))}
            </SelectInput>
          </div>
          <div className="flex items-center px-8 py-4 bg-gray-100 border-t border-gray-200">
            <DeleteButton onDelete={destroy}>Hapus</DeleteButton>
            <LoadingButton
              loading={processing}
              type="submit"
              className="ml-auto btn-indigo"
            >
              Update
            </LoadingButton>
          </div>
        </form>
      </div>
    </div>
  );
};

Edit.layout = page => <Layout title="Edit Pendampingan Nifas" children={page} />;

export default Edit;
