import React, {useState} from 'react';
import { Inertia } from '@inertiajs/inertia';
import { InertiaLink, usePage, useForm } from '@inertiajs/inertia-react';
import Layout from '@/Shared/Layout';
import LoadingButton from '@/Shared/LoadingButton';
import TextInput from '@/Shared/TextInput';
import SelectInput from '@/Shared/SelectInput';
import FileInput from "@/Shared/FileInput";
import DeleteButton from "@/Shared/DeleteButton";

const Edit = () => {
  const { pkk, categories, districts, villages, auth, levels } = usePage().props;
  const { data, setData, errors, put, processing } = useForm({
    pkk_activity_category_id: pkk.pkk_activity_category_id || '',
    district_id: pkk.district_id || '',
    village_id: pkk.village_id || '',
    date: pkk.date || '',
    description: pkk.description || '',
    source_person: pkk.source_person || '',
    source_person_from: pkk.source_person_from || '',
    target: pkk.target || '',
    target_amount: pkk.target_amount || '',
    location: pkk.location || '',
    level: pkk.level || '',
    photo: '',
  });

  const currentDistrict = auth.user.district_id
  const defaultListVillages = data.district_id ? villageByDistrict(data.district_id) : villages
  const [dataVillages, setVillages] = useState(defaultListVillages);
  const [hideFromLevel, setHideFromLevel] = useState(pkk.level === 'kabupaten' ? 'hidden' : 'block');
  const [disableDistrict] = useState(currentDistrict ? true : false);


  function handleSubmit(e) {
    e.preventDefault();
    put(route('pkk-activities.update', pkk.id));
  }

  function destroy() {
    if (confirm('Anda yakin ingin menghapus Aktivitas PKK & Desa ini?')) {
      Inertia.delete(route('pkk-activities.destroy', pkk.id));
    }
  }

  function selectCategory(e) {
    setData('pkk_activity_category_id', e.target.value)
  }

  function selectDistrict(e) {
    setData('district_id', e.target.value)
    let filteredVillage = villageByDistrict(e.target.value);
    setVillages(filteredVillage)
  }

  function selectVillage(e) {
    setData('village_id', e.target.value)
  }

  function dateFormat(date) {
    return new Date(date).toISOString().slice(0, 10);
  }

  function selectLevel(e) {
    setData('level', e.target.value)

    e.target.value === 'kabupaten' ? setHideFromLevel('hidden') : setHideFromLevel('block')
  }


  function villageByDistrict(district_id) {
    return villages.filter(function(x) { return parseInt(district_id) === parseInt(x.district_id) })
  }

  return (
    <div>
      <h1 className="mb-8 text-3xl font-bold">
        <InertiaLink
          href={route('pkk-activities')}
          className="text-indigo-600 hover:text-indigo-700"
        >
          Aktivitas PKK & Desa
        </InertiaLink>
        <span className="font-medium text-indigo-600"> /</span> Edit
      </h1>
      <div className="overflow-hidden bg-white rounded shadow">
        <form onSubmit={handleSubmit}>
          <div className="flex flex-wrap p-8 -mb-8 -mr-6">
            <SelectInput
              className="w-full pb-8 pr-6 lg:w-1/2"
              label="Tingkat"
              name="level"
              errors={errors.level}
              value={data.level}
              onChange={e => selectLevel(e)}
            >
              <option value=""></option>
              {levels.map((value, index) => (
                <option key={index} value={value}>
                  {value}
                </option>
              ))}
            </SelectInput>
            <SelectInput
              className="w-full pb-8 pr-6 lg:w-1/2"
              label="Kategori"
              name="pkk_activity_category_id"
              errors={errors.pkk_activity_category_id}
              value={data.pkk_activity_category_id}
              onChange={e => selectCategory(e)}
            >
              <option value=""></option>
              {categories.map(({ id, category }) => (
                <option key={id} value={id}>
                  {category}
                </option>
              ))}
            </SelectInput>
            <SelectInput
              className={"w-full pb-8 pr-6 lg:w-1/2" + " " + hideFromLevel}
              label="Kecamatan"
              name="district_id"
              errors={errors.district_id}
              value={data.district_id}
              onChange={e => selectDistrict(e)}
              disabled={ disableDistrict }
            >
              <option value=""></option>
              {districts.map(({ id, name }) => (
                <option key={id} value={id}>
                  {name}
                </option>
              ))}
            </SelectInput>
            <SelectInput
              className={"w-full pb-8 pr-6 lg:w-1/2" + " " + hideFromLevel}
              label="Desa"
              name="village_id"
              errors={errors.village_id}
              value={data.village_id}
              onChange={e => selectVillage(e)}
            >
              <option value=""></option>
              {dataVillages.map(({ id, name }) => (
                <option key={id} value={id}>
                  {name}
                </option>
              ))}
            </SelectInput>
            <TextInput
              className="w-full pb-8 pr-6 lg:w-1/2"
              label="Uraian"
              name="description"
              errors={errors.description}
              value={data.description}
              onChange={e => setData('description', e.target.value)}
            />
            <TextInput
              className="w-full pb-8 pr-6 lg:w-1/2"
              label="Tanggal"
              name="date"
              type="date"
              errors={errors.date}
              value={dateFormat(data.date)}
              onChange={e => setData('date', e.target.value)}
            />
            <TextInput
              className="w-full pb-8 pr-6 lg:w-1/2"
              label="Narasumber"
              name="source_person"
              type="text"
              errors={errors.source_person}
              value={data.source_person}
              onChange={e => setData('source_person', e.target.value)}
            />
            <TextInput
              className="w-full pb-8 pr-6 lg:w-1/2"
              label="Lembaga Narasumber"
              name="source_person_from"
              type="text"
              errors={errors.source_person_from}
              value={data.source_person_from}
              onChange={e => setData('source_person_from', e.target.value)}
            />
            <TextInput
              className="w-full pb-8 pr-6 lg:w-1/2"
              label="Sasaran"
              name="target"
              type="text"
              errors={errors.target}
              value={data.target}
              onChange={e => setData('target', e.target.value)}
            />
            <TextInput
              className="w-full pb-8 pr-6 lg:w-1/2"
              label="Jumlah Sasaran"
              name="target"
              type="number"
              min={1}
              errors={errors.target_amount}
              value={data.target_amount}
              onChange={e => setData('target_amount', e.target.value)}
            />
            <TextInput
              className="w-full pb-8 pr-6 lg:w-1/2"
              label="Tempat Kegiatan"
              name="location"
              type="text"
              errors={errors.location}
              value={data.location}
              onChange={e => setData('location', e.target.value)}
            />
            <FileInput
              className="w-full pb-8 pr-6 lg:w-1/2"
              label="Unggah Photo"
              name="photo"
              accept="image/*"
              errors={errors.photo}
              value={data.photo}
              onChange={photo => setData('photo', photo)}
            />
            <div className="w-full pb-8 pr-6 lg:w-1/2">
              <label className="form-label">
                Photo terunggah:
              </label>
              <img src={pkk.photo} alt={pkk.id}/>
            </div>
          </div>
          <div className="flex items-center px-8 py-4 bg-gray-100 border-t border-gray-200">
            <DeleteButton onDelete={destroy}>Hapus</DeleteButton>
            <LoadingButton
              loading={processing}
              type="submit"
              className="ml-auto btn-indigo"
            >
              Update
            </LoadingButton>
          </div>
        </form>
      </div>
    </div>
  );
};

Edit.layout = page => <Layout title="Edit Aktivitas PKK & Desa" children={page} />;

export default Edit;
