import React, {useRef} from 'react';
import { InertiaLink, usePage } from '@inertiajs/inertia-react';
import Layout from '@/Shared/Layout';
import Icon from '@/Shared/Icon';
import Pagination from '@/Shared/Pagination';
import SearchDateRange from "@/Shared/SearchDateRange";
import pickBy from "lodash/pickBy";
import { useReactToPrint } from 'react-to-print';

const Index = () => {
  const { pkk, filters } = usePage().props;
  const {
    data,
    meta: { links }
  } = pkk;

  const options = { weekday: 'long', year: 'numeric', month: 'short', day: 'numeric' };

  function dateParser(date) {
    let initDate  = new Date(date);
    return initDate.toLocaleDateString("id-ID", options);
  }

  const query = Object.keys(pickBy(filters)).length
    ? pickBy(filters)
    : {};

  const componentRef = useRef();
  const handlePrint = useReactToPrint({
    content: () => componentRef.current,
  });

  return (
    <div>
      <h1 className="text-3xl font-bold mb-1">Aktivitas PKK & Desa</h1>

      {(filters.date_start !== null && filters.date_end !== null) && (
        <p className="text-sm">( {dateParser(filters.date_start)} -   {dateParser(filters.date_end)} )</p>
      )}
      <div className="mb-8"/>
      <div className="flex items-center justify-between mb-6">
        <SearchDateRange />
        <InertiaLink
          className="btn-indigo focus:outline-none"
          href={route('pkk-activities.create')}
        >
          <span>Tambah</span>
        </InertiaLink>
      </div>
      {data.length !== 0 && (
        <div className="mb-6">
          <div className="flex -mx-2">
            <div className="px-2">
              <a
                className="inline-block px-6 py-2.5 bg-green-500 text-white font-medium text-xs leading-tight uppercase rounded shadow-md hover:bg-green-600 hover:shadow-lg focus:bg-green-600 focus:shadow-lg focus:outline-none focus:ring-0 active:bg-green-700 active:shadow-lg transition duration-150 ease-in-out"
                href={route('pkk-activities.export', query)}
              >
                <span>Export Excel</span>
              </a>
            </div>
            <div className="w-1/3 px-2">
              <a
                className="inline-block px-6 py-2.5 bg-gray-900 text-white font-medium text-xs leading-tight uppercase rounded shadow-md hover:bg-blue-500 hover:shadow-lg focus:bg-blue-500 focus:shadow-lg focus:outline-none focus:ring-0 active:bg-blue-600 active:shadow-lg transition duration-150 ease-in-out"
                onClick={handlePrint}
              >
                <span>Print</span>
              </a>
            </div>
          </div>
        </div>
      )}
      <div className="overflow-x-auto bg-white rounded shadow">
        <table className="w-full whitespace-nowrap" ref={componentRef}>
          <thead>
          <tr className="font-bold text-left">
            <th className="px-6 pt-5 pb-4" style={{color: 'white'}}>Foto</th>
            <th className="px-6 pt-5 pb-4">Tingkat</th>
            <th className="px-6 pt-5 pb-4">Tanggal</th>
            <th className="px-6 pt-5 pb-4">Kategori</th>
            <th className="px-6 pt-5 pb-4">Kecamatan</th>
            <th className="px-6 pt-5 pb-4">Desa</th>
            <th className="px-6 pt-5 pb-4">Deskripsi</th>
            <th className="px-6 pt-5 pb-4">Narasumber</th>
            <th className="px-6 pt-5 pb-4">Lembaga</th>
            <th className="px-6 pt-5 pb-4">Tempat Kegiatan</th>
            <th className="px-6 pt-5 pb-4">Sasaran</th>
            <th className="px-6 pt-5 pb-4" colSpan="2">
              Jumlah Sasaran
            </th>
          </tr>
          </thead>
          <tbody>
          {data.map(({ id, date, pkk_activity_category, description, source_person, photo, source_person_from,
                       district, village, location, target, target_amount, level }) => (

            <tr
              key={id}
              className="hover:bg-gray-100 focus-within:bg-gray-100"
            >
              <td className="border-t">
                <InertiaLink
                  tabIndex="-1"
                  href={route('pkk-activities.edit', id)}
                  className="flex items-center px-6 py-4 focus:text-indigo focus:outline-none"
                >
                  <img src={photo} alt={Date.now()} loading="lazy"/>
                </InertiaLink>
              </td>
              <td className="border-t">
                <InertiaLink
                  href={route('pkk-activities.edit', id)}
                  className="flex items-center px-6 py-4 focus:text-indigo-700 focus:outline-none"
                >
                  {level}
                </InertiaLink>
              </td>
              <td className="border-t">
                <InertiaLink
                  href={route('pkk-activities.edit', id)}
                  className="flex items-center px-6 py-4 focus:text-indigo-700 focus:outline-none"
                >
                  {dateParser(date)}
                </InertiaLink>
              </td>
              <td className="border-t">
                <InertiaLink
                  tabIndex="1"
                  className="flex items-center px-6 py-4 focus:text-indigo focus:outline-none"
                  href={route('pkk-activities.edit', id)}
                >
                  {pkk_activity_category ? pkk_activity_category.category : ''}
                </InertiaLink>
              </td>
              <td className="border-t">
                <InertiaLink
                  tabIndex="-1"
                  href={route('pkk-activities.edit', id)}
                  className="flex items-center px-6 py-4 focus:text-indigo focus:outline-none"
                >
                  {district ? district.name : ''}
                </InertiaLink>
              </td>
              <td className="border-t">
                <InertiaLink
                  tabIndex="-1"
                  href={route('pkk-activities.edit', id)}
                  className="flex items-center px-6 py-4 focus:text-indigo focus:outline-none"
                >
                  {village ? village.name : ''}
                </InertiaLink>
              </td>
              <td className="border-t">
                <InertiaLink
                  tabIndex="-1"
                  href={route('pkk-activities.edit', id)}
                  className="flex items-center px-6 py-4 focus:text-indigo focus:outline-none"
                >
                  {description}
                </InertiaLink>
              </td>
              <td className="border-t">
                <InertiaLink
                  tabIndex="-1"
                  href={route('pkk-activities.edit', id)}
                  className="flex items-center px-6 py-4 focus:text-indigo focus:outline-none"
                >
                  {source_person}
                </InertiaLink>
              </td>
              <td className="border-t">
                <InertiaLink
                  tabIndex="-1"
                  href={route('pkk-activities.edit', id)}
                  className="flex items-center px-6 py-4 focus:text-indigo focus:outline-none"
                >
                  {source_person_from}
                </InertiaLink>
              </td>
              <td className="border-t">
                <InertiaLink
                  tabIndex="-1"
                  href={route('pkk-activities.edit', id)}
                  className="flex items-center px-6 py-4 focus:text-indigo focus:outline-none"
                >
                  {location}
                </InertiaLink>
              </td>
              <td className="border-t">
                <InertiaLink
                  tabIndex="-1"
                  href={route('pkk-activities.edit', id)}
                  className="flex items-center px-6 py-4 focus:text-indigo focus:outline-none"
                >
                  {target}
                </InertiaLink>
              </td>
              <td className="border-t">
                <InertiaLink
                  tabIndex="-1"
                  href={route('pkk-activities.edit', id)}
                  className="flex items-center px-6 py-4 focus:text-indigo focus:outline-none"
                >
                  {target_amount}
                </InertiaLink>
              </td>
              <td className="w-px border-t">
                <InertiaLink
                  tabIndex="-1"
                  href={route('pkk-activities.edit', id)}
                  className="flex items-center px-4 focus:outline-none"
                >
                  <Icon
                    name="cheveron-right"
                    className="block w-6 h-6 text-gray-400 fill-current"
                  />
                </InertiaLink>
              </td>
            </tr>
          ))}
          {data.length === 0 && (
            <tr>
              <td className="px-6 py-4 border-t" colSpan="4">
                No PKK found.
              </td>
            </tr>
          )}
          </tbody>
        </table>
      </div>
      <Pagination links={links} />
    </div>
  );
};

Index.layout = page => <Layout title="Aktivitas PKK & Desa" children={page} />;

export default Index;
