import React, {useState} from 'react';
import { Inertia } from '@inertiajs/inertia';
import { InertiaLink, usePage, useForm } from '@inertiajs/inertia-react';
import Layout from '@/Shared/Layout';
import LoadingButton from '@/Shared/LoadingButton';
import TextInput from '@/Shared/TextInput';
import SelectInput from '@/Shared/SelectInput';
import FileInput from "@/Shared/FileInput";
import DeleteButton from "@/Shared/DeleteButton";

const Edit = () => {
  const { pkk } = usePage().props;
  const { data, setData, errors, put, processing } = useForm({
    name: pkk.name || '',
    number: pkk.number || '',
    date: pkk.date || '',
    description: pkk.description || '',
    issued_by: pkk.issued_by || ''
  });

  function handleSubmit(e) {
    e.preventDefault();
    put(route('pkk-regulations.update', pkk.id));
  }

  function destroy() {
    if (confirm('Anda yakin ingin menghapus regulasi ini?')) {
      Inertia.delete(route('pkk-regulations.destroy', pkk.id));
    }
  }

  function dateFormat(date) {
    return new Date(date).toISOString().slice(0, 10);
  }

  return (
    <div>
      <h1 className="mb-8 text-3xl font-bold">
        <InertiaLink
          href={route('pkk-regulations')}
          className="text-indigo-600 hover:text-indigo-700"
        >
          Regulasi
        </InertiaLink>
        <span className="font-medium text-indigo-600"> /</span> Edit
      </h1>
      <div className="overflow-hidden bg-white rounded shadow">
        <form onSubmit={handleSubmit}>
          <div className="flex flex-wrap p-8 -mb-8 -mr-6">
            <TextInput
              className="w-full pb-8 pr-6 lg:w-1/2"
              label="Tanggal"
              name="date"
              type="date"
              errors={errors.date}
              value={dateFormat(data.date)}
              onChange={e => setData('date', e.target.value)}
            />
            <TextInput
              className="w-full pb-8 pr-6 lg:w-1/2"
              label="Nomor"
              name="number"
              errors={errors.number}
              value={data.number}
              onChange={e => setData('number', e.target.value)}
            />
            <TextInput
              className="w-full pb-8 pr-6 lg:w-1/2"
              label="Nama"
              name="name"
              errors={errors.name}
              value={data.name}
              onChange={e => setData('name', e.target.value)}
            />
            <TextInput
              className="w-full pb-8 pr-6 lg:w-1/2"
              label="Uraian"
              name="description"
              errors={errors.description}
              value={data.description}
              onChange={e => setData('description', e.target.value)}
            />
            <TextInput
              className="w-full pb-8 pr-6 lg:w-1/2"
              label="Dikeluarkan Oleh"
              name="issued_by"
              type="text"
              errors={errors.issued_by}
              value={data.issued_by}
              onChange={e => setData('issued_by', e.target.value)}
            />
          </div>
          <div className="flex items-center px-8 py-4 bg-gray-100 border-t border-gray-200">
            <DeleteButton onDelete={destroy}>Hapus</DeleteButton>
            <LoadingButton
              loading={processing}
              type="submit"
              className="ml-auto btn-indigo"
            >
              Update
            </LoadingButton>
          </div>
        </form>
      </div>
    </div>
  );
};

Edit.layout = page => <Layout title="Edit Regulasi" children={page} />;

export default Edit;
