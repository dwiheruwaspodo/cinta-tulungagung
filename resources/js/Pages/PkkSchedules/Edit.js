import React, {useState} from 'react';
import { Inertia } from '@inertiajs/inertia';
import { InertiaLink, usePage, useForm } from '@inertiajs/inertia-react';
import Layout from '@/Shared/Layout';
import LoadingButton from '@/Shared/LoadingButton';
import TextInput from '@/Shared/TextInput';
import SelectInput from '@/Shared/SelectInput';
import DeleteButton from "@/Shared/DeleteButton";

const Edit = () => {
  const { pkk, categories, districts, villages, auth } = usePage().props;
  const { data, setData, errors, put, processing } = useForm({
    pkk_activity_category_id: pkk.pkk_activity_category_id || '',
    district_id: pkk.district_id || '',
    village_id: pkk.village_id || '',
    date: pkk.date || '',
    description: pkk.description || '',
    location: pkk.location || '',
  });

  const currentDistrict = auth.user.district_id
  const defaultListVillages = data.district_id ? villageByDistrict(data.district_id) : villages
  const [dataVillages, setVillages] = useState(defaultListVillages);
  const [disableDistrict] = useState(currentDistrict ? true : false);


  function handleSubmit(e) {
    e.preventDefault();
    put(route('pkk-schedules.update', pkk.id));
  }

  function destroy() {
    if (confirm('Anda yakin ingin menghapus Rencana Kerja PKK & Desa ini?')) {
      Inertia.delete(route('pkk-schedules.destroy', pkk.id));
    }
  }

  function selectCategory(e) {
    setData('pkk_activity_category_id', e.target.value)
  }

  function selectDistrict(e) {
    setData('district_id', e.target.value)
    let filteredVillage = villageByDistrict(e.target.value);
    setVillages(filteredVillage)
  }

  function selectVillage(e) {
    setData('village_id', e.target.value)
  }

  function dateFormat(date) {
    return new Date(date).toISOString().slice(0, 10);
  }

  function villageByDistrict(district_id) {
    return villages.filter(function(x) { return parseInt(district_id) === parseInt(x.district_id) })
  }

  return (
    <div>
      <h1 className="mb-8 text-3xl font-bold">
        <InertiaLink
          href={route('pkk-schedules')}
          className="text-indigo-600 hover:text-indigo-700"
        >
          Rencana Kerja PKK & Desa
        </InertiaLink>
        <span className="font-medium text-indigo-600"> /</span> Edit
      </h1>
      <div className="overflow-hidden bg-white rounded shadow">
        <form onSubmit={handleSubmit}>
          <div className="flex flex-wrap p-8 -mb-8 -mr-6">
            <SelectInput
              className="w-full pb-8 pr-6 lg:w-1/2"
              label="Kategori"
              name="pkk_activity_category_id"
              errors={errors.pkk_activity_category_id}
              value={data.pkk_activity_category_id}
              onChange={e => selectCategory(e)}
            >
              <option value=""></option>
              {categories.map(({ id, category }) => (
                <option key={id} value={id}>
                  {category}
                </option>
              ))}
            </SelectInput>
            <TextInput
              className="w-full pb-8 pr-6 lg:w-1/2"
              label="Uraian"
              name="description"
              errors={errors.description}
              value={data.description}
              onChange={e => setData('description', e.target.value)}
            />
            <TextInput
              className="w-full pb-8 pr-6 lg:w-1/2"
              label="Tanggal"
              name="date"
              type="date"
              errors={errors.date}
              value={dateFormat(data.date)}
              onChange={e => setData('date', e.target.value)}
            />
            <SelectInput
              className="w-full pb-8 pr-6 lg:w-1/2"
              label="Kecamatan"
              name="district_id"
              errors={errors.district_id}
              value={data.district_id}
              onChange={e => selectDistrict(e)}
              disabled={ disableDistrict }
            >
              <option value=""></option>
              {districts.map(({ id, name }) => (
                <option key={id} value={id}>
                  {name}
                </option>
              ))}
            </SelectInput>
            <SelectInput
              className="w-full pb-8 pr-6 lg:w-1/2"
              label="Desa"
              name="village_id"
              errors={errors.village_id}
              value={data.village_id}
              onChange={e => selectVillage(e)}
            >
              <option value=""></option>
              {dataVillages.map(({ id, name }) => (
                <option key={id} value={id}>
                  {name}
                </option>
              ))}
            </SelectInput>
            <TextInput
              className="w-full pb-8 pr-6 lg:w-1/2"
              label="Tempat Kegiatan"
              name="location"
              type="text"
              errors={errors.location}
              value={data.location}
              onChange={e => setData('location', e.target.value)}
            />
          </div>
          <div className="flex items-center px-8 py-4 bg-gray-100 border-t border-gray-200">
            <DeleteButton onDelete={destroy}>Hapus</DeleteButton>
            <LoadingButton
              loading={processing}
              type="submit"
              className="ml-auto btn-indigo"
            >
              Update
            </LoadingButton>
          </div>
        </form>
      </div>
    </div>
  );
};

Edit.layout = page => <Layout title="Edit Rencana Kerja PKK & Desa" children={page} />;

export default Edit;
