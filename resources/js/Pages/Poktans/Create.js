import React, {useEffect, useState} from 'react';
import { Inertia } from '@inertiajs/inertia';
import { InertiaLink, usePage, useForm } from '@inertiajs/inertia-react';
import Layout from '@/Shared/Layout';
import LoadingButton from '@/Shared/LoadingButton';
import TextInput from '@/Shared/TextInput';
import SelectInput from '@/Shared/SelectInput';

const Create = () => {
  const { districts, villages, auth } = usePage().props;
  const { data, setData, errors, post, processing } = useForm({
    district_id: auth.user.district_id || '',
    village_id: '',
    date: '',
    keluarga_bkb: '',
    anggota_hadir_bkb: '',
    keluarga_bkr: '',
    anggota_hadir_bkr: '',
    keluarga_bkl: '',
    anggota_hadir_bkl: '',
    keluarga_uppks: '',
    anggota_hadir_uppks: '',
    keluarga_pikr: '',
    anggota_hadir_pikr: ''
  });

  const currentDistrict = auth.user.district_id
  const defaultListVillages = currentDistrict ? villageByDistrict(currentDistrict) : villages
  const [dataVillages, setVillages] = useState(defaultListVillages);
  const [disableDistrict] = useState(currentDistrict ? true : false);

  function handleSubmit(e) {
    e.preventDefault();
    post(route('poktans.store'));
  }

  function selectDistrict(e) {
    setData('district_id', e.target.value)
    let filteredVillage = villageByDistrict(e.target.value);
    setVillages(filteredVillage)
  }

  function selectVillage(e) {
    setData('village_id', e.target.value)
  }

  function villageByDistrict(district_id) {
    return villages.filter(function(x) { return parseInt(district_id) === parseInt(x.district_id) })
  }

  return (
    <div>
      <h1 className="mb-8 text-3xl font-bold">
        <InertiaLink
          href={route('poktans')}
          className="text-indigo-600 hover:text-indigo-700"
        >
          Pertemuan Poktan
        </InertiaLink>
        <span className="font-medium text-indigo-600"> /</span> Tambah
      </h1>
      <div className="overflow-hidden bg-white rounded shadow">
        <form onSubmit={handleSubmit}>
          <div className="flex flex-wrap p-8 -mb-8 -mr-6">
            <TextInput
              className="w-full pb-8 pr-6 lg:w-1/2"
              label="Bulan dan Tahun"
              name="date"
              type="month"
              errors={errors.date}
              value={data.date}
              onChange={e => setData('date', e.target.value)}
            />
            <SelectInput
              className="w-full pb-8 pr-6 lg:w-1/2"
              label="Kecamatan"
              name="district_id"
              errors={errors.district_id}
              value={data.district_id}
              onChange={e => selectDistrict(e)}
              disabled={ disableDistrict }
            >
              <option value=""></option>
              {districts.map(({ id, name }) => (
                <option key={id} value={id}>
                  {name}
                </option>
              ))}
            </SelectInput>
            <SelectInput
              className="w-full pb-8 pr-6 lg:w-1/2"
              label="Desa"
              name="village_id"
              errors={errors.village_id}
              value={data.village_id}
              onChange={e => selectVillage(e)}
            >
              <option value=""></option>
              {dataVillages.map(({ id, name }) => (
                <option key={id} value={id}>
                  {name}
                </option>
              ))}
            </SelectInput>
            <TextInput
              className="w-full pb-8 pr-6 lg:w-1/2"
              label="Keluarga BKB"
              name="keluarga_bkb"
              type="number"
              errors={errors.keluarga_bkb}
              value={data.keluarga_bkb}
              onChange={e => setData('keluarga_bkb', e.target.value)}
            />
            <TextInput
              className="w-full pb-8 pr-6 lg:w-1/2"
              label="Anggota Hadir BKB"
              name="anggota_hadir_bkb"
              type="number"
              errors={errors.anggota_hadir_bkb}
              value={data.anggota_hadir_bkb}
              onChange={e => setData('anggota_hadir_bkb', e.target.value)}
            />
            <TextInput
              className="w-full pb-8 pr-6 lg:w-1/2"
              label="Keluarga BKR"
              name="keluarga_bkr"
              type="number"
              errors={errors.keluarga_bkr}
              value={data.keluarga_bkr}
              onChange={e => setData('keluarga_bkr', e.target.value)}
            />
            <TextInput
              className="w-full pb-8 pr-6 lg:w-1/2"
              label="Anggota Hadir BKR"
              name="anggota_hadir_bkr"
              type="number"
              errors={errors.anggota_hadir_bkr}
              value={data.anggota_hadir_bkr}
              onChange={e => setData('anggota_hadir_bkr', e.target.value)}
            />
            <TextInput
              className="w-full pb-8 pr-6 lg:w-1/2"
              label="Keluarga BKL"
              name="keluarga_bkl"
              type="number"
              errors={errors.keluarga_bkl}
              value={data.keluarga_bkl}
              onChange={e => setData('keluarga_bkl', e.target.value)}
            />
            <TextInput
              className="w-full pb-8 pr-6 lg:w-1/2"
              label="Anggota Hadir BKL"
              name="anggota_hadir_bkl"
              type="number"
              errors={errors.anggota_hadir_bkl}
              value={data.anggota_hadir_bkl}
              onChange={e => setData('anggota_hadir_bkl', e.target.value)}
            />
            <TextInput
              className="w-full pb-8 pr-6 lg:w-1/2"
              label="Keluarga UPPKS"
              name="keluarga_uppks"
              type="number"
              errors={errors.keluarga_uppks}
              value={data.keluarga_uppks}
              onChange={e => setData('keluarga_uppks', e.target.value)}
            />
            <TextInput
              className="w-full pb-8 pr-6 lg:w-1/2"
              label="Anggota Hadir UPPKS"
              name="anggota_hadir_uppks"
              type="number"
              errors={errors.anggota_hadir_uppks}
              value={data.anggota_hadir_uppks}
              onChange={e => setData('anggota_hadir_uppks', e.target.value)}
            />
            <TextInput
              className="w-full pb-8 pr-6 lg:w-1/2"
              label="Keluarga PIKR"
              name="keluarga_pikr"
              type="number"
              errors={errors.keluarga_pikr}
              value={data.keluarga_pikr}
              onChange={e => setData('keluarga_pikr', e.target.value)}
            />
            <TextInput
              className="w-full pb-8 pr-6 lg:w-1/2"
              label="Anggota Hadir PIKR"
              name="anggota_hadir_pikr"
              type="number"
              errors={errors.anggota_hadir_pikr}
              value={data.anggota_hadir_pikr}
              onChange={e => setData('anggota_hadir_pikr', e.target.value)}
            />
          </div>
          <div className="flex items-center justify-end px-8 py-4 bg-gray-100 border-t border-gray-200">
            <LoadingButton
              loading={processing}
              type="submit"
              className="btn-indigo"
            >
              Tambah
            </LoadingButton>
          </div>
        </form>
      </div>
    </div>
  );
};

Create.layout = page => <Layout title="Tambah Pertemuan Poktan" children={page} />;

export default Create;
