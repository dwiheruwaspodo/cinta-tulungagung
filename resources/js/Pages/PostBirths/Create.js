import React, {useEffect, useState} from 'react';
import { Inertia } from '@inertiajs/inertia';
import { InertiaLink, usePage, useForm } from '@inertiajs/inertia-react';
import Layout from '@/Shared/Layout';
import LoadingButton from '@/Shared/LoadingButton';
import TextInput from '@/Shared/TextInput';
import SelectInput from '@/Shared/SelectInput';

const Create = () => {
  const { districts, villages, auth } = usePage().props;
  const { data, setData, errors, post, processing } = useForm({
    district_id: auth.user.district_id || '',
    village_id: '',
    date: '',
    birth_suntik: '',
    birth_pil: '',
    birth_kondom: '',
    birth_iud: '',
    birth_implan: '',
    birth_mop: '',
    birth_mow: '',
    miscarriage_suntik: '',
    miscarriage_pil: '',
    miscarriage_kondom: '',
    miscarriage_iud: '',
    miscarriage_implan: '',
    miscarriage_mop: '',
    miscarriage_mow: ''
  });

  const currentDistrict = auth.user.district_id
  const defaultListVillages = currentDistrict ? villageByDistrict(currentDistrict) : villages
  const [dataVillages, setVillages] = useState(defaultListVillages);
  const [disableDistrict] = useState(currentDistrict ? true : false);

  function handleSubmit(e) {
    e.preventDefault();
    post(route('post-births.store'));
  }

  function selectDistrict(e) {
    setData('district_id', e.target.value)
    let filteredVillage = villageByDistrict(e.target.value);
    setVillages(filteredVillage)
  }

  function selectVillage(e) {
    setData('village_id', e.target.value)
  }

  function villageByDistrict(district_id) {
    return villages.filter(function(x) { return parseInt(district_id) === parseInt(x.district_id) })
  }

  return (
    <div>
      <h1 className="mb-8 text-3xl font-bold">
        <InertiaLink
          href={route('post-births')}
          className="text-indigo-600 hover:text-indigo-700"
        >
          Pasca Persalinan
        </InertiaLink>
        <span className="font-medium text-indigo-600"> /</span> Tambah
      </h1>
      <div className="overflow-hidden bg-white rounded shadow">
        <form onSubmit={handleSubmit}>
          <div className="flex flex-wrap p-8 -mb-8 -mr-6">
            <TextInput
              className="w-full pb-8 pr-6 lg:w-1/2"
              label="Bulan dan Tahun"
              name="date"
              type="month"
              errors={errors.date}
              value={data.date}
              onChange={e => setData('date', e.target.value)}
            />
            <SelectInput
              className="w-full pb-8 pr-6 lg:w-1/2"
              label="Kecamatan"
              name="district_id"
              errors={errors.district_id}
              value={data.district_id}
              onChange={e => selectDistrict(e)}
              disabled={ disableDistrict }
            >
              <option value=""></option>
              {districts.map(({ id, name }) => (
                <option key={id} value={id}>
                  {name}
                </option>
              ))}
            </SelectInput>
            <SelectInput
              className="w-full pb-8 pr-6 lg:w-1/2"
              label="Desa"
              name="village_id"
              errors={errors.village_id}
              value={data.village_id}
              onChange={e => selectVillage(e)}
            >
              <option value=""></option>
              {dataVillages.map(({ id, name }) => (
                <option key={id} value={id}>
                  {name}
                </option>
              ))}
            </SelectInput>
            <TextInput
              className="w-full pb-8 pr-6 lg:w-1/2"
              label="Pasca Persalinan Suntik"
              name="birth_suntik"
              type="number"
              errors={errors.birth_suntik}
              value={data.birth_suntik}
              onChange={e => setData('birth_suntik', e.target.value)}
            />
            <TextInput
              className="w-full pb-8 pr-6 lg:w-1/2"
              label="Pasca Persalinan Pil"
              name="birth_pil"
              type="number"
              errors={errors.birth_pil}
              value={data.birth_pil}
              onChange={e => setData('birth_pil', e.target.value)}
            />
            <TextInput
              className="w-full pb-8 pr-6 lg:w-1/2"
              label="Pasca Persalinan Kondom"
              name="birth_kondom"
              type="number"
              errors={errors.birth_kondom}
              value={data.birth_kondom}
              onChange={e => setData('birth_kondom', e.target.value)}
            />
            <TextInput
              className="w-full pb-8 pr-6 lg:w-1/2"
              label="Pasca Persalinan IUD"
              name="birth_iud"
              type="number"
              errors={errors.birth_iud}
              value={data.birth_iud}
              onChange={e => setData('birth_iud', e.target.value)}
            />
            <TextInput
              className="w-full pb-8 pr-6 lg:w-1/2"
              label="Pasca Persalinan Implan"
              name="birth_implan"
              type="number"
              errors={errors.birth_implan}
              value={data.birth_implan}
              onChange={e => setData('birth_implan', e.target.value)}
            />
            <TextInput
              className="w-full pb-8 pr-6 lg:w-1/2"
              label="Pasca Persalinan MOP"
              name="birth_mop"
              type="number"
              errors={errors.birth_mop}
              value={data.birth_mop}
              onChange={e => setData('birth_mop', e.target.value)}
            />
            <TextInput
              className="w-full pb-8 pr-6 lg:w-1/2"
              label="Pasca Persalinan MOW"
              name="birth_mow"
              type="number"
              errors={errors.birth_mow}
              value={data.birth_mow}
              onChange={e => setData('birth_mow', e.target.value)}
            />
            <TextInput
              className="w-full pb-8 pr-6 lg:w-1/2"
              label="Pasca Keguguran Suntik"
              name="miscarriage_suntik"
              type="number"
              errors={errors.miscarriage_suntik}
              value={data.miscarriage_suntik}
              onChange={e => setData('miscarriage_suntik', e.target.value)}
            />
            <TextInput
              className="w-full pb-8 pr-6 lg:w-1/2"
              label="Pasca Keguguran Pil"
              name="miscarriage_pil"
              type="number"
              errors={errors.miscarriage_pil}
              value={data.miscarriage_pil}
              onChange={e => setData('miscarriage_pil', e.target.value)}
            />
            <TextInput
              className="w-full pb-8 pr-6 lg:w-1/2"
              label="Pasca Keguguran Kondom"
              name="miscarriage_kondom"
              type="number"
              errors={errors.miscarriage_kondom}
              value={data.miscarriage_kondom}
              onChange={e => setData('miscarriage_kondom', e.target.value)}
            />
            <TextInput
              className="w-full pb-8 pr-6 lg:w-1/2"
              label="Pasca Keguguran IUD"
              name="miscarriage_iud"
              type="number"
              errors={errors.miscarriage_iud}
              value={data.miscarriage_iud}
              onChange={e => setData('miscarriage_iud', e.target.value)}
            />
            <TextInput
              className="w-full pb-8 pr-6 lg:w-1/2"
              label="Pasca Keguguran Implan"
              name="miscarriage_implan"
              type="number"
              errors={errors.miscarriage_implan}
              value={data.miscarriage_implan}
              onChange={e => setData('miscarriage_implan', e.target.value)}
            />
            <TextInput
              className="w-full pb-8 pr-6 lg:w-1/2"
              label="Pasca Keguguran MOP"
              name="miscarriage_mop"
              type="number"
              errors={errors.miscarriage_mop}
              value={data.miscarriage_mop}
              onChange={e => setData('miscarriage_mop', e.target.value)}
            />
            <TextInput
              className="w-full pb-8 pr-6 lg:w-1/2"
              label="Pasca Keguguran MOW"
              name="miscarriage_mow"
              type="number"
              errors={errors.miscarriage_mow}
              value={data.miscarriage_mow}
              onChange={e => setData('miscarriage_mow', e.target.value)}
            />
          </div>
          <div className="flex items-center justify-end px-8 py-4 bg-gray-100 border-t border-gray-200">
            <LoadingButton
              loading={processing}
              type="submit"
              className="btn-indigo"
            >
              Tambah
            </LoadingButton>
          </div>
        </form>
      </div>
    </div>
  );
};

Create.layout = page => <Layout title="Tambah Pasca Persalinan" children={page} />;

export default Create;
