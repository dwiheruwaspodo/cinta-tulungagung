import React, {useEffect, useState} from 'react';
import { Inertia } from '@inertiajs/inertia';
import { InertiaLink, usePage, useForm } from '@inertiajs/inertia-react';
import Layout from '@/Shared/Layout';
import LoadingButton from '@/Shared/LoadingButton';
import TextInput from '@/Shared/TextInput';
import SelectInput from '@/Shared/SelectInput';
import DeleteButton from "@/Shared/DeleteButton";

const Edit = () => {
  const { postBirth, districts, villages, auth } = usePage().props;
  const { data, setData, errors, put, processing } = useForm({
    district_id: postBirth.district_id || '',
    village_id: postBirth.village_id || '',
    date: postBirth.date || '',
    birth_suntik: postBirth.birth_suntik,
    birth_pil: postBirth.birth_pil,
    birth_kondom: postBirth.birth_kondom,
    birth_iud: postBirth.birth_iud,
    birth_implan: postBirth.birth_implan,
    birth_mop: postBirth.birth_mop,
    birth_mow: postBirth.birth_mow,
    miscarriage_suntik: postBirth.miscarriage_suntik,
    miscarriage_pil: postBirth.miscarriage_pil,
    miscarriage_kondom: postBirth.miscarriage_kondom,
    miscarriage_iud: postBirth.miscarriage_iud,
    miscarriage_implan: postBirth.miscarriage_implan,
    miscarriage_mop: postBirth.miscarriage_mop,
    miscarriage_mow: postBirth.miscarriage_mow
  });

  const currentDistrict = auth.user.district_id
  const defaultListVillages = data.district_id ? villageByDistrict(data.district_id) : villages
  const [dataVillages, setVillages] = useState(defaultListVillages);
  const [disableDistrict] = useState(currentDistrict ? true : false);

  function handleSubmit(e) {
    e.preventDefault();
    put(route('post-births.update', postBirth.id));
  }

  function destroy() {
    if (confirm('Anda yakin ingin menghapus Pasca Persalinan?')) {
      Inertia.delete(route('post-births.destroy', postBirth.id));
    }
  }

  function selectDistrict(e) {
    setData('district_id', e.target.value)
    let filteredVillage = villageByDistrict(e.target.value);
    setVillages(filteredVillage)
  }

  function selectVillage(e) {
    setData('village_id', e.target.value)
  }

  function villageByDistrict(district_id) {
    return villages.filter(function(x) { return parseInt(district_id) === parseInt(x.district_id) })
  }

  function dateFormat(date) {
    return new Date(date).toISOString().slice(0, 7);
  }

  return (
    <div>
      <h1 className="mb-8 text-3xl font-bold">
        <InertiaLink
          href={route('post-births')}
          className="text-indigo-600 hover:text-indigo-700"
        >
          Pasca Persalinan
        </InertiaLink>
        <span className="font-medium text-indigo-600"> /</span> Edit
      </h1>
      <div className="overflow-hidden bg-white rounded shadow">
        <form onSubmit={handleSubmit}>
          <div className="flex flex-wrap p-8 -mb-8 -mr-6">
            <TextInput
              className="w-full pb-8 pr-6 lg:w-1/2"
              label="Bulan dan Tahun"
              name="date"
              type="month"
              errors={errors.date}
              value={dateFormat(data.date)}
              onChange={e => setData('date', e.target.value)}
            />
            <SelectInput
              className="w-full pb-8 pr-6 lg:w-1/2"
              label="Kecamatan"
              name="district_id"
              errors={errors.district_id}
              value={data.district_id}
              onChange={e => selectDistrict(e)}
              disabled={ disableDistrict }
            >
              <option value=""></option>
              {districts.map(({ id, name }) => (
                <option key={id} value={id}>
                  {name}
                </option>
              ))}
            </SelectInput>
            <SelectInput
              className="w-full pb-8 pr-6 lg:w-1/2"
              label="Desa"
              name="village_id"
              errors={errors.village_id}
              value={data.village_id}
              onChange={e => selectVillage(e)}
            >
              <option value=""></option>
              {dataVillages.map(({ id, name }) => (
                <option key={id} value={id}>
                  {name}
                </option>
              ))}
            </SelectInput>
            <TextInput
              className="w-full pb-8 pr-6 lg:w-1/2"
              label="Pasca Persalinan Suntik"
              name="birth_suntik"
              type="number"
              errors={errors.birth_suntik}
              value={data.birth_suntik}
              onChange={e => setData('birth_suntik', e.target.value)}
            />
            <TextInput
              className="w-full pb-8 pr-6 lg:w-1/2"
              label="Pasca Persalinan Pil"
              name="birth_pil"
              type="number"
              errors={errors.birth_pil}
              value={data.birth_pil}
              onChange={e => setData('birth_pil', e.target.value)}
            />
            <TextInput
              className="w-full pb-8 pr-6 lg:w-1/2"
              label="Pasca Persalinan Kondom"
              name="birth_kondom"
              type="number"
              errors={errors.birth_kondom}
              value={data.birth_kondom}
              onChange={e => setData('birth_kondom', e.target.value)}
            />
            <TextInput
              className="w-full pb-8 pr-6 lg:w-1/2"
              label="Pasca Persalinan IUD"
              name="birth_iud"
              type="number"
              errors={errors.birth_iud}
              value={data.birth_iud}
              onChange={e => setData('birth_iud', e.target.value)}
            />
            <TextInput
              className="w-full pb-8 pr-6 lg:w-1/2"
              label="Pasca Persalinan Implan"
              name="birth_implan"
              type="number"
              errors={errors.birth_implan}
              value={data.birth_implan}
              onChange={e => setData('birth_implan', e.target.value)}
            />
            <TextInput
              className="w-full pb-8 pr-6 lg:w-1/2"
              label="Pasca Persalinan MOP"
              name="birth_mop"
              type="number"
              errors={errors.birth_mop}
              value={data.birth_mop}
              onChange={e => setData('birth_mop', e.target.value)}
            />
            <TextInput
              className="w-full pb-8 pr-6 lg:w-1/2"
              label="Pasca Persalinan MOW"
              name="birth_mow"
              type="number"
              errors={errors.birth_mow}
              value={data.birth_mow}
              onChange={e => setData('birth_mow', e.target.value)}
            />
            <TextInput
              className="w-full pb-8 pr-6 lg:w-1/2"
              label="Pasca Keguguran Suntik"
              name="miscarriage_suntik"
              type="number"
              errors={errors.miscarriage_suntik}
              value={data.miscarriage_suntik}
              onChange={e => setData('miscarriage_suntik', e.target.value)}
            />
            <TextInput
              className="w-full pb-8 pr-6 lg:w-1/2"
              label="Pasca Keguguran Pil"
              name="miscarriage_pil"
              type="number"
              errors={errors.miscarriage_pil}
              value={data.miscarriage_pil}
              onChange={e => setData('miscarriage_pil', e.target.value)}
            />
            <TextInput
              className="w-full pb-8 pr-6 lg:w-1/2"
              label="Pasca Keguguran Kondom"
              name="miscarriage_kondom"
              type="number"
              errors={errors.miscarriage_kondom}
              value={data.miscarriage_kondom}
              onChange={e => setData('miscarriage_kondom', e.target.value)}
            />
            <TextInput
              className="w-full pb-8 pr-6 lg:w-1/2"
              label="Pasca Keguguran IUD"
              name="miscarriage_iud"
              type="number"
              errors={errors.miscarriage_iud}
              value={data.miscarriage_iud}
              onChange={e => setData('miscarriage_iud', e.target.value)}
            />
            <TextInput
              className="w-full pb-8 pr-6 lg:w-1/2"
              label="Pasca Keguguran Implan"
              name="miscarriage_implan"
              type="number"
              errors={errors.miscarriage_implan}
              value={data.miscarriage_implan}
              onChange={e => setData('miscarriage_implan', e.target.value)}
            />
            <TextInput
              className="w-full pb-8 pr-6 lg:w-1/2"
              label="Pasca Keguguran MOP"
              name="miscarriage_mop"
              type="number"
              errors={errors.miscarriage_mop}
              value={data.miscarriage_mop}
              onChange={e => setData('miscarriage_mop', e.target.value)}
            />
            <TextInput
              className="w-full pb-8 pr-6 lg:w-1/2"
              label="Pasca Keguguran MOW"
              name="miscarriage_mow"
              type="number"
              errors={errors.miscarriage_mow}
              value={data.miscarriage_mow}
              onChange={e => setData('miscarriage_mow', e.target.value)}
            />
          </div>
          <div className="flex items-center px-8 py-4 bg-gray-100 border-t border-gray-200">
            <DeleteButton onDelete={destroy}>Hapus</DeleteButton>
            <LoadingButton
              loading={processing}
              type="submit"
              className="ml-auto btn-indigo"
            >
              Update
            </LoadingButton>
          </div>
        </form>
      </div>
    </div>
  );
};

Edit.layout = page => <Layout title="Edit Pasca Persalinan" children={page} />;

export default Edit;
