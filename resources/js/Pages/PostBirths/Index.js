import React, {useRef} from 'react';
import { InertiaLink, usePage } from '@inertiajs/inertia-react';
import Layout from '@/Shared/Layout';
import Icon from '@/Shared/Icon';
import Pagination from '@/Shared/Pagination';
import SearchDateRange from "@/Shared/SearchDateRange";
import pickBy from "lodash/pickBy";
import {useReactToPrint} from "react-to-print";

const Index = () => {
  const { postBirth, filters} = usePage().props;
  const {
    data,
    meta: { links }
  } = postBirth;

  const options = { weekday: 'long', year: 'numeric', month: 'short', day: 'numeric' };

  function dateParser(date) {
    let initDate  = new Date(date);
    return initDate.toLocaleDateString("id-ID", options);
  }

  function dateMonth(date) {
    let initDate  = new Date(date);
    return initDate.toLocaleString("id-ID", { month: 'long' });
  }

  function dateYear(date) {
    let initDate  = new Date(date);
    return initDate.toLocaleString("id-ID", { year: 'numeric' });
  }

  const query = Object.keys(pickBy(filters)).length
    ? pickBy(filters)
    : {};

  const componentRef = useRef();
  const handlePrint = useReactToPrint({
    content: () => componentRef.current,
  });

  return (
    <div>
      <h1 className="text-3xl font-bold mb-1">Pasca Persalinan</h1>

      {(filters.date_start !== null && filters.date_end !== null) && (
        <p className="text-sm">( {dateParser(filters.date_start)} -   {dateParser(filters.date_end)} )</p>
      )}
      <div className="mb-8"/>

      <div className="flex items-center justify-between mb-6">
        <SearchDateRange />
        <InertiaLink
          className="btn-indigo focus:outline-none"
          href={route('post-births.create')}
        >
          <span>Tambah</span>
        </InertiaLink>
      </div>
      {data.length !== 0 && (
        <div className="mb-6">
          <div className="flex -mx-2">
            <div className="px-2">
              <a
                className="inline-block px-6 py-2.5 bg-green-500 text-white font-medium text-xs leading-tight uppercase rounded shadow-md hover:bg-green-600 hover:shadow-lg focus:bg-green-600 focus:shadow-lg focus:outline-none focus:ring-0 active:bg-green-700 active:shadow-lg transition duration-150 ease-in-out"
                href={route('post-births.export', query)}
              >
                <span>Export Excel</span>
              </a>
            </div>
            <div className="w-1/3 px-2">
              <a
                className="inline-block px-6 py-2.5 bg-gray-900 text-white font-medium text-xs leading-tight uppercase rounded shadow-md hover:bg-blue-500 hover:shadow-lg focus:bg-blue-500 focus:shadow-lg focus:outline-none focus:ring-0 active:bg-blue-600 active:shadow-lg transition duration-150 ease-in-out"
                onClick={handlePrint}
              >
                <span>Print</span>
              </a>
            </div>
          </div>
        </div>
      )}
      <div className="overflow-x-auto bg-white rounded shadow">
        <table className="w-full whitespace-nowrap" ref={componentRef}>
          <thead>
          <tr className="font-bold text-left">
            <th className="px-6" rowSpan={2}>Tahun</th>
            <th className="px-6" rowSpan={2}>Bulan</th>
            <th className="px-6" rowSpan={2}>Kecamatan</th>
            <th className="px-6" rowSpan={2}>Desa</th>
            <th className="px-6 pt-5 text-center" colSpan={7}>Pasca Persalinan</th>
            <th className="px-6" rowSpan={2}>Jumlah</th>
            <th className="px-6 pt-5 text-center" colSpan={7}>Pasca Keguguran</th>
            <th className="px-6" rowSpan={2}>Jumlah</th>
          </tr>
          <tr className="font-bold text-left">
            <th className="px-6 pt-5 pb-4">Suntik</th>
            <th className="px-6 pt-5 pb-4">Pil</th>
            <th className="px-6 pt-5 pb-4">Kondom</th>
            <th className="px-6 pt-5 pb-4">IUD</th>
            <th className="px-6 pt-5 pb-4">Implan</th>
            <th className="px-6 pt-5 pb-4">MOP</th>
            <th className="px-6 pt-5 pb-4">MOW</th>
            <th className="px-6 pt-5 pb-4">Suntik</th>
            <th className="px-6 pt-5 pb-4">Pil</th>
            <th className="px-6 pt-5 pb-4">Kondom</th>
            <th className="px-6 pt-5 pb-4">IUD</th>
            <th className="px-6 pt-5 pb-4">Implan</th>
            <th className="px-6 pt-5 pb-4">MOP</th>
            <th className="px-6 pt-5 pb-4">MOW</th>
          </tr>
          </thead>
          <tbody>
          {data.map(({ id, date, district, village, birth_suntik, birth_pil, birth_kondom, birth_iud,
                       birth_implan, birth_mop, birth_mow, miscarriage_suntik, miscarriage_pil, miscarriage_kondom,
                       miscarriage_iud, miscarriage_implan, miscarriage_mop, miscarriage_mow }) => (

            <tr
              key={id}
              className="hover:bg-gray-100 focus-within:bg-gray-100"
            >
              <td className="border-t">
                <InertiaLink
                  href={route('post-births.edit', id)}
                  className="flex items-center px-6 py-4 focus:text-indigo-700 focus:outline-none"
                >
                  {dateMonth(date)}
                </InertiaLink>
              </td>
              <td className="border-t">
                <InertiaLink
                  href={route('post-births.edit', id)}
                  className="flex items-center px-6 py-4 focus:text-indigo-700 focus:outline-none"
                >
                  {dateYear(date)}
                </InertiaLink>
              </td>
              <td className="border-t">
                <InertiaLink
                  tabIndex="-1"
                  href={route('post-births.edit', id)}
                  className="flex items-center px-6 py-4 focus:text-indigo focus:outline-none"
                >
                  {district ? district.name : ''}
                </InertiaLink>
              </td>
              <td className="border-t">
                <InertiaLink
                  tabIndex="-1"
                  href={route('post-births.edit', id)}
                  className="flex items-center px-6 py-4 focus:text-indigo focus:outline-none"
                >
                  {village ? village.name : ''}
                </InertiaLink>
              </td>
              <td className="border-t">
                <InertiaLink
                  tabIndex="-1"
                  href={route('post-births.edit', id)}
                  className="flex items-center px-6 py-4 focus:text-indigo focus:outline-none"
                >
                  {birth_suntik}
                </InertiaLink>
              </td>
              <td className="border-t">
                <InertiaLink
                  tabIndex="-1"
                  href={route('post-births.edit', id)}
                  className="flex items-center px-6 py-4 focus:text-indigo focus:outline-none"
                >
                  {birth_pil}
                </InertiaLink>
              </td>
              <td className="border-t">
                <InertiaLink
                  tabIndex="-1"
                  href={route('post-births.edit', id)}
                  className="flex items-center px-6 py-4 focus:text-indigo focus:outline-none"
                >
                  {birth_kondom}
                </InertiaLink>
              </td>
              <td className="border-t">
                <InertiaLink
                  tabIndex="-1"
                  href={route('post-births.edit', id)}
                  className="flex items-center px-6 py-4 focus:text-indigo focus:outline-none"
                >
                  {birth_iud}
                </InertiaLink>
              </td>
              <td className="border-t">
                <InertiaLink
                  tabIndex="-1"
                  href={route('post-births.edit', id)}
                  className="flex items-center px-6 py-4 focus:text-indigo focus:outline-none"
                >
                  {birth_implan}
                </InertiaLink>
              </td>
              <td className="border-t">
                <InertiaLink
                  tabIndex="-1"
                  href={route('post-births.edit', id)}
                  className="flex items-center px-6 py-4 focus:text-indigo focus:outline-none"
                >
                  {birth_mop}
                </InertiaLink>
              </td>
              <td className="border-t">
                <InertiaLink
                  tabIndex="-1"
                  href={route('post-births.edit', id)}
                  className="flex items-center px-6 py-4 focus:text-indigo focus:outline-none"
                >
                  {birth_mow}
                </InertiaLink>
              </td>
              <td className="border-t">
                <InertiaLink
                  tabIndex="-1"
                  href={route('post-births.edit', id)}
                  className="flex items-center px-6 py-4 focus:text-indigo focus:outline-none"
                >
                  {birth_suntik + birth_pil + birth_kondom + birth_iud + birth_implan + birth_mop + birth_mow}
                </InertiaLink>
              </td>
              <td className="border-t">
                <InertiaLink
                  tabIndex="-1"
                  href={route('post-births.edit', id)}
                  className="flex items-center px-6 py-4 focus:text-indigo focus:outline-none"
                >
                  {miscarriage_suntik}
                </InertiaLink>
              </td>
              <td className="border-t">
                <InertiaLink
                  tabIndex="-1"
                  href={route('post-births.edit', id)}
                  className="flex items-center px-6 py-4 focus:text-indigo focus:outline-none"
                >
                  {miscarriage_pil}
                </InertiaLink>
              </td>
              <td className="border-t">
                <InertiaLink
                  tabIndex="-1"
                  href={route('post-births.edit', id)}
                  className="flex items-center px-6 py-4 focus:text-indigo focus:outline-none"
                >
                  {miscarriage_kondom}
                </InertiaLink>
              </td>
              <td className="border-t">
                <InertiaLink
                  tabIndex="-1"
                  href={route('post-births.edit', id)}
                  className="flex items-center px-6 py-4 focus:text-indigo focus:outline-none"
                >
                  {miscarriage_iud}
                </InertiaLink>
              </td>
              <td className="border-t">
                <InertiaLink
                  tabIndex="-1"
                  href={route('post-births.edit', id)}
                  className="flex items-center px-6 py-4 focus:text-indigo focus:outline-none"
                >
                  {miscarriage_implan}
                </InertiaLink>
              </td>
              <td className="border-t">
                <InertiaLink
                  tabIndex="-1"
                  href={route('post-births.edit', id)}
                  className="flex items-center px-6 py-4 focus:text-indigo focus:outline-none"
                >
                  {miscarriage_mop}
                </InertiaLink>
              </td>
              <td className="border-t">
                <InertiaLink
                  tabIndex="-1"
                  href={route('post-births.edit', id)}
                  className="flex items-center px-6 py-4 focus:text-indigo focus:outline-none"
                >
                  {miscarriage_mow}
                </InertiaLink>
              </td>
              <td className="border-t">
                <InertiaLink
                  tabIndex="-1"
                  href={route('post-births.edit', id)}
                  className="flex items-center px-6 py-4 focus:text-indigo focus:outline-none"
                >
                  {miscarriage_suntik + miscarriage_pil + miscarriage_kondom + miscarriage_iud + miscarriage_implan + miscarriage_mop + miscarriage_mow}
                </InertiaLink>
              </td>
            </tr>
          ))}
          {data.length === 0 && (
            <tr>
              <td className="px-6 py-4 border-t" colSpan="4">
                No Pasca Persalinan found.
              </td>
            </tr>
          )}
          </tbody>
        </table>
      </div>
      <Pagination links={links} />
    </div>
  );
};

Index.layout = page => <Layout title="Pasca Persalinan" children={page} />;

export default Index;
