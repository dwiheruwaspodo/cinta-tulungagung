import React, {useRef} from 'react';
import { InertiaLink, usePage } from '@inertiajs/inertia-react';
import Layout from '@/Shared/Layout';
import Icon from '@/Shared/Icon';
import Pagination from '@/Shared/Pagination';
import SearchDateRange from "@/Shared/SearchDateRange";
import pickBy from "lodash/pickBy";
import {useReactToPrint} from "react-to-print";

const Index = () => {
  const { postBirth, filters} = usePage().props;
  const {
    data,
    meta: { links }
  } = postBirth;

  const options = { weekday: 'long', year: 'numeric', month: 'short', day: 'numeric' };

  function dateParser(date) {
    let initDate  = new Date(date);
    return initDate.toLocaleDateString("id-ID", options);
  }

  function dateMonth(number) {
    let initDate  = new Date();
    initDate.setMonth(number - 1);

    return initDate.toLocaleString("id-ID", { month: 'long' });
  }

  const query = Object.keys(pickBy(filters)).length
    ? pickBy(filters)
    : {};

  const componentRef = useRef();
  const handlePrint = useReactToPrint({
    content: () => componentRef.current,
  });

  return (
    <div>
      <h1 className="text-3xl font-bold mb-1">Pasca Persalinan</h1>

      {(filters.date_start !== null && filters.date_end !== null) && (
        <p className="text-sm">( {dateParser(filters.date_start)} -   {dateParser(filters.date_end)} )</p>
      )}
      <div className="mb-8"/>

      <div className="flex items-center justify-between mb-6">
        <SearchDateRange />
        <InertiaLink
          className="btn-indigo focus:outline-none"
          href={route('post-births.create')}
        >
          <span>Tambah</span>
        </InertiaLink>
      </div>
      {data.length !== 0 && (
        <div className="mb-6">
          <div className="flex -mx-2">
            <div className="px-2">
              <a
                className="inline-block px-6 py-2.5 bg-green-500 text-white font-medium text-xs leading-tight uppercase rounded shadow-md hover:bg-green-600 hover:shadow-lg focus:bg-green-600 focus:shadow-lg focus:outline-none focus:ring-0 active:bg-green-700 active:shadow-lg transition duration-150 ease-in-out"
                href={route('post-births.export', query)}
              >
                <span>Export Excel</span>
              </a>
            </div>
            <div className="w-1/3 px-2">
              <a
                className="inline-block px-6 py-2.5 bg-gray-900 text-white font-medium text-xs leading-tight uppercase rounded shadow-md hover:bg-blue-500 hover:shadow-lg focus:bg-blue-500 focus:shadow-lg focus:outline-none focus:ring-0 active:bg-blue-600 active:shadow-lg transition duration-150 ease-in-out"
                onClick={handlePrint}
              >
                <span>Print</span>
              </a>
            </div>
          </div>
        </div>
      )}
      <div className="overflow-x-auto bg-white rounded shadow">
        <table className="w-full whitespace-nowrap" ref={componentRef}>
          <thead>
          <tr className="font-bold text-left">
            <th className="px-6" rowSpan={2}>Tahun</th>
            <th className="px-6" rowSpan={2}>Bulan</th>
            <th className="px-6" rowSpan={2}>Kecamatan</th>
            <th className="px-6 pt-5 text-center" colSpan={7}>Pasca Persalinan</th>
            <th className="px-6" rowSpan={2}>Jumlah</th>
            <th className="px-6 pt-5 text-center" colSpan={7}>Pasca Keguguran</th>
            <th className="px-6" rowSpan={2}>Jumlah</th>
          </tr>
          <tr className="font-bold text-left">
            <th className="px-6 pt-5 pb-4">Suntik</th>
            <th className="px-6 pt-5 pb-4">Pil</th>
            <th className="px-6 pt-5 pb-4">Kondom</th>
            <th className="px-6 pt-5 pb-4">IUD</th>
            <th className="px-6 pt-5 pb-4">Implan</th>
            <th className="px-6 pt-5 pb-4">MOP</th>
            <th className="px-6 pt-5 pb-4">MOW</th>
            <th className="px-6 pt-5 pb-4">Suntik</th>
            <th className="px-6 pt-5 pb-4">Pil</th>
            <th className="px-6 pt-5 pb-4">Kondom</th>
            <th className="px-6 pt-5 pb-4">IUD</th>
            <th className="px-6 pt-5 pb-4">Implan</th>
            <th className="px-6 pt-5 pb-4">MOP</th>
            <th className="px-6 pt-5 pb-4">MOW</th>
          </tr>
          </thead>
          <tbody>
          {data.map(({ district, sum_birth_suntik, sum_birth_pil, sum_birth_kondom, sum_birth_iud,
                       sum_birth_implan, sum_birth_mop, sum_birth_mow, sum_miscarriage_suntik, sum_miscarriage_pil,
                       sum_miscarriage_kondom, sum_miscarriage_iud, sum_miscarriage_implan,
                       sum_miscarriage_mop, sum_miscarriage_mow, month, year }) => (

            <tr
              key={`${month}-${year}-${district.id}`}
              className="hover:bg-gray-100 focus-within:bg-gray-100"
            >
              <td className="border-t">
                <a
                  className="flex items-center px-6 py-4 focus:text-indigo-700 focus:outline-none"
                >
                  {dateMonth(month)}
                </a>
              </td>
              <td className="border-t">
                <a
                  className="flex items-center px-6 py-4 focus:text-indigo-700 focus:outline-none"
                >
                  {year}
                </a>
              </td>
              <td className="border-t">
                <a
                  className="flex items-center px-6 py-4 focus:text-indigo-700 focus:outline-none"
                >
                  {district ? district.name : ''}
                </a>
              </td>
              <td className="border-t">
                <a
                  className="flex items-center px-6 py-4 focus:text-indigo-700 focus:outline-none"
                >
                  {sum_birth_suntik}
                </a>
              </td>
              <td className="border-t">
                <a
                  className="flex items-center px-6 py-4 focus:text-indigo-700 focus:outline-none"
                >
                  {sum_birth_pil}
                </a>
              </td>
              <td className="border-t">
                <a
                  className="flex items-center px-6 py-4 focus:text-indigo-700 focus:outline-none"
                >
                  {sum_birth_kondom}
                </a>
              </td>
              <td className="border-t">
                <a
                  className="flex items-center px-6 py-4 focus:text-indigo-700 focus:outline-none"
                >
                  {sum_birth_iud}
                </a>
              </td>
              <td className="border-t">
                <a
                  className="flex items-center px-6 py-4 focus:text-indigo-700 focus:outline-none"
                >
                  {sum_birth_implan}
                </a>
              </td>
              <td className="border-t">
                <a
                  className="flex items-center px-6 py-4 focus:text-indigo-700 focus:outline-none"
                >
                  {sum_birth_mop}
                </a>
              </td>
              <td className="border-t">
                <a
                  className="flex items-center px-6 py-4 focus:text-indigo-700 focus:outline-none"
                >
                  {sum_birth_mow}
                </a>
              </td>
              <td className="border-t">
                <a
                  className="flex items-center px-6 py-4 focus:text-indigo-700 focus:outline-none"
                >
                  {parseInt(sum_birth_suntik) + parseInt(sum_birth_pil) + parseInt(sum_birth_kondom) + parseInt(sum_birth_iud) + parseInt(sum_birth_implan) + parseInt(sum_birth_mop) + parseInt(sum_birth_mow)}
                </a>
              </td>
              <td className="border-t">
                <a
                  className="flex items-center px-6 py-4 focus:text-indigo-700 focus:outline-none"
                >
                  {sum_miscarriage_suntik}
                </a>
              </td>
              <td className="border-t">
                <a
                  className="flex items-center px-6 py-4 focus:text-indigo-700 focus:outline-none"
                >
                  {sum_miscarriage_pil}
                </a>
              </td>
              <td className="border-t">
                <a
                  className="flex items-center px-6 py-4 focus:text-indigo-700 focus:outline-none"
                >
                  {sum_miscarriage_kondom}
                </a>
              </td>
              <td className="border-t">
                <a
                  className="flex items-center px-6 py-4 focus:text-indigo-700 focus:outline-none"
                >
                  {sum_miscarriage_iud}
                </a>
              </td>
              <td className="border-t">
                <a
                  className="flex items-center px-6 py-4 focus:text-indigo-700 focus:outline-none"
                >
                  {sum_miscarriage_implan}
                </a>
              </td>
              <td className="border-t">
                <a
                  className="flex items-center px-6 py-4 focus:text-indigo-700 focus:outline-none"
                >
                  {sum_miscarriage_mop}
                </a>
              </td>
              <td className="border-t">
                <a
                  className="flex items-center px-6 py-4 focus:text-indigo-700 focus:outline-none"
                >
                  {sum_miscarriage_mow}
                </a>
              </td>
              <td className="border-t">
                <a
                  className="flex items-center px-6 py-4 focus:text-indigo-700 focus:outline-none"
                >
                  {parseInt(sum_miscarriage_suntik) + parseInt(sum_miscarriage_pil) + parseInt(sum_miscarriage_kondom) + parseInt(sum_miscarriage_iud) + parseInt(sum_miscarriage_implan) + parseInt(sum_miscarriage_mop) + parseInt(sum_miscarriage_mow)}
                </a>
              </td>
            </tr>
          ))}
          {data.length === 0 && (
            <tr>
              <td className="px-6 py-4 border-t" colSpan="4">
                No Pasca Persalinan found.
              </td>
            </tr>
          )}
          </tbody>
        </table>
      </div>
      <Pagination links={links} />
    </div>
  );
};

Index.layout = page => <Layout title="Pasca Persalinan" children={page} />;

export default Index;
