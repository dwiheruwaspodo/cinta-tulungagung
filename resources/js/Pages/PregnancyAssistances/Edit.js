import React, {useState} from 'react';
import { Inertia } from '@inertiajs/inertia';
import { InertiaLink, usePage, useForm } from '@inertiajs/inertia-react';
import Layout from '@/Shared/Layout';
import LoadingButton from '@/Shared/LoadingButton';
import TextInput from '@/Shared/TextInput';
import SelectInput from '@/Shared/SelectInput';
import DeleteButton from "@/Shared/DeleteButton";

const Edit = () => {
  const { pregnancy, villages, districts, auth } = usePage().props;
  const { data, setData, errors, put, processing } = useForm({
    date: pregnancy.date,
    nik: pregnancy.nik,
    name: pregnancy.name,
    birthday: pregnancy.birthday,
    created_by: pregnancy.created_by,
    description: pregnancy.description,
    pregnancy_age: pregnancy.pregnancy_age,
    kek: pregnancy.kek,
    stunting: pregnancy.stunting,
    inhibited_fetal_growth: pregnancy.inhibited_fetal_growth,
    four_too: pregnancy.four_too,
    anemia: pregnancy.anemia,
    village_id: pregnancy.village_id,
    district_id: pregnancy.district_id,
  });

  const currentDistrict = auth.user.district_id
  const defaultListVillages = data.district_id ? villageByDistrict(data.district_id) : villages
  const [dataVillages, setVillages] = useState(defaultListVillages);
  const [disableDistrict] = useState(currentDistrict ? true : false);

  function handleSubmit(e) {
    e.preventDefault();
    put(route('pregnancy-asisstances.update', pregnancy.id));
  }

  function selectDistrict(e) {
    setData('district_id', e.target.value)
    let filteredVillage = villageByDistrict(e.target.value);
    setVillages(filteredVillage)
  }

  function selectVillage(e) {
    setData('village_id', e.target.value)
  }

  function villageByDistrict(district_id) {
    return villages.filter(function(x) { return parseInt(district_id) === parseInt(x.district_id) })
  }

  function destroy() {
    if (confirm('Anda yakin ingin menghapus Pendampingan Bumil ini?')) {
      Inertia.delete(route('pregnancy-asisstances.destroy', pregnancy.id));
    }
  }

  function dateFormat(date) {
    return new Date(date).toISOString().slice(0, 10);
  }

  return (
    <div>
      <h1 className="mb-8 text-3xl font-bold">
        <InertiaLink
          href={route('pregnancy-asisstances')}
          className="text-indigo-600 hover:text-indigo-700"
        >
          Pendampingan Bumil
        </InertiaLink>
        <span className="font-medium text-indigo-600"> /</span> Edit
      </h1>
      <div className="overflow-hidden bg-white rounded shadow">
        <form onSubmit={handleSubmit}>
          <div className="flex flex-wrap p-8 -mb-8 -mr-6">
            <SelectInput
              className="w-full pb-8 pr-6 lg:w-1/2"
              label="Kecamatan"
              name="district_id"
              errors={errors.district_id}
              value={data.district_id}
              onChange={e => selectDistrict(e)}
              disabled={ disableDistrict }
            >
              <option value=""></option>
              {districts.map(({ id, name }) => (
                <option key={id} value={id}>
                  {name}
                </option>
              ))}
            </SelectInput>
            <SelectInput
              className="w-full pb-8 pr-6 lg:w-1/2"
              label="Desa"
              name="village_id"
              errors={errors.village_id}
              value={data.village_id}
              onChange={e => selectVillage(e)}
            >
              <option value=""></option>
              {dataVillages.map(({ id, name }) => (
                <option key={id} value={id}>
                  {name}
                </option>
              ))}
            </SelectInput>
            <TextInput
              className="w-full pb-8 pr-6 lg:w-1/2"
              label="Tanggal Pencatatan"
              name="date"
              type="date"
              errors={errors.date}
              value={dateFormat(data.date)}
              onChange={e => setData('date', e.target.value)}
            />
            <TextInput
              className="w-full pb-8 pr-6 lg:w-1/2"
              label="NIK"
              name="nik"
              type={'number'}
              maxLength={16}
              errors={errors.nik}
              value={data.nik}
              onChange={e => setData('nik', e.target.value)}
            />
            <TextInput
              className="w-full pb-8 pr-6 lg:w-1/2"
              label="Nama"
              name="name"
              errors={errors.name}
              value={data.name}
              onChange={e => setData('name', e.target.value)}
            />
            <TextInput
              className="w-full pb-8 pr-6 lg:w-1/2"
              label="Tanggal Lahir"
              name="birthday"
              type="date"
              errors={errors.birthday}
              value={dateFormat(data.birthday)}
              onChange={e => setData('birthday', e.target.value)}
            />
            <TextInput
              className="w-full pb-8 pr-6 lg:w-1/2"
              label="Usia Kandungan (dalam bulan)"
              name="pregnancy_age"
              type={"number"}
              max={9}
              min={1}
              maxLength={2}
              errors={errors.pregnancy_age}
              value={data.pregnancy_age}
              onChange={e => setData('pregnancy_age', e.target.value)}
            />
            <SelectInput
              className={"w-full pb-8 pr-6 lg:w-1/2"}
              label="Anemia"
              name="anemia"
              errors={errors.anemia}
              value={data.anemia}
              onChange={e => setData('anemia', e.target.value)}
            >
              <option value=""></option>
              <option value="1">Ya</option>
              <option value="0">Tidak</option>
            </SelectInput>
            <SelectInput
              className={"w-full pb-8 pr-6 lg:w-1/2"}
              label={"KEK (Kekurangan Energi Kronis)"}
              name="kek"
              errors={errors.kek}
              value={ data.kek}
              onChange={e => setData('kek', e.target.value)}
            >
              <option value=""></option>
              <option value="1">Ya</option>
              <option value="0">Tidak</option>
            </SelectInput>
            <SelectInput
              className={"w-full pb-8 pr-6 lg:w-1/2"}
              label="Pertumbuhan Janin Terhambat"
              name="inhibited_fetal_growth"
              errors={errors.inhibited_fetal_growth}
              value={ data.inhibited_fetal_growth}
              onChange={e => setData('inhibited_fetal_growth', e.target.value)}
            >
              <option value=""></option>
              <option value="1">Ya</option>
              <option value="0">Tidak</option>
            </SelectInput>
            <SelectInput
              className={"w-full pb-8 pr-6 lg:w-1/2"}
              label="Stunting"
              name="stunting"
              errors={errors.stunting}
              value={ data.stunting}
              onChange={e => setData('stunting', e.target.value)}
            >
              <option value=""></option>
              <option value="1">Ya</option>
              <option value="0">Tidak</option>
            </SelectInput>
            <SelectInput
              className={"w-full pb-8 pr-6 lg:w-1/2"}
              label="4 Terlalu"
              name="four_too"
              errors={errors.four_too}
              value={ data.four_too}
              onChange={e => setData('four_too', e.target.value)}
            >
              <option value=""></option>
              <option value="1">Ya</option>
              <option value="0">Tidak</option>
            </SelectInput>
            <TextInput
              className="w-full pb-8 pr-6 lg:w-1/2"
              label="Keterangan"
              name="description"
              type={"description"}
              errors={errors.description}
              value={data.description}
              onChange={e => setData('description', e.target.value)}
            />
          </div>
          <div className="flex items-center px-8 py-4 bg-gray-100 border-t border-gray-200">
            <DeleteButton onDelete={destroy}>Hapus</DeleteButton>
            <LoadingButton
              loading={processing}
              type="submit"
              className="ml-auto btn-indigo"
            >
              Update
            </LoadingButton>
          </div>
        </form>
      </div>
    </div>
  );
};

Edit.layout = page => <Layout title="Edit Pendampingan Bumil" children={page} />;

export default Edit;
