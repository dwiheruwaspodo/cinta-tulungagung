import React, {useEffect, useState} from 'react';
import { Inertia } from '@inertiajs/inertia';
import { InertiaLink, usePage, useForm } from '@inertiajs/inertia-react';
import Layout from '@/Shared/Layout';
import LoadingButton from '@/Shared/LoadingButton';
import TextInput from '@/Shared/TextInput';
import SelectInput from '@/Shared/SelectInput';

const Create = () => {
  const { districts, villages, auth } = usePage().props;
  const { data, setData, errors, post, processing } = useForm({
    district_id: auth.user.district_id || '',
    village_id: '',
    date: '',
    ssk: '',
    dahsat: '',
    kampung_kb: '',
    rumah_dataku: ''
  });

  const currentDistrict = auth.user.district_id
  const defaultListVillages = currentDistrict ? villageByDistrict(currentDistrict) : villages
  const [dataVillages, setVillages] = useState(defaultListVillages);
  const [disableDistrict] = useState(currentDistrict ? true : false);

  function handleSubmit(e) {
    e.preventDefault();
    post(route('priorities.store'));
  }

  function selectDistrict(e) {
    setData('district_id', e.target.value)
    let filteredVillage = villageByDistrict(e.target.value);
    setVillages(filteredVillage)
  }

  function selectVillage(e) {
    setData('village_id', e.target.value)
  }

  function villageByDistrict(district_id) {
    return villages.filter(function(x) { return parseInt(district_id) === parseInt(x.district_id) })
  }

  return (
    <div>
      <h1 className="mb-8 text-3xl font-bold">
        <InertiaLink
          href={route('priorities')}
          className="text-indigo-600 hover:text-indigo-700"
        >
          Prioritas
        </InertiaLink>
        <span className="font-medium text-indigo-600"> /</span> Tambah
      </h1>
      <div className="overflow-hidden bg-white rounded shadow">
        <form onSubmit={handleSubmit}>
          <div className="flex flex-wrap p-8 -mb-8 -mr-6">
            <TextInput
              className="w-full pb-8 pr-6 lg:w-1/2"
              label="Bulan dan Tahun"
              name="date"
              type="month"
              errors={errors.date}
              value={data.date}
              onChange={e => setData('date', e.target.value)}
            />
            <SelectInput
              className="w-full pb-8 pr-6 lg:w-1/2"
              label="Kecamatan"
              name="district_id"
              errors={errors.district_id}
              value={data.district_id}
              onChange={e => selectDistrict(e)}
              disabled={ disableDistrict }
            >
              <option value=""></option>
              {districts.map(({ id, name }) => (
                <option key={id} value={id}>
                  {name}
                </option>
              ))}
            </SelectInput>
            <SelectInput
              className="w-full pb-8 pr-6 lg:w-1/2"
              label="Desa"
              name="village_id"
              errors={errors.village_id}
              value={data.village_id}
              onChange={e => selectVillage(e)}
            >
              <option value=""></option>
              {dataVillages.map(({ id, name }) => (
                <option key={id} value={id}>
                  {name}
                </option>
              ))}
            </SelectInput>
            <TextInput
              className="w-full pb-8 pr-6 lg:w-1/2"
              label="Jumlah Sekolah Siaga Kependudukan"
              name="ssk"
              type="number"
              errors={errors.ssk}
              value={data.ssk}
              onChange={e => setData('ssk', e.target.value)}
            />
            <TextInput
              className="w-full pb-8 pr-6 lg:w-1/2"
              label="Jumlah DAHSAT"
              name="dahsat"
              type="number"
              errors={errors.dahsat}
              value={data.dahsat}
              onChange={e => setData('dahsat', e.target.value)}
            />
            <TextInput
              className="w-full pb-8 pr-6 lg:w-1/2"
              label="Jumlah Kampung KB"
              name="kampung_kb"
              type="number"
              errors={errors.kampung_kb}
              value={data.kampung_kb}
              onChange={e => setData('kampung_kb', e.target.value)}
            />
            <TextInput
              className="w-full pb-8 pr-6 lg:w-1/2"
              label="Jumlah Rumah Dataku"
              name="rumah_dataku"
              type="number"
              errors={errors.rumah_dataku}
              value={data.rumah_dataku}
              onChange={e => setData('rumah_dataku', e.target.value)}
            />
          </div>
          <div className="flex items-center justify-end px-8 py-4 bg-gray-100 border-t border-gray-200">
            <LoadingButton
              loading={processing}
              type="submit"
              className="btn-indigo"
            >
              Tambah
            </LoadingButton>
          </div>
        </form>
      </div>
    </div>
  );
};

Create.layout = page => <Layout title="Tambah Prioritas" children={page} />;

export default Create;
