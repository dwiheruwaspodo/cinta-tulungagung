import React, {useEffect, useState} from 'react';
import { Inertia } from '@inertiajs/inertia';
import { InertiaLink, usePage, useForm } from '@inertiajs/inertia-react';
import Layout from '@/Shared/Layout';
import LoadingButton from '@/Shared/LoadingButton';
import TextInput from '@/Shared/TextInput';
import SelectInput from '@/Shared/SelectInput';
import DeleteButton from "@/Shared/DeleteButton";

const Edit = () => {
  const { priority, districts, villages, auth } = usePage().props;
  const { data, setData, errors, put, processing } = useForm({
    district_id: priority.district_id || '',
    village_id: priority.village_id || '',
    date: priority.date || '',
    ssk: priority.ssk,
    dahsat: priority.dahsat,
    kampung_kb: priority.kampung_kb,
    rumah_dataku: priority.rumah_dataku
  });

  const currentDistrict = auth.user.district_id
  const defaultListVillages = data.district_id ? villageByDistrict(data.district_id) : villages
  const [dataVillages, setVillages] = useState(defaultListVillages);
  const [disableDistrict] = useState(currentDistrict ? true : false);

  function handleSubmit(e) {
    e.preventDefault();
    put(route('priorities.update', priority.id));
  }

  function destroy() {
    if (confirm('Anda yakin ingin menghapus Prioritas?')) {
      Inertia.delete(route('priorities.destroy', priority.id));
    }
  }

  function selectDistrict(e) {
    setData('district_id', e.target.value)
    let filteredVillage = villageByDistrict(e.target.value);
    setVillages(filteredVillage)
  }

  function selectVillage(e) {
    setData('village_id', e.target.value)
  }

  function villageByDistrict(district_id) {
    return villages.filter(function(x) { return parseInt(district_id) === parseInt(x.district_id) })
  }

  function dateFormat(date) {
    return new Date(date).toISOString().slice(0, 7);
  }

  return (
    <div>
      <h1 className="mb-8 text-3xl font-bold">
        <InertiaLink
          href={route('priorities')}
          className="text-indigo-600 hover:text-indigo-700"
        >
          Prioritas
        </InertiaLink>
        <span className="font-medium text-indigo-600"> /</span> Edit
      </h1>
      <div className="overflow-hidden bg-white rounded shadow">
        <form onSubmit={handleSubmit}>
          <div className="flex flex-wrap p-8 -mb-8 -mr-6">
            <TextInput
              className="w-full pb-8 pr-6 lg:w-1/2"x
              label="Bulan dan Tahun"
              name="date"
              type="month"
              errors={errors.date}
              value={dateFormat(data.date)}
              onChange={e => setData('date', e.target.value)}
            />
            <SelectInput
              className="w-full pb-8 pr-6 lg:w-1/2"
              label="Kecamatan"
              name="district_id"
              errors={errors.district_id}
              value={data.district_id}
              onChange={e => selectDistrict(e)}
              disabled={ disableDistrict }
            >
              <option value=""></option>
              {districts.map(({ id, name }) => (
                <option key={id} value={id}>
                  {name}
                </option>
              ))}
            </SelectInput>
            <SelectInput
              className="w-full pb-8 pr-6 lg:w-1/2"
              label="Desa"
              name="village_id"
              errors={errors.village_id}
              value={data.village_id}
              onChange={e => selectVillage(e)}
            >
              <option value=""></option>
              {dataVillages.map(({ id, name }) => (
                <option key={id} value={id}>
                  {name}
                </option>
              ))}
            </SelectInput>
            <TextInput
              className="w-full pb-8 pr-6 lg:w-1/2"
              label="Jumlah Sekolah Siaga Kependudukan"
              name="ssk"
              type="number"
              errors={errors.ssk}
              value={data.ssk}
              onChange={e => setData('ssk', e.target.value)}
            />
            <TextInput
              className="w-full pb-8 pr-6 lg:w-1/2"
              label="Jumlah DAHSAT"
              name="dahsat"
              type="number"
              errors={errors.dahsat}
              value={data.dahsat}
              onChange={e => setData('dahsat', e.target.value)}
            />
            <TextInput
              className="w-full pb-8 pr-6 lg:w-1/2"
              label="Jumlah Kampung KB"
              name="kampung_kb"
              type="number"
              errors={errors.kampung_kb}
              value={data.kampung_kb}
              onChange={e => setData('kampung_kb', e.target.value)}
            />
            <TextInput
              className="w-full pb-8 pr-6 lg:w-1/2"
              label="Jumlah Rumah Dataku"
              name="rumah_dataku"
              type="number"
              errors={errors.rumah_dataku}
              value={data.rumah_dataku}
              onChange={e => setData('rumah_dataku', e.target.value)}
            />
          </div>
          <div className="flex items-center px-8 py-4 bg-gray-100 border-t border-gray-200">
            <DeleteButton onDelete={destroy}>Hapus</DeleteButton>
            <LoadingButton
              loading={processing}
              type="submit"
              className="ml-auto btn-indigo"
            >
              Update
            </LoadingButton>
          </div>
        </form>
      </div>
    </div>
  );
};

Edit.layout = page => <Layout title="Edit Prioritas" children={page} />;

export default Edit;
