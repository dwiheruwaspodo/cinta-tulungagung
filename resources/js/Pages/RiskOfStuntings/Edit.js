import React, {useState} from 'react';
import { Inertia } from '@inertiajs/inertia';
import { InertiaLink, usePage, useForm } from '@inertiajs/inertia-react';
import Layout from '@/Shared/Layout';
import LoadingButton from '@/Shared/LoadingButton';
import TextInput from '@/Shared/TextInput';
import SelectInput from '@/Shared/SelectInput';
import DeleteButton from "@/Shared/DeleteButton";

const Edit = () => {
  const { risk, opt_source_water, opt_toilet, districts, villages, auth } = usePage().props;
  const { data, setData, errors, put, processing } = useForm({
    rw: risk.rw,
    rt: risk.rt,
    head_of_family: risk.head_of_family,
    baduta: risk.baduta,
    balita: risk.balita,
    pus: risk.pus,
    pus_hamil: risk.pus_hamil,
    source_of_water: risk.source_of_water,
    toilet: risk.toilet,
    too_young: risk.too_young,
    too_old: risk.too_old,
    district_id: risk.district_id,
    village_id: risk.village_id,
    too_near: risk.too_near,
    too_tight: risk.too_tight,
    risk: risk.risk
  });

  const currentDistrict = auth.user.district_id
  const defaultListVillages = data.district_id ? villageByDistrict(data.district_id) : villages
  const [dataVillages, setVillages] = useState(defaultListVillages);
  const [disableDistrict] = useState(currentDistrict ? true : false);

  function handleSubmit(e) {
    e.preventDefault();
    put(route('risk-of-stunting.update', risk.id));
  }

  function destroy() {
    if (confirm('Anda yakin ingin menghapus Keluarga Resiko Stunting ini?')) {
      Inertia.delete(route('risk-of-stunting.destroy', risk.id));
    }
  }

  function selectDistrict(e) {
    setData('district_id', e.target.value)
    let filteredVillage = villageByDistrict(e.target.value);
    setVillages(filteredVillage)
  }

  function selectVillage(e) {
    setData('village_id', e.target.value)
  }

  function dateFormat(date) {
    return new Date(date).toISOString().slice(0, 10);
  }

  function villageByDistrict(district_id) {
    return villages.filter(function(x) { return parseInt(district_id) === parseInt(x.district_id) })
  }

  return (
    <div>
      <h1 className="mb-8 text-3xl font-bold">
        <InertiaLink
          href={route('risk-of-stunting')}
          className="text-indigo-600 hover:text-indigo-700"
        >
          Keluarga Resiko Stunting
        </InertiaLink>
        <span className="font-medium text-indigo-600"> /</span> Edit
      </h1>
      <div className="overflow-hidden bg-white rounded shadow">
        <form onSubmit={handleSubmit}>
          <div className="flex flex-wrap p-8 -mb-8 -mr-6">
            <SelectInput
              className="w-full pb-8 pr-6 lg:w-1/2"
              label="Kecamatan"
              name="district_id"
              errors={errors.district_id}
              value={data.district_id}
              onChange={e => selectDistrict(e)}
              disabled={ disableDistrict }
            >
              <option value=""></option>
              {districts.map(({ id, name }) => (
                <option key={id} value={id}>
                  {name}
                </option>
              ))}
            </SelectInput>
            <SelectInput
              className="w-full pb-8 pr-6 lg:w-1/2"
              label="Desa"
              name="village_id"
              errors={errors.village_id}
              value={data.village_id}
              onChange={e => selectVillage(e)}
            >
              <option value=""></option>
              {dataVillages.map(({ id, name }) => (
                <option key={id} value={id}>
                  {name}
                </option>
              ))}
            </SelectInput>
            <TextInput
              className="w-full pb-8 pr-6 lg:w-1/2"
              label="RW"
              name="rw"
              type="number"
              maxLength={3}
              errors={errors.rw}
              value={data.rw}
              onChange={e => setData('rw', e.target.value)}
            />
            <TextInput
              className="w-full pb-8 pr-6 lg:w-1/2"
              label="RT"
              name="rt"
              type="number"
              maxLength={3}
              errors={errors.rt}
              value={data.rt}
              onChange={e => setData('rt', e.target.value)}
            />
            <TextInput
              className="w-full pb-8 pr-6 lg:w-1/2"
              label="Kepala Keluarga"
              name="head_of_family"
              type="text"
              errors={errors.head_of_family}
              value={data.head_of_family}
              onChange={e => setData('head_of_family', e.target.value)}
            />
            <SelectInput
              className="w-full pb-8 pr-6 lg:w-1/2"
              label="PUS"
              name="pus"
              errors={errors.pus}
              value={data.pus}
              onChange={e => setData('pus', e.target.value)}
            >
              <option value=""></option>
              <option value="1">Ya</option>
              <option value="0">Tidak</option>
            </SelectInput>
            <SelectInput
              className="w-full pb-8 pr-6 lg:w-1/2"
              label="PUS Hamil"
              name="pus_hamil"
              errors={errors.pus_hamil}
              value={data.pus_hamil}
              onChange={e => setData('pus_hamil', e.target.value)}
            >
              <option value=""></option>
              <option value="1">Ya</option>
              <option value="0">Tidak</option>
            </SelectInput>
            <SelectInput
              className="w-full pb-8 pr-6 lg:w-1/2"
              label="Sasaran Baduta"
              name="baduta"
              errors={errors.baduta}
              value={data.baduta}
              onChange={e => setData('baduta', e.target.value)}
            >
              <option value=""></option>
              <option value="1">Ya</option>
              <option value="0">Tidak</option>
            </SelectInput>
            <SelectInput
              className="w-full pb-8 pr-6 lg:w-1/2"
              label="Sasaran Balita"
              name="balita"
              errors={errors.balita}
              value={data.balita}
              onChange={e => setData('balita', e.target.value)}
            >
              <option value=""></option>
              <option value="1">Ya</option>
              <option value="0">Tidak</option>
            </SelectInput>
            <SelectInput
              className="w-full pb-8 pr-6 lg:w-1/2"
              label="Sumber Air"
              name="source_of_water"
              errors={errors.source_of_water}
              value={data.source_of_water}
              onChange={e => setData('source_of_water', e.target.value)}
            >
              <option value=""></option>
              {opt_source_water.map((value, index) => (
                <option key={index} value={value}>
                  {value}
                </option>
              ))}
            </SelectInput>
            <SelectInput
              className="w-full pb-8 pr-6 lg:w-1/2"
              label="Jamban"
              name="toilet"
              errors={errors.toilet}
              value={data.toilet}
              onChange={e => setData('toilet', e.target.value)}
            >
              <option value=""></option>
              {opt_toilet.map((value, index) => (
                <option key={index} value={value}>
                  {value}
                </option>
              ))}
            </SelectInput>
            <SelectInput
              className="w-full pb-8 pr-6 lg:w-1/2"
              label="Terlalu Muda"
              name="too_young"
              errors={errors.too_young}
              value={data.too_young}
              onChange={e => setData('too_young', e.target.value)}
            >
              <option value=""></option>
              <option value="1">Ya</option>
              <option value="0">Tidak</option>
            </SelectInput>
            <SelectInput
              className="w-full pb-8 pr-6 lg:w-1/2"
              label="Terlalu Tua"
              name="too_old"
              errors={errors.too_old}
              value={data.too_old}
              onChange={e => setData('too_old', e.target.value)}
            >
              <option value=""></option>
              <option value="1">Ya</option>
              <option value="0">Tidak</option>
            </SelectInput>
            <SelectInput
              className="w-full pb-8 pr-6 lg:w-1/2"
              label="Terlalu Dekat"
              name="too_near"
              errors={errors.too_near}
              value={data.too_near}
              onChange={e => setData('too_near', e.target.value)}
            >
              <option value=""></option>
              <option value="1">Ya</option>
              <option value="0">Tidak</option>
            </SelectInput>
            <SelectInput
              className="w-full pb-8 pr-6 lg:w-1/2"
              label="Terlalu rapat"
              name="too_tight"
              errors={errors.too_tight}
              value={data.too_tight.toString()}
              onChange={e => setData('too_tight', e.target.value)}
            >
              <option value=""></option>
              <option value="1">Ya</option>
              <option value="0">Tidak</option>
            </SelectInput>
            <SelectInput
              className="w-full pb-8 pr-6 lg:w-1/2"
              label="Beresiko Stunting"
              name="risk"
              errors={errors.risk}
              value={data.risk.toString()}
              onChange={e => setData('risk', e.target.value)}
            >
              <option value=""></option>
              <option value="1">Ya</option>
              <option value="0">Tidak</option>
            </SelectInput>
          </div>
          <div className="flex items-center px-8 py-4 bg-gray-100 border-t border-gray-200">
            <DeleteButton onDelete={destroy}>Hapus</DeleteButton>
            <LoadingButton
              loading={processing}
              type="submit"
              className="ml-auto btn-indigo"
            >
              Update
            </LoadingButton>
          </div>
        </form>
      </div>
    </div>
  );
};

Edit.layout = page => <Layout title="Edit Keluarga Resiko Stunting" children={page} />;

export default Edit;
