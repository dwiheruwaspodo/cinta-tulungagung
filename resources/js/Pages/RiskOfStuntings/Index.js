import React, {useRef} from 'react';
import { InertiaLink, usePage } from '@inertiajs/inertia-react';
import Layout from '@/Shared/Layout';
import Icon from '@/Shared/Icon';
import Pagination from '@/Shared/Pagination';
import Search from "@/Shared/Search";
import pickBy from "lodash/pickBy";
import {useReactToPrint} from "react-to-print";

const Index = () => {
  const { risks, filters } = usePage().props;
  const {
    data,
    meta: { links }
  } = risks;

  const query = Object.keys(pickBy(filters)).length
    ? pickBy(filters)
    : {};

  const componentRef = useRef();
  const handlePrint = useReactToPrint({
    content: () => componentRef.current,
  });

  return (
    <div>
      <h1 className="mb-8 text-3xl font-bold">Keluarga Resiko Stunting</h1>
      <div className="flex items-center justify-between mb-6">
        <Search />
        <InertiaLink
          className="btn-indigo focus:outline-none"
          href={route('risk-of-stunting.create')}
        >
          <span>Tambah</span>
        </InertiaLink>
      </div>
      {data.length !== 0 && (
        <div className="mb-6">
          <div className="flex -mx-2">
            <div className="px-2">
              <a
                className="inline-block px-6 py-2.5 bg-green-500 text-white font-medium text-xs leading-tight uppercase rounded shadow-md hover:bg-green-600 hover:shadow-lg focus:bg-green-600 focus:shadow-lg focus:outline-none focus:ring-0 active:bg-green-700 active:shadow-lg transition duration-150 ease-in-out"
                href={route('risk-of-stunting.export', query)}
              >
                <span>Export Excel</span>
              </a>
            </div>
            <div className="w-1/3 px-2">
              <a
                className="inline-block px-6 py-2.5 bg-gray-900 text-white font-medium text-xs leading-tight uppercase rounded shadow-md hover:bg-blue-500 hover:shadow-lg focus:bg-blue-500 focus:shadow-lg focus:outline-none focus:ring-0 active:bg-blue-600 active:shadow-lg transition duration-150 ease-in-out"
                onClick={handlePrint}
              >
                <span>Print</span>
              </a>
            </div>
          </div>
        </div>
      )}
      <div className="overflow-x-auto bg-white rounded shadow">
        <table className="w-full whitespace-nowrap" ref={componentRef}>
          <thead>
          <tr className="font-bold text-left">
            <th className="px-6 pt-5 pb-4">Desa</th>
            <th className="px-6 pt-5 pb-4">RW</th>
            <th className="px-6 pt-5 pb-4">RT</th>
            <th className="px-6 pt-5 pb-4">Kepala Keluarga</th>
            <th className="px-6 pt-5 pb-4">Baduta</th>
            <th className="px-6 pt-5 pb-4">Balita</th>
            <th className="px-6 pt-5 pb-4">PUS</th>
            <th className="px-6 pt-5 pb-4">PUS Hamil</th>
            <th className="px-6 pt-5 pb-4">Sumber Air</th>
            <th className="px-6 pt-5 pb-4">Jamban</th>
            <th className="px-6 pt-5 pb-4">Terlalu Muda</th>
            <th className="px-6 pt-5 pb-4">Terlalu Tua</th>
            <th className="px-6 pt-5 pb-4">Terlalu Dekat</th>
            <th className="px-6 pt-5 pb-4">Terlalu Rapat</th>
            <th className="px-6 pt-5 pb-4" colSpan="2">
              Resiko Stunting
            </th>
          </tr>
          </thead>
          <tbody>
          {data.map(({ id, rw, rt, head_of_family, baduta, balita,
                       pus, pus_hamil, source_of_water, toilet, too_young, too_old,
                       village, too_near, too_tight, risk }) => (

            <tr
              key={id}
              className="hover:bg-gray-100 focus-within:bg-gray-100"
            >
              <td className="border-t">
                <InertiaLink
                  href={route('risk-of-stunting.edit', id)}
                  className="flex items-center px-6 py-4 focus:text-indigo-700 focus:outline-none"
                >
                  {village.name}
                </InertiaLink>
              </td>
              <td className="border-t">
                <InertiaLink
                  tabIndex="1"
                  className="flex items-center px-6 py-4 focus:text-indigo focus:outline-none"
                  href={route('risk-of-stunting.edit', id)}
                >
                  {rw}
                </InertiaLink>
              </td>
              <td className="border-t">
                <InertiaLink
                  tabIndex="-1"
                  href={route('risk-of-stunting.edit', id)}
                  className="flex items-center px-6 py-4 focus:text-indigo focus:outline-none"
                >
                  {rt}
                </InertiaLink>
              </td>
              <td className="border-t">
                <InertiaLink
                  tabIndex="-1"
                  href={route('risk-of-stunting.edit', id)}
                  className="flex items-center px-6 py-4 focus:text-indigo focus:outline-none"
                >
                  {head_of_family}
                </InertiaLink>
              </td>
              <td className="border-t">
                <InertiaLink
                  tabIndex="-1"
                  href={route('risk-of-stunting.edit', id)}
                  className="flex items-center px-6 py-4 focus:text-indigo focus:outline-none"
                >
                  {baduta == 1 ? 'Ya' : 'Tidak'}
                </InertiaLink>
              </td>
              <td className="border-t">
                <InertiaLink
                  tabIndex="-1"
                  href={route('risk-of-stunting.edit', id)}
                  className="flex items-center px-6 py-4 focus:text-indigo focus:outline-none"
                >
                  {balita == 1 ? 'Ya' : 'Tidak'}
                </InertiaLink>
              </td>
              <td className="border-t">
                <InertiaLink
                  tabIndex="-1"
                  href={route('risk-of-stunting.edit', id)}
                  className="flex items-center px-6 py-4 focus:text-indigo focus:outline-none"
                >
                  {pus == 1 ? 'Ya' : 'Tidak'}
                </InertiaLink>
              </td>
              <td className="border-t">
                <InertiaLink
                  tabIndex="-1"
                  href={route('risk-of-stunting.edit', id)}
                  className="flex items-center px-6 py-4 focus:text-indigo focus:outline-none"
                >
                  {pus_hamil == 1 ? 'Ya' : 'Tidak'}
                </InertiaLink>
              </td>
              <td className="border-t">
                <InertiaLink
                  tabIndex="-1"
                  href={route('risk-of-stunting.edit', id)}
                  className="flex items-center px-6 py-4 focus:text-indigo focus:outline-none"
                >
                  {source_of_water}
                </InertiaLink>
              </td>
              <td className="border-t">
                <InertiaLink
                  tabIndex="-1"
                  href={route('risk-of-stunting.edit', id)}
                  className="flex items-center px-6 py-4 focus:text-indigo focus:outline-none"
                >
                  {toilet}
                </InertiaLink>
              </td>
              <td className="border-t">
                <InertiaLink
                  tabIndex="-1"
                  href={route('risk-of-stunting.edit', id)}
                  className="flex items-center px-6 py-4 focus:text-indigo focus:outline-none"
                >
                  {too_young == 1 ? 'Ya' : 'Tidak'}
                </InertiaLink>
              </td>
              <td className="border-t">
                <InertiaLink
                  tabIndex="-1"
                  href={route('risk-of-stunting.edit', id)}
                  className="flex items-center px-6 py-4 focus:text-indigo focus:outline-none"
                >
                  {too_old == 1 ? 'Ya' : 'Tidak'}
                </InertiaLink>
              </td>
              <td className="border-t">
                <InertiaLink
                  tabIndex="-1"
                  href={route('risk-of-stunting.edit', id)}
                  className="flex items-center px-6 py-4 focus:text-indigo focus:outline-none"
                >
                  {too_near == 1 ? 'Ya' : 'Tidak'}
                </InertiaLink>
              </td>
              <td className="border-t">
                <InertiaLink
                  tabIndex="-1"
                  href={route('risk-of-stunting.edit', id)}
                  className="flex items-center px-6 py-4 focus:text-indigo focus:outline-none"
                >
                  {too_tight == 1 ? 'Ya' : 'Tidak'}
                </InertiaLink>
              </td>
              <td className="border-t">
                <InertiaLink
                  tabIndex="-1"
                  href={route('risk-of-stunting.edit', id)}
                  className="flex items-center px-6 py-4 focus:text-indigo focus:outline-none"
                >
                  {risk == 1 ? 'Ya' : 'Tidak'}
                </InertiaLink>
              </td>
              <td className="w-px border-t">
                <InertiaLink
                  tabIndex="-1"
                  href={route('risk-of-stunting.edit', id)}
                  className="flex items-center px-4 focus:outline-none"
                >
                  <Icon
                    name="cheveron-right"
                    className="block w-6 h-6 text-gray-400 fill-current"
                  />
                </InertiaLink>
              </td>
            </tr>
          ))}
          {data.length === 0 && (
            <tr>
              <td className="px-6 py-4 border-t" colSpan="4">
                No Keluarga Resiko Stunting found.
              </td>
            </tr>
          )}
          </tbody>
        </table>
      </div>
      <Pagination links={links} />
    </div>
  );
};

Index.layout = page => <Layout title="Keluarga Resiko Stunting" children={page} />;

export default Index;
