import React, {useRef} from 'react';
import { InertiaLink, usePage } from '@inertiajs/inertia-react';
import Layout from '@/Shared/Layout';
import Icon from '@/Shared/Icon';
import Pagination from '@/Shared/Pagination';
import SearchDateRange from "@/Shared/SearchDateRange";
import pickBy from "lodash/pickBy";
import {useReactToPrint} from "react-to-print";

const Index = () => {
  const { unMeet, filters} = usePage().props;
  const {
    data,
    meta: { links }
  } = unMeet;

  const options = { weekday: 'long', year: 'numeric', month: 'short', day: 'numeric' };

  function dateParser(date) {
    let initDate  = new Date(date);
    return initDate.toLocaleDateString("id-ID", options);
  }

  function dateMonth(number) {
    let initDate  = new Date();
    initDate.setMonth(number - 1);

    return initDate.toLocaleString("id-ID", { month: 'long' });
  }

  function dateYear(date) {
    let initDate  = new Date(date);
    return initDate.toLocaleString("id-ID", { year: 'numeric' });
  }

  const query = Object.keys(pickBy(filters)).length
    ? pickBy(filters)
    : {};

  const componentRef = useRef();
  const handlePrint = useReactToPrint({
    content: () => componentRef.current,
  });

  return (
    <div>
      <h1 className="text-3xl font-bold mb-1">Un Meet Need</h1>

      {(filters.date_start !== null && filters.date_end !== null) && (
        <p className="text-sm">( {dateParser(filters.date_start)} -   {dateParser(filters.date_end)} )</p>
      )}
      <div className="mb-8"/>

      <div className="flex items-center justify-between mb-6">
        <SearchDateRange />
        <InertiaLink
          className="btn-indigo focus:outline-none"
          href={route('unmeet-needs.create')}
        >
          <span>Tambah</span>
        </InertiaLink>
      </div>
      {data.length !== 0 && (
        <div className="mb-6">
          <div className="flex -mx-2">
            <div className="px-2">
              <a
                className="inline-block px-6 py-2.5 bg-green-500 text-white font-medium text-xs leading-tight uppercase rounded shadow-md hover:bg-green-600 hover:shadow-lg focus:bg-green-600 focus:shadow-lg focus:outline-none focus:ring-0 active:bg-green-700 active:shadow-lg transition duration-150 ease-in-out"
                href={route('unmeet-needs.export', query)}
              >
                <span>Export Excel</span>
              </a>
            </div>
            <div className="w-1/3 px-2">
              <a
                className="inline-block px-6 py-2.5 bg-gray-900 text-white font-medium text-xs leading-tight uppercase rounded shadow-md hover:bg-blue-500 hover:shadow-lg focus:bg-blue-500 focus:shadow-lg focus:outline-none focus:ring-0 active:bg-blue-600 active:shadow-lg transition duration-150 ease-in-out"
                onClick={handlePrint}
              >
                <span>Print</span>
              </a>
            </div>
          </div>
        </div>
      )}
      <div className="overflow-x-auto bg-white rounded shadow">
        <table className="w-full whitespace-nowrap" ref={componentRef}>
          <thead>
          <tr className="font-bold text-left">
            <th className="px-6 pt-5 pb-4">Tahun</th>
            <th className="px-6 pt-5 pb-4">Bulan</th>
            <th className="px-6 pt-5 pb-4">Kecamatan</th>
            <th className="px-6 pt-5 pb-4">IAT</th>
            <th className="px-6 pt-5 pb-4">TIAL</th>
            <th className="px-6 pt-5 pb-4">Jumlah ( IAT + TIAL )</th>
            <th className="px-6 pt-5 pb-4">IAS</th>
            <th className="px-6 pt-5 pb-4">Hamil</th>
            <th className="px-6 pt-5 pb-4">Jumlah ( IAS + Hamil )</th>
          </tr>
          </thead>
          <tbody>
          {data.map(({ month, year, district, sum_iat, sum_tial, sum_ias, sum_hamil }) => (
            <tr
              key={`${month}${year}${district.id}`}
              className="hover:bg-gray-100 focus-within:bg-gray-100"
            >
              <td className="border-t">
                <a
                  className="flex items-center px-6 py-4 focus:text-indigo-700 focus:outline-none"
                >
                  {dateMonth(month)}
                </a>
              </td>
              <td className="border-t">
                <a
                  className="flex items-center px-6 py-4 focus:text-indigo-700 focus:outline-none"
                >
                  {year}
                </a>
              </td>
              <td className="border-t">
                <a
                  tabIndex="-1"
                  className="flex items-center px-6 py-4 focus:text-indigo focus:outline-none"
                >
                  {district ? district.name : ''}
                </a>
              </td>
              <td className="border-t">
                <a
                  tabIndex="-1"
                  className="flex items-center px-6 py-4 focus:text-indigo focus:outline-none"
                >
                  {parseInt(sum_iat)}
                </a>
              </td>
              <td className="border-t">
                <a
                  tabIndex="-1"
                  className="flex items-center px-6 py-4 focus:text-indigo focus:outline-none"
                >
                  {parseInt(sum_tial)}
                </a>
              </td>
              <td className="border-t">
                <a
                  tabIndex="-1"
                  className="flex items-center px-6 py-4 focus:text-indigo focus:outline-none"
                >
                  {parseInt(sum_iat) + parseInt(sum_tial)}
                </a>
              </td>
              <td className="border-t">
                <a
                  tabIndex="-1"
                  className="flex items-center px-6 py-4 focus:text-indigo focus:outline-none"
                >
                  {parseInt(sum_ias)}
                </a>
              </td>
              <td className="border-t">
                <a
                  tabIndex="-1"
                  className="flex items-center px-6 py-4 focus:text-indigo focus:outline-none"
                >
                  {parseInt(sum_hamil)}
                </a>
              </td>
              <td className="border-t">
                <a
                  tabIndex="-1"
                  className="flex items-center px-6 py-4 focus:text-indigo focus:outline-none"
                >
                  {parseInt(sum_ias) + parseInt(sum_hamil)}
                </a>
              </td>
            </tr>
          ))}
          {data.length === 0 && (
            <tr>
              <td className="px-6 py-4 border-t" colSpan="4">
                No Un Meet Needs found.
              </td>
            </tr>
          )}
          </tbody>
        </table>
      </div>
      <Pagination links={links} />
    </div>
  );
};

Index.layout = page => <Layout title="Un Meet Needs" children={page} />;

export default Index;
