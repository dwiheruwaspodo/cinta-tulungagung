import React, {useState} from 'react';
import { Inertia } from '@inertiajs/inertia';
import {InertiaLink, useForm, usePage} from '@inertiajs/inertia-react';
import Layout from '@/Shared/Layout';
import LoadingButton from '@/Shared/LoadingButton';
import TextInput from '@/Shared/TextInput';
import SelectInput from '@/Shared/SelectInput';
import FileInput from '@/Shared/FileInput';
import PasswordInput from "@/Shared/PasswordInput";

const Create = () => {
  const { roles, districts } = usePage().props;
  const { data, setData, errors, post, processing } = useForm({
    first_name: '',
    last_name: '',
    email: '',
    password: '',
    role_id: '',
    district_id: '',
    photo: ''
  });

  const [dataDistrict, setDataDistrict] = useState([]);
  const [showDistrict, setShowDistrict] = useState(false);

  function handleSubmit(e) {
    e.preventDefault();
    post(route('users.store'));
  }

  const roleIdShowedDistricts = [3, 5, 7]

  function selectRole(e) {
    if (roleIdShowedDistricts.indexOf(parseInt(e.target.value)) != -1) {
      setDataDistrict(districts)
      setShowDistrict(true)
    } else {
      setData('district_id', '')
      setShowDistrict(false)
    }
    setData('role_id', e.target.value)
  }

  return (
    <div>
      <div>
        <h1 className="mb-8 text-3xl font-bold">
          <InertiaLink
            href={route('users')}
            className="text-indigo-600 hover:text-indigo-700"
          >
            Users
          </InertiaLink>
          <span className="font-medium text-indigo-600"> /</span> Tambah
        </h1>
      </div>
      <div className="overflow-hidden bg-white rounded shadow">
        <form name="createForm" onSubmit={handleSubmit}>
          <div className="flex flex-wrap p-8 -mb-8 -mr-6">
            <TextInput
              className="w-full pb-8 pr-6 lg:w-1/2"
              label="First Name"
              name="first_name"
              errors={errors.first_name}
              value={data.first_name}
              onChange={e => setData('first_name', e.target.value)}
            />
            <TextInput
              className="w-full pb-8 pr-6 lg:w-1/2"
              label="Last Name"
              name="last_name"
              errors={errors.last_name}
              value={data.last_name}
              onChange={e => setData('last_name', e.target.value)}
            />
            <TextInput
              className="w-full pb-8 pr-6 lg:w-1/2"
              label="Email"
              name="email"
              type="email"
              errors={errors.email}
              value={data.email}
              onChange={e => setData('email', e.target.value)}
            />
            <PasswordInput
              className="w-full pb-8 pr-6 lg:w-1/2"
              label="Password"
              name="password"
              errors={errors.password}
              value={data.password}
              onChange={e => setData('password', e.target.value)}
            />
            <SelectInput
              className="w-full pb-8 pr-6 lg:w-1/2"
              label="Role"
              name="role"
              errors={errors.role_id}
              value={data.role_id}
              onChange={e => selectRole(e)}
            >
              <option value=""></option>
              {roles.map(({ id, name }) => (
                <option key={id} value={id}>
                  {name}
                </option>
              ))}
            </SelectInput>
            <SelectInput
              className={ showDistrict ? 'w-full pb-8 pr-6 lg:w-1/2' : 'hidden'}
              label="Kecamatan"
              name="district_id"
              errors={errors.district_id}
              value={data.district_id}
              onChange={e => setData('district_id', e.target.value)}
            >
              <option value=""></option>
              {dataDistrict.map(({ id, name }) => (
                <option key={id} value={id}>
                  {name}
                </option>
              ))}
            </SelectInput>
            <FileInput
              className="w-full pb-8 pr-6 lg:w-1/2"
              label="Photo"
              name="photo"
              accept="image/*"
              errors={errors.photo}
              value={data.photo}
              onChange={photo => setData('photo', photo)}
            />
          </div>
          <div className="flex items-center justify-end px-8 py-4 bg-gray-100 border-t border-gray-200">
            <LoadingButton
              loading={processing}
              type="submit"
              className="btn-indigo"
            >
              Tambah User
            </LoadingButton>
          </div>
        </form>
      </div>
    </div>
  );
};

Create.layout = page => <Layout title="Tambah User" children={page} />;

export default Create;
