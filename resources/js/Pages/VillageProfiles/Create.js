import React, {useEffect, useState} from 'react';
import { Inertia } from '@inertiajs/inertia';
import { InertiaLink, usePage, useForm } from '@inertiajs/inertia-react';
import Layout from '@/Shared/Layout';
import LoadingButton from '@/Shared/LoadingButton';
import TextInput from '@/Shared/TextInput';
import SelectInput from '@/Shared/SelectInput';
import FileInput from "@/Shared/FileInput";

const Create = () => {
  const { villages, opt_kampung_kb, opt_rumah_dataku, opt_desa_membangun, opt_potensi, auth, districts } = usePage().props;
  const { data, setData, errors, post, processing } = useForm({
    headman: '',
    kampung_kb: '',
    desa_membangun: '',
    rumah_dataku: '',
    potensi: '',
    dapur_stunting: '',
    village_id: '',
    district_id: auth.user.district_id || '',
    attach_kampung_kb: '',
    attach_rumah_dataku: ''
  });

  const currentDistrict = auth.user.district_id
  const defaultListVillages = currentDistrict ? villageByDistrict(currentDistrict) : villages
  const [dataVillages, setVillages] = useState(defaultListVillages);
  const [disableDistrict] = useState(currentDistrict ? true : false);

  function handleSubmit(e) {
    e.preventDefault();
    post(route('village-profiles.store'));
  }

  function selectVillage(e) {
    setData('village_id', e.target.value)
  }

  function villageByDistrict(district_id) {
    return villages.filter(function(x) { return parseInt(district_id) === parseInt(x.district_id) })
  }

  function selectDistrict(e) {
    setData('district_id', e.target.value)
    let filteredVillage = villageByDistrict(e.target.value);
    setVillages(filteredVillage)
  }

  return (
    <div>
      <h1 className="mb-8 text-3xl font-bold">
        <InertiaLink
          href={route('village-profiles')}
          className="text-indigo-600 hover:text-indigo-700"
        >
          Profil Desa
        </InertiaLink>
        <span className="font-medium text-indigo-600"> /</span> Tambah
      </h1>
      <div className="overflow-hidden bg-white rounded shadow">
        <form onSubmit={handleSubmit}>
          <div className="flex flex-wrap p-8 -mb-8 -mr-6">
            <SelectInput
              className="w-full pb-8 pr-6 lg:w-1/2"
              label="Kecamatan"
              name="district_id"
              errors={errors.district_id}
              value={data.district_id}
              onChange={e => selectDistrict(e)}
              disabled={ disableDistrict }
            >
              <option value=""></option>
              {districts.map(({ id, name }) => (
                <option key={id} value={id}>
                  {name}
                </option>
              ))}
            </SelectInput>
            <SelectInput
              className="w-full pb-8 pr-6 lg:w-1/2"
              label="Desa"
              name="village_id"
              errors={errors.village_id}
              value={data.village_id}
              onChange={e => selectVillage(e)}
            >
              <option value=""></option>
              {dataVillages.map(({ id, name }) => (
                <option key={id} value={id}>
                  {name}
                </option>
              ))}
            </SelectInput>
            <TextInput
              className="w-full pb-8 pr-6 lg:w-1/2"
              label="Kepala Desa"
              name="headman"
              errors={errors.headman}
              value={data.headman}
              onChange={e => setData('headman', e.target.value)}
            />
            <SelectInput
              className="w-full pb-8 pr-6 lg:w-1/2"
              label="Kampung KB"
              name="kampung_kb"
              errors={errors.kampung_kb}
              value={data.kampung_kb}
              onChange={e => setData('kampung_kb', e.target.value)}
            >
              <option value=""></option>
              {opt_kampung_kb.map((value, index) => (
                <option key={index} value={value}>
                  {value}
                </option>
              ))}
            </SelectInput>
            <SelectInput
              className="w-full pb-8 pr-6 lg:w-1/2"
              label="Rumah Dataku"
              name="rumah_dataku"
              errors={errors.rumah_dataku}
              value={data.rumah_dataku}
              onChange={e => setData('rumah_dataku', e.target.value)}
            >
              <option value=""></option>
              {opt_rumah_dataku.map((value, index) => (
                <option key={index} value={value}>
                  {value}
                </option>
              ))}
            </SelectInput>
            <SelectInput
              className="w-full pb-8 pr-6 lg:w-1/2"
              label="Indek Desa Membangun"
              name="desa_membangun"
              errors={errors.desa_membangun}
              value={data.desa_membangun}
              onChange={e => setData('desa_membangun', e.target.value)}
            >
              <option value=""></option>
              {opt_desa_membangun.map((value, index) => (
                <option key={index} value={value}>
                  {value}
                </option>
              ))}
            </SelectInput>
            <SelectInput
              className="w-full pb-8 pr-6 lg:w-1/2"
              label="Potensi Desa"
              name="potensi"
              errors={errors.potensi}
              value={data.potensi}
              onChange={e => setData('potensi', e.target.value)}
            >
              <option value=""></option>
              {opt_potensi.map((value, index) => (
                <option key={index} value={value}>
                  {value}
                </option>
              ))}
            </SelectInput>
            <SelectInput
              className="w-full pb-8 pr-6 lg:w-1/2"
              label="Dapur Stunting"
              name="dapur_stunting"
              errors={errors.dapur_stunting}
              value={data.dapur_stunting}
              onChange={e => setData('dapur_stunting', e.target.value)}
            >
              <option value=""></option>
              <option value="1">Ada</option>
              <option value="0">Belum Ada</option>
            </SelectInput>
            <FileInput
              className="w-full pb-8 pr-6 lg:w-1/2"
              label="Pdf Kampung KB"
              name="attach_kampung_kb"
              accept="application/pdf"
              errors={errors.attach_kampung_kb}
              value={data.attach_kampung_kb}
              onChange={attach_kampung_kb => setData('attach_kampung_kb', attach_kampung_kb)}
            />
            <FileInput
              className="w-full pb-8 pr-6 lg:w-1/2"
              label="Pdf Rumah Dataku"
              name="attach_rumah_dataku"
              accept="application/pdf"
              errors={errors.attach_rumah_dataku}
              value={data.attach_rumah_dataku}
              onChange={attach_rumah_dataku => setData('attach_rumah_dataku', attach_rumah_dataku)}
            />
          </div>
          <div className="flex items-center justify-end px-8 py-4 bg-gray-100 border-t border-gray-200">
            <LoadingButton
              loading={processing}
              type="submit"
              className="btn-indigo"
            >
              Tambah
            </LoadingButton>
          </div>
        </form>
      </div>
    </div>
  );
};

Create.layout = page => <Layout title="Profil Desa" children={page} />;

export default Create;
