import React, {useRef} from 'react';
import { InertiaLink, usePage } from '@inertiajs/inertia-react';
import Layout from '@/Shared/Layout';
import Icon from '@/Shared/Icon';
import Pagination from '@/Shared/Pagination';
import Search from "@/Shared/Search";
import pickBy from "lodash/pickBy";
import {useReactToPrint} from "react-to-print";

const Index = () => {
  const { villages, filters } = usePage().props;
  const {
    data,
    meta: { links }
  } = villages;

  console.log('data', data)

  const query = Object.keys(pickBy(filters)).length
    ? pickBy(filters)
    : {};

  const componentRef = useRef();
  const handlePrint = useReactToPrint({
    content: () => componentRef.current,
  });

  return (
    <div>
      <h1 className="mb-8 text-3xl font-bold">Profil Desa</h1>
      <div className="flex items-center justify-between mb-6">
        <Search />
        <InertiaLink
          className="btn-indigo focus:outline-none"
          href={route('village-profiles.create')}
        >
          <span>Tambah</span>
        </InertiaLink>
      </div>
      {data.length !== 0 && (
        <div className="mb-6">
          <div className="flex -mx-2">
            <div className="px-2">
              <a
                className="inline-block px-6 py-2.5 bg-green-500 text-white font-medium text-xs leading-tight uppercase rounded shadow-md hover:bg-green-600 hover:shadow-lg focus:bg-green-600 focus:shadow-lg focus:outline-none focus:ring-0 active:bg-green-700 active:shadow-lg transition duration-150 ease-in-out"
                href={route('village-profiles.export', query)}
              >
                <span>Export Excel</span>
              </a>
            </div>
            <div className="w-1/3 px-2">
              <a
                className="inline-block px-6 py-2.5 bg-gray-900 text-white font-medium text-xs leading-tight uppercase rounded shadow-md hover:bg-blue-500 hover:shadow-lg focus:bg-blue-500 focus:shadow-lg focus:outline-none focus:ring-0 active:bg-blue-600 active:shadow-lg transition duration-150 ease-in-out"
                onClick={handlePrint}
              >
                <span>Print</span>
              </a>
            </div>
          </div>
        </div>
      )}
      <div className="overflow-x-auto bg-white rounded shadow">
        <table className="w-full whitespace-nowrap" ref={componentRef}>
          <thead>
          <tr className="font-bold text-left">
            <th className="px-6 pt-5 pb-4">Kecamatan</th>
            <th className="px-6 pt-5 pb-4">Nama Desa</th>
            <th className="px-6 pt-5 pb-4">Kepala Desa</th>
            <th className="px-6 pt-5 pb-4">Kampung KB</th>
            <th className="px-6 pt-5 pb-4">Rumah Dataku</th>
            <th className="px-6 pt-5 pb-4">Indek Desa Membangun</th>
            <th className="px-6 pt-5 pb-4">Dapur Stunting</th>
            <th className="px-6 pt-5 pb-4" colSpan="2">
              Potensi Desa
            </th>
          </tr>
          </thead>
          <tbody>
          {data.map(({ id, headman, kampung_kb, desa_membangun, rumah_dataku, potensi, dapur_stunting,
                       village, district, attach_rumah_dataku, attach_kampung_kb }) => (

            <tr
              key={id}
              className="hover:bg-gray-100 focus-within:bg-gray-100"
            >
              <td className="border-t">
                <InertiaLink
                  href={route('village-profiles.edit', id)}
                  className="flex items-center px-6 py-4 focus:text-indigo-700 focus:outline-none"
                >
                  {district ? district.name : ''}
                </InertiaLink>
              </td>
              <td className="border-t">
                <InertiaLink
                  href={route('village-profiles.edit', id)}
                  className="flex items-center px-6 py-4 focus:text-indigo-700 focus:outline-none"
                >
                  {village.name}
                </InertiaLink>
              </td>
              <td className="border-t">
                <InertiaLink
                  href={route('village-profiles.edit', id)}
                  className="flex items-center px-6 py-4 focus:text-indigo-700 focus:outline-none"
                >
                  {headman}
                </InertiaLink>
              </td>
              <td className="border-t">
                <a
                  tabIndex="1"
                  className="flex items-center px-6 py-4 focus:text-indigo focus:outline-none"
                  href={attach_kampung_kb ? attach_kampung_kb : route('village-profiles.edit', id)}
                >
                  {kampung_kb}
                  {attach_kampung_kb && (<Icon
                    name="link"
                    className="block w-6 h-6 text-gray-400 fill-current ml-2"
                  />)}
                </a>
              </td>
              <td className="border-t">
                <a
                  tabIndex="-1"
                  href={attach_rumah_dataku ? attach_rumah_dataku : route('village-profiles.edit', id)}
                  className="flex items-center px-6 py-4 focus:text-indigo focus:outline-none"
                >
                  {rumah_dataku}
                  {attach_rumah_dataku && (<Icon
                    name="link"
                    className="block w-6 h-6 text-gray-400 fill-current ml-2"
                  />)}
                </a>
              </td>
              <td className="border-t">
                <InertiaLink
                  tabIndex="-1"
                  href={route('village-profiles.edit', id)}
                  className="flex items-center px-6 py-4 focus:text-indigo focus:outline-none"
                >
                  {desa_membangun}
                </InertiaLink>
              </td>
              <td className="border-t">
                <InertiaLink
                  tabIndex="-1"
                  href={route('village-profiles.edit', id)}
                  className="flex items-center px-6 py-4 focus:text-indigo focus:outline-none"
                >
                  {Boolean(dapur_stunting) ? 'Ada' : 'Belum Ada'}
                </InertiaLink>
              </td>
              <td className="border-t">
                <InertiaLink
                  tabIndex="-1"
                  href={route('village-profiles.edit', id)}
                  className="flex items-center px-6 py-4 focus:text-indigo focus:outline-none"
                >
                  {potensi}
                </InertiaLink>
              </td>
              <td className="w-px border-t">
                <InertiaLink
                  tabIndex="-1"
                  href={route('village-profiles.edit', id)}
                  className="flex items-center px-4 focus:outline-none"
                >
                  <Icon
                    name="cheveron-right"
                    className="block w-6 h-6 text-gray-400 fill-current"
                  />
                </InertiaLink>
              </td>
            </tr>
          ))}
          {data.length === 0 && (
            <tr>
              <td className="px-6 py-4 border-t" colSpan="4">
                No Profil Desa found.
              </td>
            </tr>
          )}
          </tbody>
        </table>
      </div>
      <Pagination links={links} />
    </div>
  );
};

Index.layout = page => <Layout title="Profil Desa PKK" children={page} />;

export default Index;
