import React, {useState} from 'react';
import { Inertia } from '@inertiajs/inertia';
import { InertiaLink, usePage, useForm } from '@inertiajs/inertia-react';
import Layout from '@/Shared/Layout';
import LoadingButton from '@/Shared/LoadingButton';
import TextInput from '@/Shared/TextInput';
import SelectInput from '@/Shared/SelectInput';

const Create = () => {
  const { districts, villages, auth } = usePage().props;
  const { data, setData, errors, post, processing } = useForm({
    date: '',
    nik: '',
    name: '',
    gender: '',
    birthday: '',
    created_by: '',
    to: '',
    smoking: '',
    anemia: '',
    lila: '',
    imt: '',
    stunting: '',
    village_id: '',
    district_id: auth.user.district_id || '',
  });

  const currentDistrict = auth.user.district_id
  const defaultListVillages = currentDistrict ? villageByDistrict(currentDistrict) : villages
  const [dataVillages, setVillages] = useState(defaultListVillages);
  const [disableDistrict] = useState(currentDistrict ? true : false);
  const [hideAnemia, setHideAnemia] = useState('invisible');
  const [hideLila, setHideLila] = useState('invisible');
  const [hideImt, setHideImt] = useState('invisible');

  function handleSubmit(e) {
    e.preventDefault();
    post(route('wedding-asisstances.store'));
  }

  function selectDistrict(e) {
    setData('district_id', e.target.value)
    let filteredVillage = villageByDistrict(e.target.value);
    setVillages(filteredVillage)
  }

  function selectVillage(e) {
    setData('village_id', e.target.value)
  }

  function villageByDistrict(district_id) {
    return villages.filter(function(x) { return parseInt(district_id) === parseInt(x.district_id) })
  }

  function selectGender(e) {
    setData('gender', e.target.value)

    if (parseInt(e.target.value) == 1) {
      setHideAnemia('invisible')
      setHideLila('invisible')
      setHideImt('invisible')
    } else {
      setHideAnemia('visible')
      setHideLila('visible')
      setHideImt('visible')
    }
  }

  return (
    <div>
      <h1 className="mb-8 text-3xl font-bold">
        <InertiaLink
          href={route('wedding-asisstances')}
          className="text-indigo-600 hover:text-indigo-700"
        >
          Pendampingan Nikah
        </InertiaLink>
        <span className="font-medium text-indigo-600"> /</span> Tambah
      </h1>
      <div className="overflow-hidden bg-white rounded shadow">
        <form onSubmit={handleSubmit}>
          <div className="flex flex-wrap p-8 -mb-8 -mr-6">
            <SelectInput
              className="w-full pb-8 pr-6 lg:w-1/2"
              label="Kecamatan"
              name="district_id"
              errors={errors.district_id}
              value={data.district_id}
              onChange={e => selectDistrict(e)}
              disabled={ disableDistrict }
            >
              <option value=""></option>
              {districts.map(({ id, name }) => (
                <option key={id} value={id}>
                  {name}
                </option>
              ))}
            </SelectInput>
            <SelectInput
              className="w-full pb-8 pr-6 lg:w-1/2"
              label="Desa"
              name="village_id"
              errors={errors.village_id}
              value={data.village_id}
              onChange={e => selectVillage(e)}
            >
              <option value=""></option>
              {dataVillages.map(({ id, name }) => (
                <option key={id} value={id}>
                  {name}
                </option>
              ))}
            </SelectInput>
            <TextInput
              className="w-full pb-8 pr-6 lg:w-1/2"
              label="Tanggal Pencatatan"
              name="date"
              type="date"
              errors={errors.date}
              value={data.date}
              onChange={e => setData('date', e.target.value)}
            />
            <TextInput
              className="w-full pb-8 pr-6 lg:w-1/2"
              label="Pendampingan Ke"
              name="to"
              type={"number"}
              errors={errors.to}
              value={data.to}
              onChange={e => setData('to', e.target.value)}
            />
            <TextInput
              className="w-full pb-8 pr-6 lg:w-1/2"
              label="NIK"
              name="nik"
              type="number"
              maxLength={16}
              errors={errors.nik}
              value={data.nik}
              onChange={e => setData('nik', e.target.value)}
            />
            <TextInput
              className="w-full pb-8 pr-6 lg:w-1/2"
              label="Nama Calon"
              name="name"
              errors={errors.name}
              value={data.name}
              onChange={e => setData('name', e.target.value)}
            />
            <TextInput
              className="w-full pb-8 pr-6 lg:w-1/2"
              label="Tanggal Lahir"
              name="birthday"
              type="date"
              errors={errors.birthday}
              value={data.birthday}
              onChange={e => setData('birthday', e.target.value)}
            />
            <SelectInput
              className="w-full pb-8 pr-6 lg:w-1/2"
              label="Jenis Kelamin"
              name="gender"
              errors={errors.gender}
              value={data.gender}
              onChange={e => selectGender(e)}
            >
              <option value=""></option>
              <option value="0">Perempuan</option>
              <option value="1">Laki-laki</option>
            </SelectInput>
            <SelectInput
              className="w-full pb-8 pr-6 lg:w-1/2"
              label="Merokok"
              name="smoking"
              errors={errors.smoking}
              value={data.smoking}
              onChange={e => setData('smoking', e.target.value)}
            >
              <option value=""></option>
              <option value="1">Ya</option>
              <option value="0">Tidak</option>
            </SelectInput>
            <SelectInput
              className={"w-full pb-8 pr-6 lg:w-1/2"}
              label="Resiko Stunting"
              name="stunting"
              errors={errors.stunting}
              value={data.stunting}
              onChange={e => setData('stunting', e.target.value)}
            >
              <option value=""></option>
              <option value="1">Ya</option>
              <option value="0">Tidak</option>
            </SelectInput>
            <SelectInput
              className={"w-full pb-8 pr-6 lg:w-1/2" + " " + hideAnemia}
              label="Anemia"
              name="anemia"
              errors={errors.anemia}
              value={ (hideAnemia == 'invisible') ? "" : data.anemia}
              onChange={e => setData('anemia', e.target.value)}
            >
              <option value=""></option>
              <option value="1">Ya</option>
              <option value="0">Tidak</option>
            </SelectInput>
            <SelectInput
              className={"w-full pb-8 pr-6 lg:w-1/2" + " " + hideImt}
              label={"IMT < 18.4"}
              name="imt"
              errors={errors.imt}
              value={ (hideImt == 'invisible') ? "" : data.imt}
              onChange={e => setData('imt', e.target.value)}
            >
              <option value=""></option>
              <option value="1">Ya</option>
              <option value="0">Tidak</option>
            </SelectInput>
            <SelectInput
              className={"w-full pb-8 pr-6 lg:w-1/2" + " " + hideLila}
              label={"Lila < 23.5"}
              name="lila"
              errors={errors.lila}
              value={ (hideLila == 'invisible') ? "" : data.lila}
              onChange={e => setData('lila', e.target.value)}
            >
              <option value=""></option>
              <option value="1">Ya</option>
              <option value="0">Tidak</option>
            </SelectInput>
          </div>
          <div className="flex items-center justify-end px-8 py-4 bg-gray-100 border-t border-gray-200">
            <LoadingButton
              loading={processing}
              type="submit"
              className="btn-indigo"
            >
              Tambah
            </LoadingButton>
          </div>
        </form>
      </div>
    </div>
  );
};

Create.layout = page => <Layout title="Tambah Pendampingan Nikah" children={page} />;

export default Create;
