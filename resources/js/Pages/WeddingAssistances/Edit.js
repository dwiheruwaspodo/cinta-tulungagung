import React, {useState} from 'react';
import { Inertia } from '@inertiajs/inertia';
import { InertiaLink, usePage, useForm } from '@inertiajs/inertia-react';
import Layout from '@/Shared/Layout';
import LoadingButton from '@/Shared/LoadingButton';
import TextInput from '@/Shared/TextInput';
import SelectInput from '@/Shared/SelectInput';
import DeleteButton from "@/Shared/DeleteButton";

const Edit = () => {
  const { wedding, villages, districts, auth } = usePage().props;
  const { data, setData, errors, put, processing } = useForm({
    date: wedding.date,
    nik: wedding.nik,
    name: wedding.name,
    gender: wedding.gender,
    birthday: wedding.birthday,
    created_by: wedding.created_by,
    to: wedding.to,
    smoking: wedding.smoking,
    anemia: (wedding.anemia === null) ? '' : wedding.anemia,
    lila: (wedding.lila === null) ? '' : wedding.lila,
    imt: (wedding.imt === null) ? '' : wedding.imt,
    stunting: (wedding.stunting === null) ? '' : wedding.stunting,
    village_id: wedding.village_id,
    district_id: wedding.district_id,
  });

  const currentDistrict = auth.user.district_id
  const defaultListVillages = data.district_id ? villageByDistrict(data.district_id) : villages
  const [dataVillages, setVillages] = useState(defaultListVillages);
  const [disableDistrict] = useState(currentDistrict ? true : false);
  const [hideAnemia, setHideAnemia] = useState(wedding.gender == 1 ? 'invisible' : 'visible');
  const [hideLila, setHideLila] = useState(wedding.gender == 1 ? 'invisible' : 'visible');
  const [hideImt, setHideImt] = useState(wedding.gender == 1 ? 'invisible' : 'visible');

  function handleSubmit(e) {
    e.preventDefault();
    put(route('wedding-asisstances.update', wedding.id));
  }

  function selectDistrict(e) {
    setData('district_id', e.target.value)
    let filteredVillage = villageByDistrict(e.target.value);
    setVillages(filteredVillage)
  }

  function selectVillage(e) {
    setData('village_id', e.target.value)
  }

  function villageByDistrict(district_id) {
    return villages.filter(function(x) { return parseInt(district_id) === parseInt(x.district_id) })
  }

  function destroy() {
    if (confirm('Anda yakin ingin menghapus Pendampingan Nikah ini?')) {
      Inertia.delete(route('wedding-asisstances.destroy', wedding.id));
    }
  }

  function dateFormat(date) {
    return new Date(date).toISOString().slice(0, 10);
  }

  function selectGender(e) {
    setData('gender', e.target.value)

    if (parseInt(e.target.value) == 1) {
      setHideAnemia('invisible')
      setHideLila('invisible')
      setHideImt('invisible')
    } else {
      setHideAnemia('visible')
      setHideLila('visible')
      setHideImt('visible')
    }
  }

  return (
    <div>
      <h1 className="mb-8 text-3xl font-bold">
        <InertiaLink
          href={route('wedding-asisstances')}
          className="text-indigo-600 hover:text-indigo-700"
        >
          Pendampingan Nikah
        </InertiaLink>
        <span className="font-medium text-indigo-600"> /</span> Edit
      </h1>
      <div className="overflow-hidden bg-white rounded shadow">
        <form onSubmit={handleSubmit}>
          <div className="flex flex-wrap p-8 -mb-8 -mr-6">
            <SelectInput
              className="w-full pb-8 pr-6 lg:w-1/2"
              label="Kecamatan"
              name="district_id"
              errors={errors.district_id}
              value={data.district_id}
              onChange={e => selectDistrict(e)}
              disabled={ disableDistrict }
            >
              <option value=""></option>
              {districts.map(({ id, name }) => (
                <option key={id} value={id}>
                  {name}
                </option>
              ))}
            </SelectInput>
            <SelectInput
              className="w-full pb-8 pr-6 lg:w-1/2"
              label="Desa"
              name="village_id"
              errors={errors.village_id}
              value={data.village_id}
              onChange={e => selectVillage(e)}
            >
              <option value=""></option>
              {dataVillages.map(({ id, name }) => (
                <option key={id} value={id}>
                  {name}
                </option>
              ))}
            </SelectInput>
            <TextInput
              className="w-full pb-8 pr-6 lg:w-1/2"
              label="Tanggal Pencatatan"
              name="date"
              type="date"
              errors={errors.date}
              value={dateFormat(data.date)}
              onChange={e => setData('date', e.target.value)}
            />
            <TextInput
              className="w-full pb-8 pr-6 lg:w-1/2"
              label="Pendampingan Ke"
              name="to"
              type={"number"}
              errors={errors.to}
              value={data.to}
              onChange={e => setData('to', e.target.value)}
            />
            <TextInput
              className="w-full pb-8 pr-6 lg:w-1/2"
              label="NIK"
              name="nik"
              type="number"
              maxLength={16}
              errors={errors.nik}
              value={data.nik}
              onChange={e => setData('nik', e.target.value)}
            />
            <TextInput
              className="w-full pb-8 pr-6 lg:w-1/2"
              label="Nama Calon"
              name="name"
              errors={errors.name}
              value={data.name}
              onChange={e => setData('name', e.target.value)}
            />
            <TextInput
              className="w-full pb-8 pr-6 lg:w-1/2"
              label="Tanggal Lahir"
              name="birthday"
              type="date"
              errors={errors.birthday}
              value={dateFormat(data.birthday)}
              onChange={e => setData('birthday', e.target.value)}
            />
            <SelectInput
              className="w-full pb-8 pr-6 lg:w-1/2"
              label="Jenis Kelamin"
              name="gender"
              errors={errors.gender}
              value={data.gender}
              onChange={e => selectGender(e)}
            >
              <option value=""></option>
              <option value="0">Perempuan</option>
              <option value="1">Laki-laki</option>
            </SelectInput>
            <SelectInput
              className="w-full pb-8 pr-6 lg:w-1/2"
              label="Merokok"
              name="smoking"
              errors={errors.smoking}
              value={data.smoking}
              onChange={e => setData('smoking', e.target.value)}
            >
              <option value=""></option>
              <option value="1">Ya</option>
              <option value="0">Tidak</option>
            </SelectInput>
            <SelectInput
              className={"w-full pb-8 pr-6 lg:w-1/2"}
              label="Resiko Stunting"
              name="stunting"
              errors={errors.stunting}
              value={data.stunting}
              onChange={e => setData('stunting', e.target.value)}
            >
              <option value=""></option>
              <option value="1">Ya</option>
              <option value="0">Tidak</option>
            </SelectInput>
            <SelectInput
              className={"w-full pb-8 pr-6 lg:w-1/2" + " " + hideAnemia}
              label="Anemia"
              name="anemia"
              errors={errors.anemia}
              value={ (hideAnemia == 'invisible') ? "" : data.anemia}
              onChange={e => setData('anemia', e.target.value)}
            >
              <option value=""></option>
              <option value="1">Ya</option>
              <option value="0">Tidak</option>
            </SelectInput>
            <SelectInput
              className={"w-full pb-8 pr-6 lg:w-1/2" + " " + hideImt}
              label={"IMT < 18.4"}
              name="imt"
              errors={errors.imt}
              value={ (hideImt == 'invisible') ? "" : data.imt}
              onChange={e => setData('imt', e.target.value)}
            >
              <option value=""></option>
              <option value="1">Ya</option>
              <option value="0">Tidak</option>
            </SelectInput>
            <SelectInput
              className={"w-full pb-8 pr-6 lg:w-1/2" + " " + hideLila}
              label={"Lila < 23.5"}
              name="lila"
              errors={errors.lila}
              value={ (hideLila == 'invisible') ? "" : data.lila}
              onChange={e => setData('lila', e.target.value)}
            >
              <option value=""></option>
              <option value="1">Ya</option>
              <option value="0">Tidak</option>
            </SelectInput>
          </div>
          <div className="flex items-center px-8 py-4 bg-gray-100 border-t border-gray-200">
            <DeleteButton onDelete={destroy}>Hapus</DeleteButton>
            <LoadingButton
              loading={processing}
              type="submit"
              className="ml-auto btn-indigo"
            >
              Update
            </LoadingButton>
          </div>
        </form>
      </div>
    </div>
  );
};

Edit.layout = page => <Layout title="Edit Pendampingan Nikah" children={page} />;

export default Edit;
