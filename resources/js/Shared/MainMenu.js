import React from 'react';
import MainMenuItem from '@/Shared/MainMenuItem';
import {usePage} from "@inertiajs/inertia-react";

export default ({ className }) => {
  const { auth } = usePage().props;

  return (
    <div className={className}>
      <MainMenuItem text="Dashboard" link="dashboard" icon="dashboard" />
      {(auth.isAdmin === true) && (
        <>
          <MainMenuItem text="Users" link="users" icon="smile" />
        </>
      )}

      {(auth.isAdmin === true || auth.isPKK === true) && (
        <>
          <MainMenuItem text="Aktivitas PKK & Desa" link="pkk-activities" icon="smile" />
          <MainMenuItem text="Rencana Kerja PKK & Desa" link="pkk-schedules" icon="smile" />
          <MainMenuItem text="Regulasi" link="pkk-regulations" icon="smile" />
          <MainMenuItem text="Profil Desa" link="village-profiles" icon="smile" />
          <MainMenuItem text="Prioritas" link="priorities" icon="smile" />
        </>
      )}

      {(auth.isAdmin === true || auth.isKB === true) && (
        <>
          <MainMenuItem text="Unmeet Need" link="unmeet-needs" icon="smile" />
          <MainMenuItem text="Usia Kawin" link="marriage-ages" icon="smile" />
          <MainMenuItem text="Keluarga Resiko Stunting" link="risk-of-stunting" icon="smile" />
          <MainMenuItem text="Poktan" link="poktans" icon="smile" />
          <MainMenuItem text="Pendampingan Nikah" link="wedding-asisstances" icon="smile" />
          <MainMenuItem text="Pendampingan Balita" link="balita-asisstances" icon="smile" />
          <MainMenuItem text="Pendampingan Nifas" link="nifas-asisstances" icon="smile" />
          <MainMenuItem text="Pendampingan Bumil" link="pregnancy-asisstances" icon="smile" />
          <MainMenuItem text="Pasca Salin" link="post-births" icon="smile" />
        </>
      )}

      {(auth.isAdmin === true || (auth.isKB === true && auth.isKabupaten === true)) && (
        <>
          <MainMenuItem text="BOKB" link="bokbs" icon="smile" />
        </>
      )}

      {(auth.isAdmin === true || auth.isKS === true) && (
        <>
          <MainMenuItem text="Pasca Salin" link="post-births" icon="smile" />
        </>
      )}
    </div>
  );
};
