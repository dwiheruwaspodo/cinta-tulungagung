import React, {useState} from 'react';

export default ({ label, name, className, errors = [], ...props }) => {
  const [show, setShow] = useState(false);
  return (
    <div className={className}>
      {label && (
        <label className="form-label" htmlFor={name}>
          {label}:
        </label>
      )}
      <input
        id={name}
        name={name}
        type={show ? 'text' : 'password'}
        {...props}
        className={`form-input ${errors.length ? 'error' : ''}`}
      />
      {errors && <div className="form-error">{errors}</div>}
      <div className={'flex justify-end'}>
        <a
          className={'p-1 bg-green-500 text-white font-medium text-xs leading-tight uppercase rounded shadow-md hover:bg-yellow-600 hover:shadow-lg focus:bg-yellow-600 focus:shadow-lg focus:outline-none focus:ring-0 active:bg-yellow-700 active:shadow-lg transition duration-150 ease-in-out'}
          onClick={() => setShow(!show)}>{show ? 'hide' : 'show'}
        </a>
      </div>
    </div>
  );
};
