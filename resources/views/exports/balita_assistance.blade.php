<table>
    <thead>
        <tr>
            <th>Tanggal</th>
            <th>Posyandu</th>
            <th>Nama</th>
            <th>Umur</th>
            <th>Berat</th>
            <th>Tinggi</th>
            <th>Jenis Kelamin</th>
            <th>Nama Orang Tua</th>
            <th>Kecamatan</th>
            <th>Desa</th>
            <th>Gakin</th>
            <th>Status</th>
        </tr>
    </thead>
    <tbody>
        @foreach($data as $pkk)
        <tr>
            <td>{{ $pkk->date }}</td>
            <td>{{ $pkk->posyandu }}</td>
            <td>{{ $pkk->name }}</td>
            <td>{{ $pkk->age }}</td>
            <td>{{ $pkk->weight }}</td>
            <td>{{ $pkk->height }}</td>
            <td>{{ boolval($pkk->gender) ? 'Laki-laki' : 'Perempuan' }}</td>
            <td>{{ $pkk->parent_name }}</td>
            <td>{{ $pkk->district ? $pkk->district->name : '' }}</td>
            <td>{{ $pkk->village ? $pkk->village->name : '' }}</td>
            <td>{{ boolval($pkk->gakin) ? 'Ya' : 'Tidak' }}</td>
            <td>{{ $pkk->status }}</td>
        </tr>
        @endforeach
    </tbody>
</table>
