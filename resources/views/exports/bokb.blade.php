<table>
    <thead>
        <tr>
            <th>Tanggal</th>
            <th>Kecamatan</th>
            <th>Desa</th>
            <th>Jenis Kegiatan</th>
            <th>Lokasi</th>
            <th>Link Document</th>
            <th>Narasumber</th>
            <th>Asal Narasumber</th>
            <th>Nama Warung</th>
        </tr>
    </thead>
    <tbody>
        @foreach($data as $pkk)
        <tr>
            <td>{{ $pkk->date }}</td>
            <td>{{ $pkk->district ? $pkk->district->name : '' }}</td>
            <td>{{ $pkk->village ? $pkk->village->name : '' }}</td>
            <td>{{ $pkk->activity }}</td>
            <td>{{ $pkk->location }}</td>
            <td>{{ $pkk->link_document }}</td>
            <td>{{ $pkk->source_person }}</td>
            <td>{{ $pkk->source_person_from }}</td>
            <td>{{ $pkk->warung }}</td>
        </tr>
        @endforeach
    </tbody>
</table>
