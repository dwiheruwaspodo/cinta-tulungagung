<table>
    <thead>
        <tr>
            <th>Tahun</th>
            <th>Bulan</th>
            <th>Kecamatan</th>
            <th>Desa</th>
            <th>{{ 'Usia < 20 Tahun' }}</th>
            <th>{{ 'Usia 21 - 25 Tahun' }}</th>
            <th>{{ 'Usia 26 - 20 Tahun' }}</th>
            <th>{{ 'Usia > 30 Tahun' }}</th>
            <th>Jumlah Perkawinan</th>
        </tr>
    </thead>
    <tbody>
        @foreach($data as $pkk)
        <tr>
            <td>{{ date('Y', strtotime($pkk->date)) }}</td>
            <td>{{ date('F', strtotime($pkk->date)) }}</td>
            <td>{{ $pkk->district ? $pkk->district->name : '' }}</td>
            <td>{{ $pkk->village ? $pkk->village->name : '' }}</td>
            <td>{{ $pkk->less_20 }}</td>
            <td>{{ $pkk->age_21_25 }}</td>
            <td>{{ $pkk->age_26_30 }}</td>
            <td>{{ $pkk->more_30 }}</td>
            <td>{{ $pkk->less_20 + $pkk->age_21_25 + $pkk->age_26_30 + $pkk->more_30 }}</td>
        </tr>
        @endforeach
    </tbody>
</table>
