<table>
    <thead>
        <tr>
            <th>Tahun</th>
            <th>Bulan</th>
            <th>Kecamatan</th>
            <th>{{ 'Usia < 20 Tahun' }}</th>
            <th>{{ 'Usia 21 - 25 Tahun' }}</th>
            <th>{{ 'Usia 26 - 20 Tahun' }}</th>
            <th>{{ 'Usia > 30 Tahun' }}</th>
            <th>Jumlah Perkawinan</th>
        </tr>
    </thead>
    <tbody>
        @foreach($data as $pkk)
        <tr>
            <td>{{ $pkk->year }}</td>
            <td>{{ date('F', mktime(0, 0, 0, $pkk->month, 10)) }}</td>
            <td>{{ $pkk->district ? $pkk->district->name : '' }}</td>
            <td>{{ $pkk->sum_less_20 }}</td>
            <td>{{ $pkk->sum_age_21_25 }}</td>
            <td>{{ $pkk->sum_age_26_30 }}</td>
            <td>{{ $pkk->sum_more_30 }}</td>
            <td>{{ (int)$pkk->sum_less_20 + (int)$pkk->sum_age_21_25 + (int)$pkk->sum_age_26_30 + (int)$pkk->sum_more_30 }}</td>
        </tr>
        @endforeach
    </tbody>
</table>
