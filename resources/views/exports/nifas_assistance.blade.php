<table>
    <thead>
        <tr>
            <th>Tanggal</th>
            <th>Kecamatan</th>
            <th>Desa</th>
            <th>NIK</th>
            <th>Nama</th>
            <th>Tanggal lahir</th>
            <th>Alat Kontrasepsi</th>
            <th>Keterangan</th>
        </tr>
    </thead>
    <tbody>
        @foreach($data as $pkk)
        <tr>
            <td>{{ $pkk->date }}</td>
            <td>{{ $pkk->district ? $pkk->district->name : '' }}</td>
            <td>{{ $pkk->village ? $pkk->village->name : '' }}</td>
            <td>{{ $pkk->nik }}</td>
            <td>{{ $pkk->name }}</td>
            <td>{{ $pkk->birthday }}</td>
            <td>{{ boolval($pkk->is_contraception) ? $pkk->contraception : '-' }}</td>
            <td>{{ $pkk->description }}</td>
        </tr>
        @endforeach
    </tbody>
</table>
