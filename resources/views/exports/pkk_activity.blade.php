<table>
    <thead>
        <tr>
            <th>Tingkat</th>
            <th>Tanggal</th>
            <th>Kategori</th>
            <th>Kecamatan</th>
            <th>Desa</th>
            <th>Deskripsi</th>
            <th>Narasumber</th>
            <th>Lembaga</th>
            <th>Tempat Kegiatan</th>
            <th>Sasaran</th>
            <th>Jumlah Sasaran</th>
        </tr>
    </thead>
    <tbody>
        @foreach($data as $pkk)
        <tr>
            <td>{{ $pkk->level }}</td>
            <td>{{ $pkk->date }}</td>
            <td>{{ $pkk->pkk_activity_category ? $pkk->pkk_activity_category->category : '' }}</td>
            <td>{{ $pkk->district ? $pkk->district->name : '' }}</td>
            <td>{{ $pkk->village ? $pkk->village->name : '' }}</td>
            <td>{{ $pkk->description }}</td>
            <td>{{ $pkk->source_person }}</td>
            <td>{{ $pkk->source_person_from }}</td>
            <td>{{ $pkk->location }}</td>
            <td>{{ $pkk->target }}</td>
            <td>{{ $pkk->target_amount }}</td>
        </tr>
        @endforeach
    </tbody>
</table>
