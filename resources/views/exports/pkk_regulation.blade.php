<table>
    <thead>
        <tr>
            <th>Tanggal</th>
            <th>Nomor</th>
            <th>Nama</th>
            <th>Deskripsi</th>
            <th>Dikeluarkan Oleh</th>
        </tr>
    </thead>
    <tbody>
        @foreach($data as $pkk)
        <tr>
            <td>{{ $pkk->date }}</td>
            <td>{{ $pkk->number }}</td>
            <td>{{ $pkk->name }}</td>
            <td>{{ $pkk->description }}</td>
            <td>{{ $pkk->issued_by }}</td>
        </tr>
        @endforeach
    </tbody>
</table>
