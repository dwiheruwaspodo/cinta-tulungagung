<table>
    <thead>
        <tr>
            <th>Tanggal</th>
            <th>Kategori</th>
            <th>Deskripsi</th>
            <th>Kecamatan</th>
            <th>Desa</th>
            <th>Lokasi</th>
        </tr>
    </thead>
    <tbody>
        @foreach($data as $pkk)
        <tr>
            <td>{{ $pkk->date }}</td>
            <td>{{ $pkk->pkk_activity_category ? $pkk->pkk_activity_category->category : '' }}</td>
            <td>{{ $pkk->description }}</td>
            <td>{{ $pkk->district ? $pkk->district->name : '' }}</td>
            <td>{{ $pkk->village ? $pkk->village->name : '' }}</td>
            <td>{{ $pkk->location }}</td>
        </tr>
        @endforeach
    </tbody>
</table>
