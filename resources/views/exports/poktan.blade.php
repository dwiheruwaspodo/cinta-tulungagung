<table>
    <thead>
    <tr>
        <th>Tahun</th>
        <th>Bulan</th>
        <th>Kecamatan</th>
        <th>Desa</th>
        <th>Jumlah Kel. BKB</th>
        <th>Jumlah anggota hadir BKB</th>
        <th>Jumlah Kel. BKB</th>
        <th>Jumlah anggota hadir BKB</th>
        <th>Jumlah Kel. BKR</th>
        <th>Jumlah anggota hadir BKR</th>
        <th>Jumlah Kel. BKL</th>
        <th>Jumlah anggota hadir BKL</th>
        <th>Jumlah Kel. UPPKS</th>
        <th>Jumlah anggota hadir UPPKS</th>
    </tr>
    </thead>
    <tbody>
    @foreach($data as $pkk)
        <tr>
            <td>{{ date('Y', strtotime($pkk->date)) }}</td>
            <td>{{ date('F', strtotime($pkk->date)) }}</td>
            <td>{{ $pkk->district ? $pkk->district->name : '' }}</td>
            <td>{{ $pkk->village ? $pkk->village->name : '' }}</td>
            <td>{{ $pkk->keluarga_bkb }}</td>
            <td>{{ $pkk->anggota_hadir_bkb }}</td>
            <td>{{ $pkk->keluarga_bkr }}</td>
            <td>{{ $pkk->anggota_hadir_bkr }}</td>
            <td>{{ $pkk->keluarga_bkl }}</td>
            <td>{{ $pkk->anggota_hadir_bkl }}</td>
            <td>{{ $pkk->keluarga_uppks }}</td>
            <td>{{ $pkk->anggota_hadir_uppks }}</td>
            <td>{{ $pkk->keluarga_pikr }}</td>
            <td>{{ $pkk->anggota_hadir_pikr }}</td>
        </tr>
    @endforeach
    </tbody>
</table>
