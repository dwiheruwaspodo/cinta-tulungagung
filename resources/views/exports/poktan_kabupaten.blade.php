<table>
    <thead>
    <tr>
        <th>Tahun</th>
        <th>Bulan</th>
        <th>Kecamatan</th>
        <th>Jumlah Kel. BKB</th>
        <th>Jumlah anggota hadir BKB</th>
        <th>Jumlah Kel. BKB</th>
        <th>Jumlah anggota hadir BKB</th>
        <th>Jumlah Kel. BKR</th>
        <th>Jumlah anggota hadir BKR</th>
        <th>Jumlah Kel. BKL</th>
        <th>Jumlah anggota hadir BKL</th>
        <th>Jumlah Kel. UPPKS</th>
        <th>Jumlah anggota hadir UPPKS</th>
    </tr>
    </thead>
    <tbody>
    @foreach($data as $pkk)
        <tr>
            <td>{{ $pkk->year }}</td>
            <td>{{ date('F', mktime(0, 0, 0, $pkk->month, 10)) }}</td>
            <td>{{ $pkk->district ? $pkk->district->name : '' }}</td>
            <td>{{ $pkk->sum_keluarga_bkb }}</td>
            <td>{{ $pkk->sum_anggota_hadir_bkb }}</td>
            <td>{{ $pkk->sum_keluarga_bkr }}</td>
            <td>{{ $pkk->sum_anggota_hadir_bkr }}</td>
            <td>{{ $pkk->sum_keluarga_bkl }}</td>
            <td>{{ $pkk->sum_anggota_hadir_bkl }}</td>
            <td>{{ $pkk->sum_keluarga_uppks }}</td>
            <td>{{ $pkk->sum_anggota_hadir_uppks }}</td>
            <td>{{ $pkk->sum_keluarga_pikr }}</td>
            <td>{{ $pkk->sum_anggota_hadir_pikr }}</td>
        </tr>
    @endforeach
    </tbody>
</table>
