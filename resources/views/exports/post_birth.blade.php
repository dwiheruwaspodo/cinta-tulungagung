<table>
    <thead>
    <tr>
        <th rowspan="2">Tahun</th>
        <th rowspan="2">Bulan</th>
        <th rowspan="2">Kecamatan</th>
        <th rowspan="2">Desa</th>
        <th colspan="7">Pasca Persalinan</th>
        <th rowspan="2">Jumlah</th>
        <th colspan="7">Pasca Keguguran</th>
        <th rowspan="2">Jumlah</th>
    </tr>
    <tr>
        <th>Suntik</th>
        <th>Pil</th>
        <th>Kondom</th>
        <th>IUD</th>
        <th>Implan</th>
        <th>MOP</th>
        <th>MOW</th>
        <th>Suntik</th>
        <th>Pil</th>
        <th>Kondom</th>
        <th>IUD</th>
        <th>Implan</th>
        <th>MOP</th>
        <th>MOW</th>
    </tr>
    </thead>
    <tbody>
        @foreach($data as $pkk)
            <tr>
                <td>{{ date('Y', strtotime($pkk->date)) }}</td>
                <td>{{ date('F', strtotime($pkk->date)) }}</td>
                <td>{{ $pkk->district ? $pkk->district->name : '' }}</td>
                <td>{{ $pkk->village ? $pkk->village->name : '' }}</td>
                <td>{{ $pkk->birth_suntik }}</td>
                <td>{{ $pkk->birth_pil }}</td>
                <td>{{ $pkk->birth_kondom }}</td>
                <td>{{ $pkk->birth_iud }}</td>
                <td>{{ $pkk->birth_implan }}</td>
                <td>{{ $pkk->birth_mop }}</td>
                <td>{{ $pkk->birth_mow }}</td>
                <td>{{ $pkk->birth_suntik + $pkk->birth_pil + $pkk->birth_kondom + $pkk->birth_iud + $pkk->birth_implan + $pkk->birth_mop + $pkk->birth_mow  }}</td>
                <td>{{ $pkk->miscarriage_suntik }}</td>
                <td>{{ $pkk->miscarriage_pil }}</td>
                <td>{{ $pkk->miscarriage_kondom }}</td>
                <td>{{ $pkk->miscarriage_iud }}</td>
                <td>{{ $pkk->miscarriage_implan }}</td>
                <td>{{ $pkk->miscarriage_mop }}</td>
                <td>{{ $pkk->miscarriage_mow }}</td>
                <td>{{ $pkk->miscarriage_suntik + $pkk->miscarriage_pil + $pkk->miscarriage_kondom + $pkk->miscarriage_iud + $pkk->miscarriage_implan + $pkk->miscarriage_mop + $pkk->miscarriage_mow  }}</td>
            </tr>
        @endforeach
    </tbody>
</table>
