<table>
    <thead>
    <tr>
        <th rowspan="2">Tahun</th>
        <th rowspan="2">Bulan</th>
        <th rowspan="2">Kecamatan</th>
        <th colspan="7">Pasca Persalinan</th>
        <th rowspan="2">Jumlah</th>
        <th colspan="7">Pasca Keguguran</th>
        <th rowspan="2">Jumlah</th>
    </tr>
    <tr>
        <th>Suntik</th>
        <th>Pil</th>
        <th>Kondom</th>
        <th>IUD</th>
        <th>Implan</th>
        <th>MOP</th>
        <th>MOW</th>
        <th>Suntik</th>
        <th>Pil</th>
        <th>Kondom</th>
        <th>IUD</th>
        <th>Implan</th>
        <th>MOP</th>
        <th>MOW</th>
    </tr>
    </thead>
    <tbody>
        @foreach($data as $pkk)
            <tr>
                <td>{{ $pkk->year }}</td>
                <td>{{ date('F', mktime(0, 0, 0, $pkk->month, 10)) }}</td>
                <td>{{ $pkk->district ? $pkk->district->name : '' }}</td>
                <td>{{ $pkk->sum_birth_suntik }}</td>
                <td>{{ $pkk->sum_birth_pil }}</td>
                <td>{{ $pkk->sum_birth_kondom }}</td>
                <td>{{ $pkk->sum_birth_iud }}</td>
                <td>{{ $pkk->sum_birth_implan }}</td>
                <td>{{ $pkk->sum_birth_mop }}</td>
                <td>{{ $pkk->sum_birth_mow }}</td>
                <td>{{ (int)$pkk->sum_birth_suntik + (int)$pkk->sum_birth_pil + (int)$pkk->sum_birth_kondom + (int)$pkk->sum_birth_iud + (int)$pkk->sum_birth_implan + (int)$pkk->sum_birth_mop + (int)$pkk->sum_birth_mow  }}</td>
                <td>{{ $pkk->sum_miscarriage_suntik }}</td>
                <td>{{ $pkk->sum_miscarriage_pil }}</td>
                <td>{{ $pkk->sum_miscarriage_kondom }}</td>
                <td>{{ $pkk->sum_miscarriage_iud }}</td>
                <td>{{ $pkk->sum_miscarriage_implan }}</td>
                <td>{{ $pkk->sum_miscarriage_mop }}</td>
                <td>{{ $pkk->sum_miscarriage_mow }}</td>
                <td>{{ (int)$pkk->sum_miscarriage_suntik + (int)$pkk->sum_miscarriage_pil + (int)$pkk->sum_miscarriage_kondom + (int)$pkk->sum_miscarriage_iud + (int)$pkk->sum_miscarriage_implan + (int)$pkk->sum_miscarriage_mop + (int)$pkk->sum_miscarriage_mow  }}</td>
            </tr>
        @endforeach
    </tbody>
</table>
