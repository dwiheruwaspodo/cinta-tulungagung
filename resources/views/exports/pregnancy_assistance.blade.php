<table>
    <thead>
        <tr>
            <th>Tanggal</th>
            <th>Kecamatan</th>
            <th>Desa</th>
            <th>NIK</th>
            <th>Nama Ibu</th>
            <th>Tanggal Lahir</th>
            <th>Usia Kandungan (bulan)</th>
            <th>Anemia</th>
            <th>KEK</th>
            <th>Pertumbuhan Janin Terhambat</th>
            <th>Resiko Stunting</th>
            <th>4 Terlalu</th>
            <th>Keterangan</th>
        </tr>
    </thead>
    <tbody>
        @foreach($data as $pkk)
        <tr>
            <td>{{ $pkk->date }}</td>
            <td>{{ $pkk->district ? $pkk->district->name : '' }}</td>
            <td>{{ $pkk->village ? $pkk->village->name : '' }}</td>
            <td>{{ $pkk->nik }}</td>
            <td>{{ $pkk->name }}</td>
            <td>{{ $pkk->birthday }}</td>
            <td>{{ $pkk->pregnancy_age }}</td>
            <td>{{ boolval($pkk->anemia) ? 'Ya' : 'Tidak' }}</td>
            <td>{{ boolval($pkk->kek) ? 'Ya' : 'Tidak' }}</td>
            <td>{{ boolval($pkk->inhibited_fetal_growth) ? 'Ya' : 'Tidak' }}</td>
            <td>{{ boolval($pkk->stunting) ? 'Ya' : 'Tidak' }}</td>
            <td>{{ boolval($pkk->four_too) ? 'Ya' : 'Tidak' }}</td>
            <td>{{ $pkk->description }}</td>
        </tr>
        @endforeach
    </tbody>
</table>
