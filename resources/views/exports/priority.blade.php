<table>
    <thead>
        <tr>
            <th>Tahun</th>
            <th>Bulan</th>
            <th>Kecamatan</th>
            <th>Desa</th>
            <th>Jumlah Sekolah Siaga Kependudukan</th>
            <th>Jumlah DAHSAT</th>
            <th>Jumlah Kampung KB</th>
            <th>Jumlah Rumah DataKu</th>
        </tr>
    </thead>
    <tbody>
        @foreach($data as $pkk)
        <tr>
            <td>{{ date('Y', strtotime($pkk->date)) }}</td>
            <td>{{ date('F', strtotime($pkk->date)) }}</td>
            <td>{{ $pkk->district ? $pkk->district->name : '' }}</td>
            <td>{{ $pkk->village ? $pkk->village->name : '' }}</td>
            <td>{{ $pkk->ssk }}</td>
            <td>{{ $pkk->dahsat }}</td>
            <td>{{ $pkk->kampung_kb }}</td>
            <td>{{ $pkk->rumah_dataku }}</td>
        </tr>
        @endforeach
    </tbody>
</table>
