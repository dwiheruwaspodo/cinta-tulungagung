<table>
    <thead>
        <tr>
            <th>Kecamatan</th>
            <th>Desa</th>
            <th>RW</th>
            <th>RT</th>
            <th>Kepala Keluarga</th>
            <th>Baduta</th>
            <th>Balita</th>
            <th>PUS</th>
            <th>PUS Hamil</th>
            <th>Sumber Air</th>
            <th>Jamban</th>
            <th>Terlalu Muda</th>
            <th>Terlalu Tua</th>
            <th>Terlalu Dekat</th>
            <th>Terlalu Rapat</th>
            <th>Resiko Stunting</th>
        </tr>
    </thead>
    <tbody>
        @foreach($data as $pkk)
        <tr>
            <td>{{ $pkk->district ? $pkk->district->name : '' }}</td>
            <td>{{ $pkk->village ? $pkk->village->name : '' }}</td>
            <td>{{ $pkk->rw }}</td>
            <td>{{ $pkk->rt }}</td>
            <td>{{ $pkk->head_of_family }}</td>
            <td>{{ boolval($pkk->baduta) ? 'Ya' : 'Tidak' }}</td>
            <td>{{ boolval($pkk->balita) ? 'Ya' : 'Tidak' }}</td>
            <td>{{ boolval($pkk->pus) ? 'Ya' : 'Tidak' }}</td>
            <td>{{ boolval($pkk->pus_hamil) ? 'Ya' : 'Tidak' }}</td>
            <td>{{ $pkk->source_of_water }}</td>
            <td>{{ $pkk->toilet }}</td>
            <td>{{ boolval($pkk->too_young) ? 'Ya' : 'Tidak' }}</td>
            <td>{{ boolval($pkk->too_old) ? 'Ya' : 'Tidak' }}</td>
            <td>{{ boolval($pkk->too_near) ? 'Ya' : 'Tidak' }}</td>
            <td>{{ boolval($pkk->too_tight) ? 'Ya' : 'Tidak' }}</td>
            <td>{{ boolval($pkk->risk) ? 'Ya' : 'Tidak' }}</td>
        </tr>
        @endforeach
    </tbody>
</table>
