<table>
    <thead>
        <tr>
            <th>Tahun</th>
            <th>Bulan</th>
            <th>Kecamatan</th>
            <th>Desa</th>
            <th>IAT</th>
            <th>TIAL</th>
            <th>Jumlah ( IAT + TIAL )</th>
            <th>IAS</th>
            <th>Hamil</th>
            <th>Jumlah ( IAS + Hamil )</th>
        </tr>
    </thead>
    <tbody>
        @foreach($data as $pkk)
        <tr>
            <td>{{ date('Y', strtotime($pkk->date)) }}</td>
            <td>{{ date('F', strtotime($pkk->date)) }}</td>
            <td>{{ $pkk->district ? $pkk->district->name : '' }}</td>
            <td>{{ $pkk->village ? $pkk->village->name : '' }}</td>
            <td>{{ $pkk->iat }}</td>
            <td>{{ $pkk->tial }}</td>
            <td>{{ $pkk->tial + $pkk->iat }}</td>
            <td>{{ $pkk->ias }}</td>
            <td>{{ $pkk->hamil }}</td>
            <td>{{ $pkk->ias + $pkk->hamil }}</td>
        </tr>
        @endforeach
    </tbody>
</table>
