<table>
    <thead>
        <tr>
            <th>Tahun</th>
            <th>Bulan</th>
            <th>Kecamatan</th>
            <th>IAT</th>
            <th>TIAL</th>
            <th>Jumlah ( IAT + TIAL )</th>
            <th>IAS</th>
            <th>Hamil</th>
            <th>Jumlah ( IAS + Hamil )</th>
        </tr>
    </thead>
    <tbody>
        @foreach($data as $pkk)
        <tr>
            <td>{{ $pkk->year }}</td>
            <td>{{ date('F', mktime(0, 0, 0, $pkk->month, 10)) }}</td>
            <td>{{ $pkk->district ? $pkk->district->name : '' }}</td>
            <td>{{ $pkk->sum_iat }}</td>
            <td>{{ $pkk->sum_tial }}</td>
            <td>{{ (int)$pkk->sum_tial + (int)$pkk->sum_iat }}</td>
            <td>{{ $pkk->sum_ias }}</td>
            <td>{{ $pkk->sum_hamil }}</td>
            <td>{{ (int)$pkk->sum_ias + (int)$pkk->sum_hamil }}</td>
        </tr>
        @endforeach
    </tbody>
</table>
