<table>
    <thead>
        <tr>
            <th>Nama Desa</th>
            <th>Kepala Desa</th>
            <th>Kampung KB</th>
            <th>Rumah Dataku</th>
            <th>Indek Desa Membangun</th>
            <th>Dapur Stunting</th>
            <th>Potensi Desa</th>
        </tr>
    </thead>
    <tbody>
        @foreach($data as $pkk)
        <tr>
{{--            <td>{{ $pkk->district ? $pkk->district->name : '' }}</td>--}}
            <td>{{ $pkk->village ? $pkk->village->name : '' }}</td>
            <td>{{ $pkk->headman }}</td>
            <td>{{ $pkk->kampung_kb }}</td>
            <td>{{ $pkk->rumah_dataku }}</td>
            <td>{{ $pkk->desa_membangun }}</td>
            <td>{{ boolval($pkk->dapur_stunting) ? 'Ada' : 'Belum Ada' }}</td>
            <td>{{ $pkk->potensi }}</td>
        </tr>
        @endforeach
    </tbody>
</table>
