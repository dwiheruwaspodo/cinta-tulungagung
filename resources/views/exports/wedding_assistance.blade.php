<table>
    <thead>
        <tr>
            <th>Tanggal</th>
            <th>Pendampingan Ke</th>
            <th>Kecamatan</th>
            <th>Desa</th>
            <th>NIK</th>
            <th>Nama Calon</th>
            <th>Jenis Kelamin</th>
            <th>Tanggal Lahir</th>
            <th>Merokok</th>
            <th>Anemia</th>
            <th>{{ 'Lila < 23.5' }}</th>
            <th>{{ 'IMT < 18.4' }}</th>
            <th>Resiko Stunting</th>
        </tr>
    </thead>
    <tbody>
        @foreach($data as $pkk)
        <tr>
            <td>{{ $pkk->date }}</td>
            <td>{{ $pkk->to }}</td>
            <td>{{ $pkk->district ? $pkk->district->name : '' }}</td>
            <td>{{ $pkk->village ? $pkk->village->name : '' }}</td>
            <td>{{ $pkk->nik }}</td>
            <td>{{ $pkk->name }}</td>
            <td>{{ boolval($pkk->gender) ? 'Laki-laki' : 'Perempuan' }}</td>
            <td>{{ $pkk->birthday }}</td>
            <td>{{ boolval($pkk->smoking) ? 'Ya' : 'Tidak' }}</td>
            <td>{{ boolval($pkk->anemia) ? 'Ya' : 'Tidak' }}</td>
            <td>{{ boolval($pkk->lila) ? 'Ya' : 'Tidak' }}</td>
            <td>{{ boolval($pkk->imt) ? 'Ya' : 'Tidak' }}</td>
            <td>{{ boolval($pkk->stunting) ? 'Ya' : 'Tidak' }}</td>
        </tr>
        @endforeach
    </tbody>
</table>
