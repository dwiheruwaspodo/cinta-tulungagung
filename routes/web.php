<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Auth
Route::get('login')->name('login')->uses('Auth\LoginController@showLoginForm')->middleware('guest');
Route::post('login')->name('login.attempt')->uses('Auth\LoginController@login')->middleware('guest');
Route::post('logout')->name('logout')->uses('Auth\LoginController@logout');

// Dashboard
Route::get('/')->name('dashboard')->uses('DashboardController')->middleware('auth');

// Users
Route::get('users')->name('users')->uses('UsersController@index')->middleware('remember', 'auth', 'admin');
Route::get('users/create')->name('users.create')->uses('UsersController@create')->middleware('auth', 'admin');
Route::post('users')->name('users.store')->uses('UsersController@store')->middleware('auth','admin');
Route::get('users/{user}/edit')->name('users.edit')->uses('UsersController@edit')->middleware('auth', 'admin');
Route::put('users/{user}')->name('users.update')->uses('UsersController@update')->middleware('auth', 'admin');
Route::delete('users/{user}')->name('users.destroy')->uses('UsersController@destroy')->middleware('auth', 'admin');
Route::put('users/{user}/restore')->name('users.restore')->uses('UsersController@restore')->middleware('auth', 'admin');

// Images
Route::get('/img/{path}', 'ImagesController@show')->where('path', '.*');

// PKK
Route::get('pkk-activities')->name('pkk-activities')->uses('PkkActivityController@index')->middleware('remember', 'auth', 'pkk');
Route::get('pkk-activities/export')->name('pkk-activities.export')->uses('PkkActivityController@export')->middleware('remember', 'auth', 'pkk');
Route::get('pkk-activities/create')->name('pkk-activities.create')->uses('PkkActivityController@create')->middleware('auth', 'pkk');
Route::post('pkk-activities')->name('pkk-activities.store')->uses('PkkActivityController@store')->middleware('auth', 'pkk');
Route::get('pkk-activities/{pkkActivity}/edit')->name('pkk-activities.edit')->uses('PkkActivityController@edit')->middleware('auth', 'pkk');
Route::put('pkk-activities/{pkkActivity}')->name('pkk-activities.update')->uses('PkkActivityController@update')->middleware('auth', 'pkk');
Route::delete('pkk-activities/{pkkActivity}')->name('pkk-activities.destroy')->uses('PkkActivityController@destroy')->middleware('auth', 'pkk');

// PKK Regulation
Route::get('pkk-regulations')->name('pkk-regulations')->uses('PkkRegulationController@index')->middleware('remember', 'auth', 'pkk');
Route::get('pkk-regulations/export')->name('pkk-regulations.export')->uses('PkkRegulationController@export')->middleware('remember', 'auth', 'pkk');
Route::get('pkk-regulations/create')->name('pkk-regulations.create')->uses('PkkRegulationController@create')->middleware('auth', 'pkk');
Route::post('pkk-regulations')->name('pkk-regulations.store')->uses('PkkRegulationController@store')->middleware('auth', 'pkk');
Route::get('pkk-regulations/{pkkRegulation}/edit')->name('pkk-regulations.edit')->uses('PkkRegulationController@edit')->middleware('auth', 'pkk');
Route::put('pkk-regulations/{pkkRegulation}')->name('pkk-regulations.update')->uses('PkkRegulationController@update')->middleware('auth', 'pkk');
Route::delete('pkk-regulations/{pkkRegulation}')->name('pkk-regulations.destroy')->uses('PkkRegulationController@destroy')->middleware('auth', 'pkk');

// PKK Schedule
Route::get('pkk-schedules')->name('pkk-schedules')->uses('PkkScheduleController@index')->middleware('remember', 'auth', 'pkk');
Route::get('pkk-schedules/export')->name('pkk-schedules.export')->uses('PkkScheduleController@export')->middleware('remember', 'auth', 'pkk');
Route::get('pkk-schedules/create')->name('pkk-schedules.create')->uses('PkkScheduleController@create')->middleware('auth', 'pkk');
Route::post('pkk-schedules')->name('pkk-schedules.store')->uses('PkkScheduleController@store')->middleware('auth', 'pkk');
Route::get('pkk-schedules/{pkkSchedule}/edit')->name('pkk-schedules.edit')->uses('PkkScheduleController@edit')->middleware('auth', 'pkk');
Route::put('pkk-schedules/{pkkSchedule}')->name('pkk-schedules.update')->uses('PkkScheduleController@update')->middleware('auth', 'pkk');
Route::delete('pkk-schedules/{pkkActivity}')->name('pkk-schedules.destroy')->uses('PkkScheduleController@destroy')->middleware('auth', 'pkk');

// Village Profile
Route::get('village-profiles')->name('village-profiles')->uses('VillageProfileController@index')->middleware('remember', 'auth', 'pkk');
Route::get('village-profiles/export')->name('village-profiles.export')->uses('VillageProfileController@export')->middleware('remember', 'auth', 'pkk');
Route::get('village-profiles/create')->name('village-profiles.create')->uses('VillageProfileController@create')->middleware('auth', 'pkk');
Route::post('village-profiles')->name('village-profiles.store')->uses('VillageProfileController@store')->middleware('auth', 'pkk');
Route::get('village-profiles/{villageProfile}/edit')->name('village-profiles.edit')->uses('VillageProfileController@edit')->middleware('auth', 'pkk');
Route::put('village-profiles/{villageProfile}')->name('village-profiles.update')->uses('VillageProfileController@update')->middleware('auth', 'pkk');
Route::delete('village-profiles/{villageProfile}')->name('village-profiles.destroy')->uses('VillageProfileController@destroy')->middleware('auth', 'pkk');

// Risk Of Stunting
Route::get('risk-of-stunting')->name('risk-of-stunting')->uses('RiskOfStuntingController@index')->middleware('remember', 'auth', 'kb');
Route::get('risk-of-stunting/export')->name('risk-of-stunting.export')->uses('RiskOfStuntingController@export')->middleware('auth', 'kb');
Route::get('risk-of-stunting/create')->name('risk-of-stunting.create')->uses('RiskOfStuntingController@create')->middleware('auth', 'kb');
Route::post('risk-of-stunting')->name('risk-of-stunting.store')->uses('RiskOfStuntingController@store')->middleware('auth', 'kb');
Route::get('risk-of-stunting/{riskOfStunting}/edit')->name('risk-of-stunting.edit')->uses('RiskOfStuntingController@edit')->middleware('auth', 'kb');
Route::put('risk-of-stunting/{riskOfStunting}')->name('risk-of-stunting.update')->uses('RiskOfStuntingController@update')->middleware('auth', 'kb');
Route::delete('risk-of-stunting/{riskOfStunting}')->name('risk-of-stunting.destroy')->uses('RiskOfStuntingController@destroy')->middleware('auth', 'kb');

// Wedding Assistance
Route::get('wedding-asisstances')->name('wedding-asisstances')->uses('WeddingAssistanceController@index')->middleware('remember', 'auth', 'kb');
Route::get('wedding-asisstances/export')->name('wedding-asisstances.export')->uses('WeddingAssistanceController@export')->middleware('remember', 'auth', 'kb');
Route::get('wedding-asisstances/create')->name('wedding-asisstances.create')->uses('WeddingAssistanceController@create')->middleware('auth', 'kb');
Route::post('wedding-asisstances')->name('wedding-asisstances.store')->uses('WeddingAssistanceController@store')->middleware('auth', 'kb');
Route::get('wedding-asisstances/{weddingAssistance}/edit')->name('wedding-asisstances.edit')->uses('WeddingAssistanceController@edit')->middleware('auth', 'kb');
Route::put('wedding-asisstances/{weddingAssistance}')->name('wedding-asisstances.update')->uses('WeddingAssistanceController@update')->middleware('auth', 'kb');
Route::delete('wedding-asisstances/{weddingAssistance}')->name('wedding-asisstances.destroy')->uses('WeddingAssistanceController@destroy')->middleware('auth', 'kb');

// Wedding Assistance
Route::get('nifas-asisstances')->name('nifas-asisstances')->uses('NifasAssistanceController@index')->middleware('remember', 'auth', 'kb');
Route::get('nifas-asisstances/export')->name('nifas-asisstances.export')->uses('NifasAssistanceController@export')->middleware('remember', 'auth', 'kb');
Route::get('nifas-asisstances/create')->name('nifas-asisstances.create')->uses('NifasAssistanceController@create')->middleware('auth', 'kb');
Route::post('nifas-asisstances')->name('nifas-asisstances.store')->uses('NifasAssistanceController@store')->middleware('auth', 'kb');
Route::get('nifas-asisstances/{nifasAssistance}/edit')->name('nifas-asisstances.edit')->uses('NifasAssistanceController@edit')->middleware('auth', 'kb');
Route::put('nifas-asisstances/{nifasAssistance}')->name('nifas-asisstances.update')->uses('NifasAssistanceController@update')->middleware('auth', 'kb');
Route::delete('nifas-asisstances/{nifasAssistance}')->name('nifas-asisstances.destroy')->uses('NifasAssistanceController@destroy')->middleware('auth', 'kb');

// Pregnancy Assistance
Route::get('pregnancy-asisstances')->name('pregnancy-asisstances')->uses('PregnancyAssistanceController@index')->middleware('remember', 'auth', 'kb');
Route::get('pregnancy-asisstances/export')->name('pregnancy-asisstances.export')->uses('PregnancyAssistanceController@export')->middleware('auth', 'kb');
Route::get('pregnancy-asisstances/create')->name('pregnancy-asisstances.create')->uses('PregnancyAssistanceController@create')->middleware('auth', 'kb');
Route::post('pregnancy-asisstances')->name('pregnancy-asisstances.store')->uses('PregnancyAssistanceController@store')->middleware('auth', 'kb');
Route::get('pregnancy-asisstances/{pregnancyAssistance}/edit')->name('pregnancy-asisstances.edit')->uses('PregnancyAssistanceController@edit')->middleware('auth', 'kb');
Route::put('pregnancy-asisstances/{pregnancyAssistance}')->name('pregnancy-asisstances.update')->uses('PregnancyAssistanceController@update')->middleware('auth', 'kb');
Route::delete('pregnancy-asisstances/{pregnancyAssistance}')->name('pregnancy-asisstances.destroy')->uses('PregnancyAssistanceController@destroy')->middleware('auth', 'kb');

Route::get('balita-asisstances')->name('balita-asisstances')->uses('BalitaAssistanceController@index')->middleware('remember', 'auth', 'ks');
Route::get('balita-asisstances/export')->name('balita-asisstances.export')->uses('BalitaAssistanceController@export')->middleware('auth', 'ks');
Route::get('balita-asisstances/create')->name('balita-asisstances.create')->uses('BalitaAssistanceController@create')->middleware('auth', 'ks');
Route::post('balita-asisstances')->name('balita-asisstances.store')->uses('BalitaAssistanceController@store')->middleware('auth', 'ks');
Route::get('balita-asisstances/{balitaAssistance}/edit')->name('balita-asisstances.edit')->uses('BalitaAssistanceController@edit')->middleware('auth', 'ks');
Route::put('balita-asisstances/{balitaAssistance}')->name('balita-asisstances.update')->uses('BalitaAssistanceController@update')->middleware('auth', 'ks');
Route::delete('balita-asisstances/{balitaAssistance}')->name('balita-asisstances.destroy')->uses('BalitaAssistanceController@destroy')->middleware('auth', 'ks');

// POST BIRTH
Route::get('post-births')->name('post-births')->uses('PostBirthController@index')->middleware('remember', 'auth', 'ks');
Route::get('post-births/export')->name('post-births.export')->uses('PostBirthController@export')->middleware('auth', 'ks');
Route::get('post-births/create')->name('post-births.create')->uses('PostBirthController@create')->middleware('auth', 'ks');
Route::post('post-births')->name('post-births.store')->uses('PostBirthController@store')->middleware('auth', 'ks');
Route::get('post-births/{postBirth}/edit')->name('post-births.edit')->uses('PostBirthController@edit')->middleware('auth', 'ks');
Route::put('post-births/{postBirth}')->name('post-births.update')->uses('PostBirthController@update')->middleware('auth', 'ks');
Route::delete('post-births/{postBirth}')->name('post-births.destroy')->uses('PostBirthController@destroy')->middleware('auth', 'ks');

// UNMEET NEED
Route::get('unmeet-needs')->name('unmeet-needs')->uses('UnMeetNeedController@index')->middleware('remember', 'auth', 'kb');
Route::get('unmeet-needs/export')->name('unmeet-needs.export')->uses('UnMeetNeedController@export')->middleware('auth', 'kb');
Route::get('unmeet-needs/create')->name('unmeet-needs.create')->uses('UnMeetNeedController@create')->middleware('auth', 'kb');
Route::post('unmeet-needs')->name('unmeet-needs.store')->uses('UnMeetNeedController@store')->middleware('auth', 'kb');
Route::get('unmeet-needs/{unMeetNeed}/edit')->name('unmeet-needs.edit')->uses('UnMeetNeedController@edit')->middleware('auth', 'kb');
Route::put('unmeet-needs/{unMeetNeed}')->name('unmeet-needs.update')->uses('UnMeetNeedController@update')->middleware('auth', 'kb');
Route::delete('unmeet-needs/{unMeetNeed}')->name('unmeet-needs.destroy')->uses('UnMeetNeedController@destroy')->middleware('auth', 'kb');

// MARRIAGE AGES
Route::get('marriage-ages')->name('marriage-ages')->uses('MarriageAgeController@index')->middleware('remember', 'auth', 'kb');
Route::get('marriage-ages/export')->name('marriage-ages.export')->uses('MarriageAgeController@export')->middleware('auth', 'kb');
Route::get('marriage-ages/create')->name('marriage-ages.create')->uses('MarriageAgeController@create')->middleware('auth', 'kb');
Route::post('marriage-ages')->name('marriage-ages.store')->uses('MarriageAgeController@store')->middleware('auth', 'kb');
Route::get('marriage-ages/{marriageAge}/edit')->name('marriage-ages.edit')->uses('MarriageAgeController@edit')->middleware('auth', 'kb');
Route::put('marriage-ages/{marriageAge}')->name('marriage-ages.update')->uses('MarriageAgeController@update')->middleware('auth', 'kb');
Route::delete('marriage-ages/{marriageAge}')->name('marriage-ages.destroy')->uses('MarriageAgeController@destroy')->middleware('auth', 'kb');

// POKTAN
Route::get('poktans')->name('poktans')->uses('PoktanController@index')->middleware('remember', 'auth', 'kb');
Route::get('poktans/export')->name('poktans.export')->uses('PoktanController@export')->middleware('auth', 'kb');
Route::get('poktans/create')->name('poktans.create')->uses('PoktanController@create')->middleware('auth', 'kb');
Route::post('poktans')->name('poktans.store')->uses('PoktanController@store')->middleware('auth', 'kb');
Route::get('poktans/{poktan}/edit')->name('poktans.edit')->uses('PoktanController@edit')->middleware('auth', 'kb');
Route::put('poktans/{poktan}')->name('poktans.update')->uses('PoktanController@update')->middleware('auth', 'kb');
Route::delete('poktans/{poktan}')->name('poktans.destroy')->uses('PoktanController@destroy')->middleware('auth', 'kb');

// BOKBS
Route::get('bokbs')->name('bokbs')->uses('BokbController@index')->middleware('remember', 'auth', 'kb');
Route::get('bokbs/export')->name('bokbs.export')->uses('BokbController@export')->middleware('auth', 'kb');
Route::get('bokbs/create')->name('bokbs.create')->uses('BokbController@create')->middleware('auth', 'kb');
Route::post('bokbs')->name('bokbs.store')->uses('BokbController@store')->middleware('auth', 'kb');
Route::get('bokbs/{bokb}/edit')->name('bokbs.edit')->uses('BokbController@edit')->middleware('auth', 'kb');
Route::put('bokbs/{bokb}')->name('bokbs.update')->uses('BokbController@update')->middleware('auth', 'kb');
Route::delete('bokbs/{bokb}')->name('bokbs.destroy')->uses('BokbController@destroy')->middleware('auth', 'kb');

// PRIORITAS
Route::get('priorities')->name('priorities')->uses('PriorityController@index')->middleware('remember', 'auth', 'pkk');
Route::get('priorities/export')->name('priorities.export')->uses('PriorityController@export')->middleware('auth', 'pkk');
Route::get('priorities/create')->name('priorities.create')->uses('PriorityController@create')->middleware('auth', 'pkk');
Route::post('priorities')->name('priorities.store')->uses('PriorityController@store')->middleware('auth', 'pkk');
Route::get('priorities/{priority}/edit')->name('priorities.edit')->uses('PriorityController@edit')->middleware('auth', 'pkk');
Route::put('priorities/{priority}')->name('priorities.update')->uses('PriorityController@update')->middleware('auth', 'pkk');
Route::delete('priorities/{priority}')->name('priorities.destroy')->uses('PriorityController@destroy')->middleware('auth', 'pkk');

// 500 error
Route::get('500', function () {
    echo 'Error';
});
